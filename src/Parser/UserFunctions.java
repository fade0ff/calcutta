package Parser;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

/*
 * Copyright 2009 Volker Oth
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * User functions class used by the Parser to store/read back values into/from variables.
 *
 * @author Volker Oth
 */
public class UserFunctions {
	private HashMap<String,UserFunction[]> ufMap; // <UserFunction[]>
	private String nameSpace = "";
	private ALStack<String> nameSpaceStack; // <String> Stack is used for speedup

	/**
	 * default ctor
	 */
	public UserFunctions() {
		ufMap = new HashMap<String,UserFunction[]>();
		nameSpaceStack = new ALStack<String>();
	}

	/**
	 * Clear user functions
	 */
	public void clear() {
		ufMap.clear();
		resetNameSpace();
	}

	/**
	 * Get sorted list of user defined functions
	 * @return sorted list of user defined functions
	 */
	public ArrayList<String> getList() {
		ArrayList<String> list = new ArrayList<String>();
		/*
		list.addAll(ufMap.keySet());
		Collections.sort(list);
		// returns sorted list of strings !!!
		 */
		Iterator<String> it = ufMap.keySet().iterator();
		while (it.hasNext()) {
			String key = it.next();
			UserFunction ufs[] = ufMap.get(key);
			// there might be more than one function of that name
			for (int i=0; i<ufs.length; i++) {
				// build parameter string
				StringBuffer sb = new StringBuffer(key);
				sb.append('(');
				String param[] = ufs[i].param;
				for (int j=0; j<param.length; j++) {
					sb.append(param[j]);
					sb.append(',');
				}
				if (param.length==0)
					sb.append(')');
				else
					sb.setCharAt(sb.length()-1,')');
				list.add(sb.toString());
			}
		}
		return list;
	}

	/**
	 * Reset NameSpace
	 */
	public void resetNameSpace() {
		nameSpaceStack.clear();
		nameSpace = "";
	}

	/**
	 * Enter Namespace
	 * @param name NameSpace name
	 */
	public void enterNameSpace(String name) {
		StringBuffer sb = new StringBuffer(nameSpace);
		sb.append(name);
		sb.append("@");
		// push old namespace on stack
		nameSpaceStack.push(nameSpace);
		nameSpace = sb.toString();
	}

	/**
	 * Leave namespace
	 */
	public void leaveNameSpace() {
		nameSpace = nameSpaceStack.pop();
	}

	/**
	 * put user function
	 * @param name User function name
	 * @param uf User function
	 * @throws ParseException
	 * @return true if the function didn't exist before, false if it was overwritten
	 */
	boolean put(String name, UserFunction uf) throws ParseException {
		UserFunction ufs[];
		boolean retval = true;
		//
		StringBuffer sb = new StringBuffer(nameSpace);
		sb.append(name);
		// first check if there's already a function
		ufs = ufMap.get(sb.toString());
		if (ufs == null) {
			// first function of this name
			ufs = new UserFunction[1];
			ufs[0] = uf;
		} else {
			// there is/are already function(s) of this name
			// check if on has the same number of parameters
			for (int i=0; i<ufs.length; i++) {
				if (ufs[i].param.length == uf.param.length) {
					retval = false;
					// replace array entry
					ufs[i] = uf;
					break;
				}
			}
			if (retval) {
				// ok, just append to this array
				UserFunction ufsh[] = new UserFunction[ufs.length+1];
				for (int i=0; i<ufs.length; i++) {
					ufsh[i] = ufs[i];
				}
				ufsh[ufs.length] = uf;
				ufs = ufsh;
			}
		}
		// put array of user functions
		ufMap.put(sb.toString(),ufs);
		return retval;
	}


	/**
	 * Get user function
	 * @param name function name
	 * @return String value from variable
	 */
	public UserFunction[] get(String name) {
		UserFunction uf[] = null;
		StringBuffer sb;
		// first look inside this context
		String nSpace = nameSpace;
		sb = new StringBuffer(nSpace);
		sb.append(name);
		uf = ufMap.get(sb.toString());
		if (uf == null) {
			// now search the contexts "above" this one
			for (int i=nameSpaceStack.size()-1; i>=0; i--) {
				nSpace = nameSpaceStack.get(i);
				sb = new StringBuffer(nSpace);
				sb.append(name);
				uf = ufMap.get(sb.toString());
			}
		}
		if (uf!=null)
			return uf;
		return null;
	}

}
