package Parser;

/*
 * Copyright 2009 Volker Oth
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * Storage class for errors in script files.
 *
 * @author Volker Oth
 */
public class ErrorPos {
	int line;
	int pos;
	String file;

	/**
	 * Constructor
	 * @param pos_ Error position inside line
	 * @param line_ Line of the error
	 * @param file_ File in which the error is located
	 */
	public ErrorPos(int pos_, int line_, String file_) {
		line = line_;
		pos = pos_;
		file = file_;
	}

	/**
	 * Copy constructor
	 * @param from Copy content of from to this
	 */
	public ErrorPos(ErrorPos from) {
		line = from.line;
		pos = from.pos;
		file = from.file;
	}


	/**
	 * Setter interface
	 * @param pos_ Error position inside line
	 * @param line_ Line of the error
	 * @param file_ File in which the error is located
	 */
	public void set(int pos_, int line_, String file_) {
		line = line_;
		pos = pos_;
		file = file_;
	}

	/**
	 * Get error position inside line
	 * @return error position inside line
	 */
	public int getPos() {
		return pos;
	}

	/**
	 * Get line of error position
	 * @return line of error position
	 */
	public int getLine() {
		return line;
	}

	/**
	 * Get file in which error is located
	 * @return File in which error is located
	 */
	public String getFile() {
		return file;
	}

	/**
	 * Copy operator
	 * @param from Copy content of from to this
	 */
	public void copyFrom(ErrorPos from) {
		line = from.line;
		pos = from.pos;
		file = from.file;
	}

}

