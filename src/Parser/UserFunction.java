package Parser;

/*
 * Copyright 2009 Volker Oth
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * Implements a user defines function to be used by the parser.
 *
 * @author Volker Oth
 */
public class UserFunction {

	/**
	 * String array containing parameter names
	 */
	public String[] param;

	/**
	 * RPN stack that contains the functionality
	 */
	public ALStack<Token> rpnStack;

	/**
	 * Constructor
	 * @param ufParam String array containing parameter names
	 * @param ufRpnStack RPN stack that contains the functionality
	 */
	public UserFunction(String ufParam[], ALStack<Token> ufRpnStack ) {
		param = ufParam;       // only created for this, so no need to clone
		rpnStack = ufRpnStack; // only created for this, so no need to clone
	}

	/**
	 * Calculate value of user function
	 * @param vParam Parameter values
	 * @return value of user function
	 * @param parser Parser Object
	 * @throws ParseException
	 */
	Value result(Value vParam[], JParse parser) throws ParseException {
		Value result=null;
		parser.variables.pushStack(); // create new stack for local variables
		// put parameters on local variable stack (copy by value)
		for (int i=0; i<param.length; i++) {
			parser.variables.putValue(param[i],vParam[i]);
		}
		try {
			result = parser.evalRPNStack(rpnStack); // calculate result
			// check if result is variable
			parser.variables.popStack();  // destroy stack for local variables
			return result;
		} catch (ParseException ex) {
			parser.variables.popStack();  // destroy stack for local variables
			throw ex;
		}
	}
}
