package Parser;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

/*
 * Copyright 2009 Volker Oth
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * Variable class used by the Parser to store/read back values into/from variables.
 *
 * @author Volker Oth
 */
public class Variables {
	private ALStack<HashMap<String,Value>> varStack; // <HashMap>

	/**
	 * default ctor
	 */
	public Variables() {
		varStack = new ALStack<HashMap<String,Value>>();
		varStack.push(new HashMap<String,Value>());
	}

	/**
	 * Get sorted list of variables
	 * @return sorted list of variables
	 */
	public ArrayList<String> getList() {
		ArrayList<String> list = new ArrayList<String>();
		for(int i=varStack.size()-1;i>=0; i--)
			list.addAll((varStack.get(i)).keySet());
		Collections.sort(list);
		// returns sorted list of strings !!!
		return list;
	}

	/**
	 * Clear variables
	 */
	public void clear() {
		varStack.clear();
		varStack.push(new HashMap<String,Value>());
	}

	/**
	 * Create new variable hash on the stack.
	 */
	public void pushStack() {
		varStack.push(new HashMap<String,Value>());
	}

	/**
	 * Delete uppermost variable hash from the stack
	 */
	public void popStack() {
		varStack.pop();
	}

	/**
	 * Put Value Object in variable
	 * @param name variable name
	 * @param val variable value
	 * @throws ParseException
	 */
	public void putValue(String name, Value val) throws ParseException {
		// always put in the uppermost variable hash
		HashMap<String,Value> hash = varStack.peek();
		// check for indexed access
		int pos;
		String strIndex="";
		if ( (pos=name.indexOf("@")) != -1 ) {
			// indexed access
			strIndex = name.substring(pos+1); // points at first character after "@"
			name = name.substring(0,pos);
		}
		if (pos != -1) {
			// check if variable already exists
			Value v = hash.get(name);
			if (v == null)
				throw new ParseException("variable \""+name+"\" undefined");
			// now get the variable at the correct index.
			do {
				int idx;
				if (v.type != Value.LIST)
					throw new ParseException("Index access on variable that's not a list.");
				pos = strIndex.indexOf("@"); // another index ?
				if (pos == -1) {
					// last index
					idx = Integer.valueOf(strIndex).intValue();
				} else {
					idx = Integer.valueOf(strIndex.substring(0,pos)).intValue();
					strIndex = strIndex.substring(pos+1); // points at first character after "@"
				}
				ValueList al = (ValueList)v.value;
				if (idx < 0 || idx >= al.size())
					throw new ParseException("Index "+idx+" out of bounds.");
				v = al.get(idx);
			} while ( pos != -1 );
			// copy new value
			v.copyFrom(val);
		} else {
			// simple case: just put
			hash.put(name,new Value(val));
		}
	}

	/**
	 * Put JNumber value in variable
	 * @param name variable name
	 * @param num variable value
	 * @throws ParseException
	 */
	public void putNumber(String name, JNumber num) throws ParseException {
		putValue(name, new Value(num));
	}

	/**
	 * Put String value in variable
	 * @param name variable name
	 * @param s variabel value
	 * @throws ParseException
	 */
	public void putString(String name, String s) throws ParseException {
		putValue(name, new Value(s));
	}

	/**
	 * Put boolean value in variable
	 * @param name variable name
	 * @param b variabel value
	 * @throws ParseException
	 */
	public void putBoolean(String name, boolean b) throws ParseException {
		putValue(name, new Value(Boolean.valueOf(b)));
	}

	/**
	 * Get value from the hash(es)
	 * All the stacks are searched from the bottom to the current level.
	 * @param name Name of value
	 * @return Value
	 * @throws ParseException
	 */
	private Value getVal(String name) throws ParseException {
		HashMap<String,Value> hash;
		Value val=null;
		int pos;
		String strIndex="";

		if ( (pos=name.indexOf("@")) != -1 ) {
			// indexed access
			strIndex = name.substring(pos+1); // points at first character after "@"
			name = name.substring(0,pos);
		}
		// search variable name
		for(int i=varStack.size()-1;i>=0; i--) {
			hash = varStack.get(i);
			val = hash.get(name);
			if (val!=null)
				break;
		}
		if (val == null)
			throw new ParseException("variable \""+name+"\" undefined");
		if (pos != -1) {
			// indexed
			// now get the variable at the correct index.
			do {
				int idx;
				if (val.type != Value.LIST)
					throw new ParseException("Index access on variable that's not a list.");
				pos = strIndex.indexOf("@"); // another index ?
				if (pos == -1) {
					// last index
					idx = Integer.valueOf(strIndex).intValue();
				} else {
					idx = Integer.valueOf(strIndex.substring(0,pos)).intValue();
					strIndex = strIndex.substring(pos+1); // points at first character after "@"
				}
				ValueList al = (ValueList)val.value;
				if (idx < 0 || idx >= al.size())
					throw new ParseException("Index "+idx+" out of bounds.");
				val = al.get(idx);
			} while ( pos != -1 );
		}
		return val;
	}


	/**
	 * Get Value object from variable
	 * @param name variable name
	 * @return Value object
	 * @throws ParseException
	 */
	public Value getValue(String name) throws ParseException{
		Value val = getVal(name);
		return val;
	}

	/**
	 * Get String from variable
	 * @param name variable name
	 * @return String value from variable
	 * @throws ParseException
	 */
	public JNumber getNumber(String name) throws ParseException{
		Value val = getVal(name);
		if (val.type!=Value.NUMBER)
			throw new ParseException("variable \""+name+"\" is not a number");
		return (JNumber)val.value;
	}

	/**
	 * Get boolean from variable
	 * @param name variable name
	 * @return boolean value from variable
	 * @throws ParseException
	 */
	public boolean getBoolean(String name) throws ParseException{
		Value val = getVal(name);
		if (val.type!=Value.BOOL)
			throw new ParseException("variable \""+name+"\" is not a boolean");
		return ((Boolean)val.value).booleanValue();
	}

	/**
	 * Get String from variable
	 * @param name variable name
	 * @return String value from variable
	 * @throws ParseException
	 */
	public String getString(String name) throws ParseException{
		Value val = getVal(name);
		if (val.type!=Value.STRING)
			throw new ParseException("variable \""+name+"\" is not a string");
		return (String)val.value;
	}

}
