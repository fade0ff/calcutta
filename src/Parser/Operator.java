package Parser;

/*
 * Copyright 2009 Volker Oth
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * Abstract class used to define operators used by the parser.
 *
 * @author Volker Oth
 */
public class Operator {

	final static long serialVersionUID = 0x000000001;

	/**
	 * Operator associated to operand on left side
	 */
	public static final int UNARY_LEFT  = 0;
	/**
	 * Operator associated to operand on right side
	 */
	public static final int UNARY_RIGHT = 1;
	/**
	 * Operator using operand on both sides, associated to the left (evaluate left side first, e.g. a+b+c)
	 */
	public static final int BINARY_LEFT = 2;
	/**
	 * Operator using operand on both sides, associated to the right (evaluate right side first, e.g. a=b=c)
	 */
	public static final int BINARY_RIGHT = 3;
	/**
	 * In parsing, type of operator expected can be unknown
	 */
	public static final int UNKNOWN     = 4;

	/**
	 * Association type: left
	 */
	public static final int LEFT       = 32;
	/**
	 * Association type: right
	 */
	public static final int RIGHT      = 64;


	/**
	 * Allowed characters for operators (the 2nd minus is ASCII code 0x96!)
	 */
	public static final String operatorChars = "-�+*/%<>=!~^|&?:";


	/**
	 * Operator type (unary_right, binary_left etc.)
	 */
	public int     type;        // unary, binary
	/**
	 * Operator precedence (the lower the number, the higher the priority)
	 */
	public int     precedence;
	/**
	 * Operator association (derived from {@link Operator#type})
	 */
	public int     association; // association

	/**
	 * Default ctor
	 * @param precedence_ Operator precedence (the lower the number, the higher the priority)
	 */
	public Operator(int precedence_) {
		precedence = precedence_;
	}

	/**
	 * ctor
	 * @param type_ Operator type (unary_right, binary_left etc.)
	 * @param precedence_ Operator precedence (the lower the number, the higher the priority)
	 */
	public Operator(int type_, int precedence_) {
		type = type_;
		precedence = precedence_;
		switch (type) {
			case BINARY_LEFT:
			case UNARY_LEFT:
				association = LEFT;
				break;
			default:
				association = RIGHT;
		}
	}

	/**
	 * Returns result of operator calculation
	 * @param operand left or right operand or both
	 * @return result of operator calculation
	 * @throws ParseException
	 */
	public Value result(Token operand[]) throws ParseException {
		return new Value(false);
	}

	/** Overload if this class is a shortcut operator
	 *  @return Can this operator use a shortcut?
	 *  @throws ParseException
	 */
	public boolean canShortCut() throws ParseException {
		return false;
	}

	/** Overload if this class is a shortcut operator
	 *  @param operand Operand to check for shortcut condition
	 *  @return Is this a shortcut condition?
	 *  @throws ParseException
	 */
	public boolean isShortCut(Token operand) throws ParseException {
		return false;
	}

	/** Overload if this class is a shortcut operator
	 *  @return Result of shortcut evaluation
	 */
	public Value resultShortCut() {
		return null;
	}
}