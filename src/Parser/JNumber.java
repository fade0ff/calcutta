package Parser;

import java.math.BigDecimal;
import java.math.BigInteger;

/*
 * Copyright 2009 Volker Oth
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * Rational number class that expresses a number as a fraction using a BigInteger numerator and denominator.
 *
 * @author Volker Oth
 */
public class JNumber {

	final static long serialVersionUID = 0x000000001;

	/**
	 * Numerator of rational number
	 */
	private BigInteger numerator;
	/**
	 * Denominator of rational number
	 */
	private BigInteger denominator;

	/**
	 * Scale used for BigDecimal calculations
	 */
	static int internalScale = 64;
	/**
	 * Rounding mode applied in here
	 */
	public static int roundingMode = BigDecimal.ROUND_HALF_DOWN;
	/**
	 * Accuray used for BigDecimal calculations
	 */
	static BigDecimal internalAccuracy = new BigDecimal(BigInteger.ONE,internalScale);
	/**
	 * Maximum number of bits for integer output
	 */
	final static int outputBits   =  512;
	/**
	 * Maximum numbers after the "."
	 */
	final static int postScale   =  16;
	/**
	 * Accuracy used for BigDecimal output
	 */
	final static BigDecimal outputAccuracy = new BigDecimal(BigInteger.ONE,postScale);
	/**
	 * Maximum numbers before the "."
	 */
	final static int preScale   =  16;
	/**
	 * Maximum zeroes after the "."
	 */
	final static int zeroScale   =  9;

	private static BigDecimal log2 = BigDecimalMath.log2(internalScale);
	private static BigDecimal log10 = BigDecimalMath.log10(internalScale);

	final private static BigInteger BigIntegerONE     = BigInteger.valueOf(1);
	final private static BigInteger BigIntegerTEN     = BigInteger.valueOf(10);
	final private static BigInteger BigIntegerMAXINT  = BigInteger.valueOf(Integer.MAX_VALUE);
	final private static BigInteger BigIntegerMININT  = BigInteger.valueOf(Integer.MIN_VALUE);

	final private static BigDecimal BigDecimalONE     = BigDecimal.valueOf(1);


	/**
	 * Constant for ZERO
	 */
	final public static JNumber ZERO = new JNumber(0);
	/**
	 * Constant for 0.5
	 */
	final public static JNumber HALF = new JNumber("1/2");
	/**
	 * Constant for ONE
	 */
	final public static JNumber ONE = new JNumber(1);
	/**
	 * Constant for TWO
	 */
	final public static JNumber TWO = new JNumber(2);
	/**
	 * Constant for TEN
	 */
	final public static JNumber TEN = new JNumber(10);
	/**
	 * Constant for ONE MILLION
	 */
	final public static JNumber MILLION = new JNumber(1000000);

	public static void setInternalScale(int scale) {
		internalScale = scale;
		internalAccuracy = new BigDecimal(BigInteger.ONE,internalScale);
		log2 = BigDecimalMath.log2(internalScale);
		log10 = BigDecimalMath.log10(internalScale);

	}

	//    private static boolean interrupt = false;

	public static void interrupt() {
		//    	interrupt = true;
	}

	/**
	 * default ctor
	 */
	public JNumber() {
		numerator = BigInteger.ZERO;
		denominator = BigInteger.ONE;
	}

	/**
	 * Copy ctor. Contruct JNUmber from a given JNumber
	 * @param num Number to copy
	 */
	public JNumber(JNumber num) {
		numerator = BigInteger.ZERO.add(num.numerator);
		denominator = BigInteger.ZERO.add(num.denominator);
	}

	/**
	 * Contruct JNumber from long numerator and denominator
	 * @param numerator_ Numerator
	 * @param denominator_ Denominator
	 */
	public JNumber(long numerator_, long denominator_) {
		numerator = new BigInteger(Long.toString(numerator_));
		denominator = new BigInteger(Long.toString(denominator_));
	}

	/**
	 * ctor
	 * Contruct JNumber from long BigInteger numerator and denominator
	 * @param numerator_ Numerator
	 * @param denominator_ Denominator
	 */
	public JNumber(BigInteger numerator_, BigInteger denominator_) {
		numerator = numerator_;
		denominator = denominator_;
	}

	/**
	 * Contruct JNumber from long
	 * @param num long number to convert
	 * @throws NumberFormatException
	 */
	public JNumber(long num) throws NumberFormatException {
		numerator = BigInteger.valueOf(num);
		denominator = BigInteger.ONE;
	}

	/**
	 * Contruct JNumber from double
	 * @param num double number to convert
	 * @throws NumberFormatException
	 */
	public JNumber(double num) throws NumberFormatException {
		parseString(Double.toString(num));
	}

	/**
	 * Contruct JNumber from BigDecimal
	 * @param d BigDecimal to convert
	 * @see #parseString(String)
	 * @throws NumberFormatException
	 */

	public JNumber(BigDecimal d) throws NumberFormatException {
		parseString(d.toString());
	}
	/**
	 * Contruct JNumber from String
	 * Handles hex ("0xab"), binary ("0b1010"), octal ("076"), decimal ("0.123"), double ("0.234e-4") and
	 * decimal with period ("0.123p456").
	 * @param s String to convert
	 * @see #parseString(String)
	 * @throws NumberFormatException
	 */
	public JNumber(String s) throws NumberFormatException {
		parseString(s);
	}

	/**
	 * Set JNumber to value from String
	 * Handles hex ("0xab"), binary ("0b1010"), octal ("076"), decimal ("0.123"), double ("0.234e-4") and
	 * decimal with period ("0.123p456").
	 * @param s String to convert
	 * @see #JNumber(String)
	 * @throws NumberFormatException
	 */
	private void parseString(String s)throws NumberFormatException {
		int pos, expSign=1;
		String temp;
		BigInteger t,e=null;

		if (s.length() < 1)
			throw new NumberFormatException();

		numerator = null;
		denominator = BigInteger.ONE;

		if ( (pos=s.indexOf('.')) == -1) {
			// test if 0xNNN, 0b123 and so on
			try {
				if (s.charAt(0) == '0') {
					if (s.length() == 1) {
						numerator = BigInteger.ZERO;
					}
					else if (s.charAt(1) == 'x') {
						// hex
						numerator = new BigInteger(s.substring(2),16);
					}
					else if (s.charAt(1) == 'b') {
						// binary
						numerator = new BigInteger(s.substring(2),2);
					} else {
						// octal
						numerator = new BigInteger(s.substring(0),8);
					}
				}
			} catch (NumberFormatException ex) {
				numerator = null;
			}
		}

		if (numerator == null) {
			// special case: contains exponent
			if ( (pos=s.indexOf('e')) != -1 || (pos=s.indexOf('E')) != -1) {
				// get exponent 10^n
				int p;
				if ( s.length() > pos+2 && (p="-+".indexOf(s.charAt(pos+1))) != -1) {
					if (p==0)
						expSign = -1;
					else
						expSign = +1;
					temp = s.substring(pos+2);
				}
				else
					temp = s.substring(pos+1);
				e = BigIntegerTEN.pow(Integer.parseInt(temp));
				s = s.substring(0,pos);
			}
			// special case: fraction representation ( can only happen after fraction-NUM_STRING output)
			if ( (pos=s.indexOf('/')) != -1) {
				String num, denom;
				num = s.substring(0,pos);
				denom = s.substring(pos+1,s.length());
				numerator = new BigInteger(num);
				denominator = new BigInteger(denom);
			}
			else if ( (pos=s.indexOf('.')) != -1) {
				String before,after,period=null; // before.after
				StringBuffer sb = new StringBuffer();
				before = s.substring(0,pos);
				after = s.substring(pos+1,s.length());
				if ( (pos=after.indexOf('p')) != -1) {
					period = after.substring(pos+1);
					after = after.substring(0,pos);
				}
				if (period == null || period.length()==0)
					period = "0";
				// calculate numerator
				sb.append(before); sb.append(after);
				t = new BigInteger(sb.toString());
				sb.append(period);
				numerator = new BigInteger(sb.toString());
				numerator = numerator.subtract(t);
				// calculate denominator
				sb.setLength(0);
				for (int i=0; i<period.length();i++)
					sb.append('9');
				for (int i=0; i<after.length();i++)
					sb.append('0');
				denominator = new BigInteger(sb.toString());

			}
		}
		// last resort: normal number or bust
		if (numerator == null)
			numerator = new BigInteger(s);

		if (e!=null) {
			if (expSign<0)
				denominator = denominator.multiply(e);
			else
				numerator = numerator.multiply(e);
		}
		cancelDown();
	}

	/**
	 * Calculate least common multiple of BigInteger a and b
	 * @param a BigInteger parameter
	 * @param b BigInteger parameter
	 * @return least common multiple of BigInteger a and b
	 */
	/*
	private BigInteger leastCommonMultiple(BigInteger a, BigInteger b) {
		BigInteger lcm = a.multiply(b);
		lcm.divide(a.gcd(b));
		return lcm;
	}
	 */


	/**
	 * Cancel down fraction
	 * Changes this
	 * @return this
	 */
	public JNumber cancelDown() {
		BigInteger gcd = numerator.gcd(denominator);
		if (!gcd.equals(BigInteger.ONE)) {
			numerator = numerator.divide(gcd);
			denominator = denominator.divide(gcd);
		}
		// don't use negative denominators
		if (denominator.signum() < 0) {
			denominator = denominator.negate();
			numerator = numerator.negate();
		}
		return this;
	}

	/**
	 * returns binary scale of this
	 * @return binary scale of this
	 */
	public long scale() {
		if (denominator.equals(BigInteger.ONE))
			return numerator.bitLength();
		return numerator.bitLength() - denominator.bitLength();
	}

	/**
	 * returns signum of this
	 * @return signum of this
	 */
	public long signum() {
		return numerator.signum();
	}

	/**
	 * Add fractional number to this
	 * Changes this
	 * @param num JNumber to add
	 * @return this
	 */
	public JNumber add(JNumber num) {
		// test the easy case
		if (denominator.equals(BigInteger.ONE) && num.denominator.equals(BigInteger.ONE)) {
			numerator = numerator.add(num.numerator);
			return this;
		}
		// now the not so easy case
		BigInteger a,b;
		a = numerator.multiply(num.denominator);
		b = num.numerator.multiply(denominator);
		numerator = a.add(b);
		denominator = denominator.multiply(num.denominator);
		cancelDown();
		return this;
	}

	/**
	 * Subtract fractional number from this
	 * Changes this
	 * @param num JNumber to subtract
	 * @return this
	 */
	public JNumber subtract(JNumber num) {
		// test the easy case
		if (denominator.equals(BigInteger.ONE) && num.denominator.equals(BigInteger.ONE)) {
			numerator = numerator.subtract(num.numerator);
			return this;
		}
		// now the not so easy case
		BigInteger a,b;
		a = numerator.multiply(num.denominator);
		b = num.numerator.multiply(denominator);
		numerator = a.subtract(b);
		denominator = denominator.multiply(num.denominator);
		cancelDown();
		return this;
	}

	/**
	 * Multiply this with fractional number
	 * Changes this
	 * @param num JNumber multiplicator
	 * @return this
	 */
	public JNumber multiply(JNumber num) {
		numerator = numerator.multiply(num.numerator);
		denominator = denominator.multiply(num.denominator);
		cancelDown();
		return this;
	}

	/**
	 * Divide this by fractional number
	 * Changes this
	 * @param num JNumber divider
	 * @return this
	 * @throws ArithmeticException
	 */
	public JNumber divide(JNumber num) throws ArithmeticException {
		numerator = numerator.multiply(num.denominator);
		denominator = denominator.multiply(num.numerator);
		if (denominator.signum() == 0)
			throw new ArithmeticException();
		cancelDown();
		return this;
	}

	/**
	 * Divide this by BigInteger
	 * Changes this
	 * @param num JNumber divider
	 * @return this
	 * @throws ArithmeticException
	 */
	public JNumber divide(BigInteger num) throws ArithmeticException {
		denominator = denominator.multiply(num);
		if (denominator.signum() == 0)
			throw new ArithmeticException();
		cancelDown();
		return this;
	}

	/**
	 * Convert this to BigInteger by dividing numerator by denominator and keeping only the integer part.
	 * Doesn't change this.
	 * @return integer part of fraction
	 */
	public BigInteger toBigInteger() {
		if (denominator.equals(BigInteger.ONE))
			return numerator;
		return numerator.divide(denominator);
	}

	/**
	 * Convert this to long by dividing numerator by denominator and keeping only the integer part.
	 * Doesn't change this.
	 * @return integer part of fraction
	 */
	public long toLong() {
		if (denominator.equals(BigInteger.ONE))
			return numerator.longValue();
		return numerator.divide(denominator).longValue();
	}


	/**
	 * Check if this is an integer value (denominator == 1)
	 * @return true (is integer) or false (is no integer)
	 */
	public boolean isInteger() {
		if (denominator.equals(BigInteger.ONE))
			return true;
		return false;
	}

	/**
	 * Convert this to double by dividing numerator by denominator and keeping only the integer part.
	 * Doesn't change this.
	 * @return double representation of fraction
	 */
	public double toDouble() {
		if (denominator.equals(BigInteger.ONE))
			return numerator.doubleValue();
		return numerator.doubleValue() / denominator.doubleValue();
	}

	/**
	 * Limit unsigned integer to a maximum value, e.g. for u16 cast: limit = 65536
	 * Changes this
	 * @param limit Value that can't be reached
	 * @return this
	 */
	public JNumber limitUnsigned(BigInteger limit) {
		BigInteger i = this.toBigInteger();
		i = i.mod(limit);
		if (i.signum()< 0)
			i = limit.subtract(i);
		numerator = i;
		denominator = BigInteger.ONE;
		return this;
	}

	/**
	 * Limit signed integer to a maximum value, e.g. for s16 cast: limit = 65536
	 * Changes this
	 * @param limit Value that can't be reached
	 * @param maxPositive Maximum positive value
	 * @param maxNegative Maximum negative value
	 * @return this
	 */
	public JNumber limitSigned(BigInteger limit, BigInteger maxPositive, BigInteger maxNegative) {
		BigInteger i = this.toBigInteger();
		i = i.mod(limit); // limit to max value
		if (i.compareTo(maxPositive)>0)
			i = limit.subtract(i).negate();
		else if (i.compareTo(maxNegative)<0)
			i = limit.subtract(i);
		numerator = i;
		denominator = BigInteger.ONE;
		return this;
	}

	/**
	 * Convert this to an integer by dividing numerator by denominator and keeping only the integer part.
	 * Changes this
	 * @return this
	 */
	public JNumber trunc() {
		numerator = this.toBigInteger();
		denominator = BigInteger.ONE;
		return this;
	}

	/**
	 * Returns the largest (closest to positive infinity) value that is less than
	 * or equal to the argument and is equal to a mathematical integer
	 * Changes this
	 * @return this
	 */
	public JNumber floor() {
		JNumber i = new JNumber(this);
		i.trunc();
		if (i.compareTo(this)>0)
			i.subtract(ONE);
		this.copyFrom(i);
		return this;
	}

	/**
	 * Return the smallest (closest to negative infinity) value that is greater than or
	 * equal to the argument and is equal to a mathematical integer.
	 * Changes this
	 * @return this
	 */
	public JNumber ceil() {
		JNumber i = new JNumber(this);
		i.trunc();
		if (i.compareTo(this)<0)
			i.add(ONE);
		this.copyFrom(i);
		return this;
	}

	/**
	 * Round this to an integer by adding 0.5, then dividing numerator by denominator and keeping only the integer part.
	 * Changes this
	 * @return this
	 */
	public JNumber round() {
		this.add(HALF).floor();
		return this;
	}

	/**
	 * Negates this (-x)
	 * Changes this
	 * @return this
	 */
	public JNumber negate() {
		numerator = numerator.negate();
		return this;
	}

	/**
	 * Complement of this (~x)
	 * Changes this. If this is not an integer, it is converted first.
	 * @return this
	 * @throws JNumberException
	 */
	public JNumber not() throws JNumberException {
		if (!this.isInteger())
			throw new JNumberException("Only defined for integer values.");
		numerator = numerator.not();
		return this;
	}

	/**
	 * Or this with integer number num ( x | num)
	 * Changes this. If this is not an integer, it is converted first.
	 * @param num OR value. If this is not an integer, it is converted first.
	 * @return this
	 * @throws JNumberException
	 */
	public JNumber or(JNumber num) throws JNumberException {
		if (!this.isInteger() || !num.isInteger())
			throw new JNumberException("Only defined for integer values.");
		numerator = numerator.or(num.numerator);
		return this;
	}

	/**
	 * Xor this with integer number num ( x ^ num)
	 * Changes this. If this is not an integer, it is converted first.
	 * @param num Xor value. If this is not an integer, it is converted first.
	 * @return this
	 * @throws JNumberException
	 */
	public JNumber xor(JNumber num) throws JNumberException {
		if (!this.isInteger() || !num.isInteger())
			throw new JNumberException("Only defined for integer values.");
		numerator = numerator.xor(num.numerator);
		return this;
	}

	/**
	 * And this with integer number num ( x & num)
	 * Changes this. If this is not an integer, it is converted first.
	 * @param num And value. If this is not an integer, it is converted first.
	 * @return this
	 * @throws JNumberException
	 */
	public JNumber and(JNumber num) throws JNumberException {
		if (!this.isInteger() || !num.isInteger())
			throw new JNumberException("Only defined for integer values.");
		numerator = numerator.and(num.numerator);
		return this;
	}

	/**
	 * Calculate this << leftshift distance
	 * Changes this. If this is not an integer, it is converted first.
	 * @param distance Shift distance. must be Integer and <= Integer.MAX_VALUE
	 * @throws JNumberException
	 * @return this
	 */
	public JNumber shiftLeft(JNumber distance) throws JNumberException {
		if (!this.isInteger() || !distance.isInteger())
			throw new JNumberException("Only defined for integer values.");
		if (distance.numerator.compareTo(BigIntegerMAXINT)>0)
			throw new JNumberException("Shift distance must be integer and <="+Integer.MAX_VALUE);
		int d = distance.numerator.intValue();
		numerator = numerator.shiftLeft(d);
		return this;
	}

	/**
	 * Calculate this >> distance
	 * Changes this. If this is not an integer, it is converted first.
	 * @param distance Shift distance. Must be Integer and <= Integer.MAX_VALUE
	 * @throws JNumberException
	 * @return this
	 */
	public JNumber shiftRight(JNumber distance) throws JNumberException {
		if (!this.isInteger() || !distance.isInteger())
			throw new JNumberException("Only defined for integer values.");
		if (!distance.isInteger() || distance.numerator.compareTo(BigIntegerMAXINT)>0)
			throw new JNumberException("Shift distance must be integer and <="+Integer.MAX_VALUE);
		int d = distance.numerator.intValue();
		numerator = numerator.shiftRight(d);
		return this;
	}

	/**
	 * Calculate x to the power of exp (x**exp)
	 * Changes this. If this is not an integer, it is converted first.
	 * @param exp Exponent. Must be Integer and <= Integer.MAX_VALUE
	 * @return this
	 * @throws JNumberException
	 */
	public JNumber pow(JNumber exp) throws JNumberException {
		if (!exp.isInteger() || exp.numerator.compareTo(BigIntegerMAXINT)>0 || exp.numerator.compareTo(BigIntegerMININT)<0) {
			BigDecimal x,n;
			x = this.toBigDecimal(internalScale);
			n = exp.toBigDecimal(internalScale);
			JNumber result = new JNumber(BigDecimalMath.pow(x,n,internalScale));
			this.copyFrom(result);
			return this;
		}
		int e = exp.numerator.intValue();
		if (e<0) {
			if (numerator.signum() == 0)
				throw new JNumberException("Divison by zero");
			e=-e;
			BigInteger temp = numerator.pow(e);
			numerator = denominator.pow(e);
			denominator = temp;
			// don't use negative denominators
			if (denominator.signum() < 0) {
				denominator = denominator.negate();
				numerator = numerator.negate();
			}
		} else {
			numerator = numerator.pow(e);
			denominator = denominator.pow(e);
		}
		cancelDown();
		return this;
	}

	/**
	 * Calculate factorial of this (x!)
	 * Changes this. If this is not an integer, it is converted first. this must be Integer and <= Integer.MAX_VALUE
	 * @return factorial of this
	 * @throws JNumberException
	 */
	public JNumber fac() throws JNumberException {
		cancelDown();
		if (!this.isInteger() || this.numerator.compareTo(BigIntegerMAXINT)>0 || this.numerator.compareTo(BigIntegerMININT)<0)
			throw new JNumberException("Parameter must be integer and <="+Integer.MAX_VALUE+" and >="+Integer.MIN_VALUE);

		int f = numerator.intValue();
		Factorial fact = new Factorial();
		numerator = fact.factorial(f);

		return this;
	}

	/**
	 * Calculate this % num
	 * Changes this. If this is not an integer, it is converted first.
	 * @param divider Exponent. If this is not an integer, it is converted first.
	 * @return this
	 * @throws JNumberException
	 */
	public JNumber mod(JNumber divider) throws JNumberException {
		//        if (!divider.isInteger() || divider.numerator.compareTo(BigIntegerMAXINT)>0)
		//            throw new JNumberException("Divider must be positive and integer and <="+Integer.MAX_VALUE);
		if (!divider.isInteger() || divider.numerator.compareTo(BigInteger.ZERO) <= 0)
			throw new JNumberException("Divider must be integer and positive");

		if (divider.numerator.compareTo(BigIntegerMAXINT)>0) {
			this.trunc();
			BigInteger d[] = numerator.divideAndRemainder(divider.numerator);
			numerator = d[1];
			return this;
		}

		BigInteger d;
		//        if (!divider.isInteger())
		//            d = divider.toBigInteger();
		//        else
		d = divider.numerator;
		this.trunc();
		numerator = numerator.remainder(d);
		return this;
	}

	/**
	 * Calculate square root of this
	 * @throws JNumberException
	 * @return square root of this
	 */
	public JNumber sqrt() throws JNumberException {
		if (this.signum()<0)
			throw new JNumberException("Argument of sqrt must not be negative");
		BigDecimal x = toBigDecimal();
		JNumber result = new JNumber(BigDecimalMath.sqrt(x,internalScale));
		this.copyFrom(result);
		return this;
	}

	/**
	 * Calculate nth root of this
	 * @param N Nth root
	 * @throws JNumberException
	 * @return square root of this
	 */
	public JNumber root(JNumber N) throws JNumberException {
		if (this.signum()<0)
			throw new JNumberException("First argument of root must not be negative");
		BigDecimal x = toBigDecimal();
		BigDecimal n = N.toBigDecimal();
		JNumber result = new JNumber(BigDecimalMath.root(x,n,internalScale));
		this.copyFrom(result);
		return this;
	}


	/**
	 * Calculate equality between this and JNumber num
	 * @param num Number to compare this with
	 * @return boolean expressing if this equals num
	 */
	public boolean equals(JNumber num) {
		return (numerator.equals(num.numerator) && denominator.equals(num.denominator));
	}

	/**
	 * Calculate equality between this and Object obj
	 * @param obj Object to compare this with
	 * @return boolean expressing if this equals obj
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof JNumber) {
			JNumber num = (JNumber)obj;
			return (numerator.equals(num.numerator) && denominator.equals(num.denominator));
		}
		return false;
	}

	/**
	 * Calculate absolute value of this: abs(x)
	 * Changes this
	 * @return this
	 */
	public JNumber abs() {
		numerator = numerator.abs();
		return this;
	}

	/**
	 * Numbers of zeroes behind the dot in decimal representation if Numerator < Denominator, else 0
	 * Doesn't change this
	 * @return numbers of zeroes behind the dot in decimal representation
	 */
	public int zeroScale() {
		if (numerator.compareTo(denominator) >= 0)
			return 0;
		String n = numerator.toString();
		String d = denominator.toString();
		return (d.length()-n.length()-1);
	}

	/**
	 * Compares this to JNumber num
	 * @param num JNumber to compare this to
	 * @return -1 if this &gt; num, 0 if this == num, 1 if this &lt; num
	 */
	public int compareTo(JNumber num) {
		// test the easy case
		if (denominator.equals(BigInteger.ONE) && num.denominator.equals(BigInteger.ONE))
			return numerator.compareTo(num.numerator);
		// now the not so easy case
		BigInteger a,b;
		a = numerator.multiply(num.denominator);
		b = num.numerator.multiply(denominator);
		return a.compareTo(b);
	}

	/**
	 * Test if BigInteger num is a prime number
	 * @param num BigInteger to test
	 * @return true if num is a prime number, else false
	 */
	private static boolean isPrime(BigInteger num) {
		/*
		BigInteger divider = BigIntegerTHREE;
		if (num.compareTo(BigIntegerTWO) < 0)  // negative or 0 or 1 -> no prime
			return false;
		if (num.compareTo(BigIntegerTWO) == 0) // 2 -> prime
			return true;
		if (!num.testBit(0))                   // lowest bit not set -> even -> no prime
			return false;

		BigInteger maxDivider = sqrt(num);
		while (divider.compareTo(maxDivider) <= 0) {
			if (num.mod(divider).signum()==0)
				return false;
			divider = divider.add(BigIntegerTWO);
		}
		return true;
		 */
		return num.isProbablePrime(256);
	}

	/**
	 * Test if this num is a prime number
	 * Doesn't change this
	 * @return true if this is a prime number, else false
	 */
	public boolean isPrime() {
		BigInteger testNum = new JNumber(this).trunc().numerator;
		return isPrime(testNum);
	}

	/**
	 * Calculates BigInteger square root of BigInteger n
	 * @param n BigInteger to calcuate squate root for
	 * @return BigInteger square root of BigInteger n
	 */
	/*
	private static BigInteger sqrt(BigInteger n) {
		BigInteger result = n.divide(BigIntegerTWO).add(BigInteger.ONE);
		BigInteger help = result.add(BigInteger.ONE);
		//while (result*result > x || (result+1)*(result+1)< x)
		while (result.multiply(result).compareTo(n) > 0 || help.multiply(help).compareTo(n) < 0) {
			result = result.add(n.divide(result)).divide(BigIntegerTWO);
			help = result.add(BigInteger.ONE);
		}
		return result;
	}
	 */

	/**
	 * Convert this to BigDecimal
	 * @return this converted to BigDecimal
	 */
	public BigDecimal toBigDecimal() {
		if (isInteger())
			return new BigDecimal(this.numerator);
		BigDecimal n,d;
		n = new BigDecimal(this.numerator);
		d = new BigDecimal(this.denominator);
		try {
			return n.divide(d,BigDecimal.ROUND_UNNECESSARY);
		} catch (ArithmeticException ex) {
			return n.divide(d,internalScale,roundingMode);
		}
	}

	/**
	 * Convert this to BigDecimal
	 * @param scale Scale to use for conversion
	 * @return this converted to BigDecimal
	 */
	public BigDecimal toBigDecimal(int scale) {
		if (isInteger())
			return new BigDecimal(this.numerator);
		BigDecimal n,d;
		n = new BigDecimal(this.numerator);
		d = new BigDecimal(this.denominator);
		try {
			return n.divide(d,BigDecimal.ROUND_UNNECESSARY);
		} catch (ArithmeticException ex) {
			return n.divide(d,scale,roundingMode);
		}
	}

	/**
	 * Calculate sinus of this
	 * Changes this
	 * Uses Taylor approximation
	 * @return sinus(this)
	 */
	public JNumber sin() {
		BigDecimal x1 = toBigDecimal();
		JNumber result = new JNumber(BigDecimalMath.sin(x1,internalScale));
		this.copyFrom(result);
		return this;
	}

	/**
	 * Calculate sinh of this
	 * Uses Taylor approximation
	 * @return sinus hyperbolicus (this)
	 */
	public JNumber sinh() {
		BigDecimal x1 = toBigDecimal();
		JNumber result = new JNumber(BigDecimalMath.sinh(x1,internalScale));
		this.copyFrom(result);
		return this;
	}

	/**
	 * Calculate asin of this
	 * Uses Taylor approximation
	 * @throws JNumberException
	 * @return arcus sinus (this)
	 */
	public JNumber asin() throws JNumberException {
		if (this.compareTo(ONE) > 0)
			throw new JNumberException("input parameter must be <= 1.0");
		BigDecimal x1 = toBigDecimal();
		JNumber result = new JNumber(BigDecimalMath.asin(x1,internalScale));
		this.copyFrom(result);
		return this;
	}

	/**
	 * Calculate cosinus of this
	 * Uses Taylor approximation
	 * @return cosinus(this)
	 */
	public JNumber cos() {
		BigDecimal x1 = toBigDecimal();
		JNumber result = new JNumber(BigDecimalMath.cos(x1,internalScale));
		this.copyFrom(result);
		return this;
	}

	/**
	 * Calculate cosh of this
	 * Uses Taylor approximation
	 * @return cosinus hyperbolicus (this)
	 */
	public JNumber cosh() {
		BigDecimal x1 = toBigDecimal();
		JNumber result = new JNumber(BigDecimalMath.cosh(x1,internalScale));
		this.copyFrom(result);
		return this;
	}

	/**
	 * Calculate acos of this
	 * Uses Taylor approximation
	 * @throws JNumberException
	 * @return arcus cosinus (this)
	 */
	public JNumber acos() throws JNumberException {
		if (this.compareTo(ONE) > 0)
			throw new JNumberException("input parameter must be <= 1.0");
		BigDecimal x1 = toBigDecimal();
		JNumber result = new JNumber(BigDecimalMath.acos(x1,internalScale));
		this.copyFrom(result);
		return this;
	}

	/**
	 * Calculate tangens of this
	 * Uses Taylor approximation
	 * @return tangens (this)
	 */
	public JNumber tan() {
		BigDecimal x1 = toBigDecimal();
		JNumber result = new JNumber(BigDecimalMath.tan(x1,internalScale));
		this.copyFrom(result);
		return this;
	}

	/**
	 * Calculate tanh of this
	 * Uses Taylor approximation
	 * @return tangens hyperbolicus (this)
	 */
	public JNumber tanh() {
		BigDecimal x1 = toBigDecimal();
		JNumber result = new JNumber(BigDecimalMath.tanh(x1,internalScale));
		this.copyFrom(result);
		return this;
	}

	/**
	 * Calculate atan of this
	 * Uses Taylor approximation
	 * @return arcus sinus (this)
	 */
	public JNumber atan() {
		BigDecimal x1 = toBigDecimal();
		JNumber result = new JNumber(BigDecimalMath.atan(x1,internalScale));
		this.copyFrom(result);
		return this;
	}

	/**
	 * Copy numerator and denonimator from num to this (no new instances are created)
	 * @param num Number to copy numerator and denonimator from
	 */
	public void copyFrom(JNumber num) {
		if (num != this) {
			numerator = num.numerator;
			denominator = num.denominator;
		}
	}

	/**
	 * Calculate natural logarithm of this
	 * Uses Taylor approximation
	 * @return log(this)
	 */
	public JNumber log() {
		BigDecimal x = toBigDecimal();
		JNumber result = new JNumber(BigDecimalMath.log(x,internalScale));
		this.copyFrom(result);
		return this;
	}

	/**
	 * Calculate logarithm dualis of this
	 * Uses Taylor approximation
	 * @return ld(this)
	 */
	public JNumber ld() {
		BigDecimal x = toBigDecimal();
		JNumber result = new JNumber(BigDecimalMath.log(x,internalScale).divide(log2,internalScale,roundingMode));
		this.copyFrom(result);
		return this;
	}

	/**
	 * Calculate logarithm to base 10
	 * Uses Taylor approximation
	 * @return log10(this)
	 */
	public JNumber log10() {
		BigDecimal x = toBigDecimal();
		JNumber result = new JNumber(BigDecimalMath.log(x,internalScale).divide(log10,internalScale,roundingMode));
		this.copyFrom(result);
		return this;
	}


	/**
	 * Calculate exp of this
	 * Uses Taylor approximation
	 * @return exp(this)
	 */
	public JNumber exp() {
		BigDecimal x = toBigDecimal();
		return new JNumber(BigDecimalMath.exp(x,internalScale));
	}

	/**
	 * Convert value of fraction to a string
	 * @see java.lang.Object#toString()
	 * @return String representing the value of the fraction
	 */
	@Override
	public String toString() {
		cancelDown();
		if (this.numerator.signum() == 0)
			return "0";
		// convert to bigdecimal and then to string to analyze number
		BigDecimal dec = this.toBigDecimal(postScale*2);
		// special case: very small number
		if (dec.abs().compareTo(outputAccuracy) < 0) {
			int exp = zeroScale();
			exp = exp/3*3;
			JNumber s = new JNumber(this);
			s.numerator = s.numerator.multiply(BigIntegerTEN.pow(exp));
			dec = s.toBigDecimal(postScale);
			return dec.toString()+"e-"+exp;
		}
		String out = dec.toString();
		// find numbers before and after decimal dot
		int dotPos, lenPre, lenPost, len, i;
		len = out.length();
		dotPos = out.indexOf('.');
		// check for redundant zeroes
		if (dotPos != -1) {
			for (i=len -1; i>dotPos; i--)
				if (out.charAt(i) != '0')
					break;
			if (i != len-1)
				len = i+1;
			lenPre = dotPos;
			lenPost = len-lenPre-1;
		} else {
			lenPre = len;
			lenPost = 0;
		}
		if (out.charAt(0) == '-')
			lenPre--;
		// first case: very small number -> Xe-Y
		if (dec.abs().compareTo(BigDecimalONE)<0) {
			//find first relevant number
			for (i=dotPos+1; i<len;i++)
				if (out.charAt(i) != '0')
					break;
			// workaround for bug in BigDecimal's toString() function
			// 1/200000000 -> out = 5000000000000000.0000000000000000e-24
			if (out.charAt(i) == 'E')
				i = zeroScale;
			if (i > zeroScale) {
				int exponent = i/3; // "000" makes 3 characters
				dec = dec.multiply(new BigDecimal(BigInteger.valueOf(1000).pow(exponent)));
				dec = dec.setScale(postScale,roundingMode);
				return dec.toString()+"e-"+(exponent*3);
			}
			// workaround for JVM 1.5
			else if ( (i=out.indexOf('E'))!=-1) {
				int exponent = -Integer.parseInt(out.substring(i+1))/3;
				dec = dec.multiply(new BigDecimal(BigInteger.valueOf(1000).pow(exponent)));
				dec = dec.setScale(postScale,roundingMode);
				out = dec.toString();
				// check for redundant zeroes
				dotPos = out.indexOf('.');
				if (dotPos != -1) {
					for (i=out.length()-1; i>dotPos; i--)
						if (out.charAt(i) != '0')
							break;
				}
				if (i > dotPos)
					i++;
				out = out.substring(0, i);
				return out+"e-"+(exponent*3);
			}
		}
		// second case: very large number -> XeY
		if (lenPre>preScale) {
			int exponent = lenPre/3; // "000" makes 3 characters
			dec = dec.divide(new BigDecimal(BigInteger.valueOf(1000).pow(exponent)),postScale,roundingMode);
			return dec.toString()+"e+"+(exponent*3);
		}
		// third case: round scale
		if (lenPost > postScale)
			lenPost = postScale;
		dec = dec.setScale(lenPost,roundingMode);
		return dec.toString();
	}

	/**
	 * Convert value of fraction to a string
	 * @see java.lang.Object#toString()
	 * @return String representing the value of the fraction
	 */
	public String toFullString() {
		cancelDown();
		if (this.numerator.signum() == 0)
			return "0";
		if (isInteger())
			return numerator.toString();
		// convert to bigdecimal and then to string to analyze number
		BigDecimal dec = this.toBigDecimal(internalScale*2);
		// special case: very small number
		if (dec.compareTo(internalAccuracy) < 0) {
			int exp = zeroScale();
			exp = exp/3*3; // I engineer ;)
			JNumber s = new JNumber(this);
			s.numerator = s.numerator.multiply(BigIntegerTEN.pow(exp));
			dec = s.toBigDecimal(internalScale);
			return dec.toString()+"e-"+exp;
		}
		String out = dec.toString();
		// find numbers before and after decimal dot
		int dotPos, lenPre, lenPost, len, i;
		len = out.length();
		dotPos = out.indexOf('.');
		// check for redundant zeroes
		if (dotPos != -1) {
			for (i=len-1; i>dotPos; i--)
				if (out.charAt(i) != '0')
					break;
			if (i != len-1)
				len = i+1;
			lenPre = dotPos;
			lenPost = len-lenPre-1;
		} else {
			lenPre = len;
			lenPost = 0;
		}
		dec = dec.setScale(lenPost,roundingMode);
		return dec.toString();
	}

	/**
	 * Convert value of fraction to a string
	 * @return string representing fraction as numerator/denominator
	 */
	public String toFraction() {
		String n = numerator.toString();
		String d = denominator.toString();
		return n+"/"+d;
	}


	/**
	 * Convert value of fraction to a string considering the radix
	 * @param radix Radix used for creating the string representation
	 * @return String representing the value of the fraction
	 */
	public String toString(int radix) {
		cancelDown();
		BigInteger outNum = new JNumber(this).trunc().numerator;
		return outNum.toString(radix);
	}

	/**
	 * Convert BigInteger bi to a hex string
	 * @param bi BigInteger to create a hex string from
	 * @return String representing the value of the fraction
	 */
	public String toHexString(BigInteger bi) {
		byte bytes[] = bi.toByteArray();
		StringBuffer sb = new StringBuffer();

		// number of array elements = number of bytes
		// enhance to next power of 2
		int len = bytes.length;
		int i=0;
		boolean first = true;

		// skip leading zero
		if (len > 1 && bytes[0] == 0)
			i++;

		for (; i<bytes.length; i++) {
			String temp = Integer.toHexString(bytes[i]);
			len = temp.length();
			if (len>2)
				temp = temp.substring(len-2,len);
			else if (len<2 && !first)
				sb.append('0');
			sb.append(temp);
			first = false;
		}

		// make sure that a positive number always never starts with an 'f'
		int sign = bi.signum();
		if (sign > 0 && sb.charAt(0)=='f')
			sb.insert(0, '0');
		else if (sign < 0 && sb.charAt(0)!='f') {
			// make sure that a negative number always starts with an f
			sb.insert(0, 'f');
			// fill up the leading byte in case of f padding
			if ((sb.length()&1)==1)
				sb.insert(0, 'f');
		}

		return sb.toString();
	}

	/**
	 * Convert this to hex string
	 * @return String representing the hex value
	 */
	public String toHexString() {
		BigInteger bi = this.toBigInteger();
		return toHexString(bi);
	}

	/**
	 * Convert BigInteger bi to a binary string
	 * @param bi BigInteger to create a binary string from
	 * @return String representing the value of the fraction
	 */
	public String toBinaryString(BigInteger bi) {
		byte bytes[] = bi.toByteArray();
		StringBuffer sb = new StringBuffer();

		// number of array elements = number of bytes
		// enhance to next power of 2
		int byteNum = 1;
		int len = bytes.length;
		int i;

		// power of 2 just larger than the number
		while ( byteNum < len)
			byteNum *= 2;

		if (bi.signum() < 0)
			for (i=0; i<byteNum-len; i++)
				sb.append("11111111");

		for (i = 0; i<bytes.length; i++) {
			String temp = Integer.toBinaryString(bytes[i]);
			len = temp.length();
			if (len>8)
				temp = temp.substring(len-8,len);
			else for (int j=len; j<8; j++)
				sb.append('0');
			sb.append(temp);
		}
		return sb.toString();
	}

	/**
	 * Convert this to binary string
	 * @return String representing the binary value
	 */
	public String toBinaryString() {
		cancelDown();
		BigInteger bi = this.toBigInteger();
		return toBinaryString(bi);
	}

	/**
	 * Convert BigInteger bi to an octal string
	 * @param bi BigInteger to create a octal string from
	 * @return String representing the value of the fraction
	 */
	public String toOctalString(BigInteger bi) {
		if (bi.signum() < 0) {
			int len = bi.bitLength();
			// number of bytes needed
			if ((len % 8) != 0)
				len = len / 8  + 1;
			else
				len = len/8;
			// power of 2 just larger than the number
			int byteNum = 1;
			while ( byteNum < len)
				byteNum *= 2;
			// bi = 1<<(byteNum*8)
			bi = BigIntegerONE.shiftLeft(byteNum*8).add(bi);
		}
		String s = bi.toString(8);
		return s;
	}

	/**
	 * Convert this to octal string
	 * @return String representing the octal value
	 */
	public String toOctalString() {
		cancelDown();
		BigInteger bi = this.toBigInteger();
		return toOctalString(bi);
	}

	/**
	 * Fast integer bitlength implementation of logarithmus dualis
	 * @return approximated ld(this)
	 */
	public int numBits() {
		cancelDown();
		BigInteger bi = this.toBigInteger();
		int result = bi.bitLength();
		return result;
	}

}

/**
 * Fast Factorial calculation.
 *
 * @author Volker Oth / Peter Luschny
 * @see http://www.luschny.de/math/factorial/index.html
 * @see <a href="http://www.luschny.de/math/factorial/index.html">Peter Luschny's site</a>
 */
class Factorial {

	private BigInteger f;
	private long N;

	private final BigInteger swing(long n) {
		long nN = this.N;
		long s = nN - 1 + ((n - nN + 1) % 4);
		boolean oddN = (nN & 1) != 1;

		for (; nN <= s; nN++) {
			if (oddN = !oddN)
				f = f.multiply(BigInteger.valueOf(nN));
			else
				f = (f.shiftLeft(2)).divide(BigInteger.valueOf(nN));
		}

		if (oddN)
			for (; nN <= n; nN += 4) {
				long m = ((nN + 1) * (nN + 3)) << 1;
				long d = (nN * (nN + 2)) >> 3;

				f = f.multiply(BigInteger.valueOf(m)).divide(BigInteger.valueOf(d));
			}
		else
			for (; nN <= n; nN += 4) {
				long m = (nN * (nN + 2)) << 1;
				long d = ((nN + 1) * (nN + 3)) >> 3;

				f = f.multiply(BigInteger.valueOf(m)).divide(BigInteger.valueOf(d));
			}
		this.N = nN;
		return f;
	}

	private final BigInteger recFactorial(int n) {
		if (n < 2)
			return BigInteger.ONE;
		BigInteger result = recFactorial(n / 2);
		return result.multiply(result).multiply(swing(n));
	}

	BigInteger factorial(int n) {
		N = 1;
		f = BigInteger.ONE;
		return recFactorial(n);
	}
}
