package Parser;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

/*
 * Copyright 2009 Volker Oth
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * Specialized list class for objects of type {@link Value}.
 *
 * @author Volker Oth
 */
public class ValueList extends ArrayList<Value> {

	final static long serialVersionUID = 0x000000001;

	/**
	 * default ctor
	 */
	public ValueList() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * ctor
	 * @param initialCapacity
	 */
	public ValueList(int initialCapacity) {
		super(initialCapacity);
		// TODO Auto-generated constructor stub
	}

	/**
	 * ctor (creates shallow copy of collection c)
	 * @param c
	 */
	public ValueList(Collection<Value> c) {
		super(c);
		// TODO Auto-generated constructor stub
	}

	/**
	 * Calculate equality between this and list
	 * @param list List to compare this with
	 * @return boolean expressing if this equals list
	 */
	public boolean equals(ValueList list) {
		int size = size();
		int lSize = list.size();
		if (size != lSize)
			return false;
		Iterator<Value> i1 = iterator();
		Iterator<Value> i2 = list.iterator();
		Value v1,v2;
		while (i1.hasNext()) {
			v1 = i1.next();
			v2 = i2.next();
			if (v1.type != v2.type)
				return false;
			if (!v1.equals(v2))
				return false;
		}
		return true;
	}

	/**
	 * Calculate equality between this and Object obj
	 * @param obj Object to compare this with
	 * @return boolean expressing if this equals obj
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof ValueList)
			return (equals((ValueList)obj));
		return false;
	}

	private void deepcopy(ValueList from) {
		for (int i=0; i<from.size();i++)
			add(new Value(from.get(i)));
	}

	/**
	 * Deep copy of other Jlist into this list
	 * @param from
	 */
	public void copyFrom(ValueList from) {
		clear();
		deepcopy(from);
	}

	/**
	 * clone Jlist (creates deep copy)
	 * @return deep copy of this
	 * @see java.lang.Object#clone()
	 */
	@Override
	public ValueList clone() {
		ValueList jl = new ValueList();
		jl.deepcopy(this);
		return jl;
	}

}
