package Parser;

/*
 * Copyright 2009 Volker Oth
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * Token class for bottom-up parser
 * 
 * @author Volker Oth
 */
public class Token {
	final static long serialVersionUID = 0x000000001;

	/**
	 * Start token
	 */
	public static final int START = 0;
	/**
	 * Value token
	 */
	public static final int VALUE = 1;
	/**
	 * Variable token
	 */
	public static final int VARIABLE = 2;
	/**
	 * Operator token
	 */
	public static final int OPERATOR = 3;
	/**
	 * Function token
	 */
	public static final int FUNCTION = 4;
	/**
	 * User function token
	 */
	public static final int USER_FUNCTION = 8;
	/**
	 * User function name token
	 */
	public static final int USER_FUNCTION_NAME = 9;
	/**
	 * Parenthesis open token "("
	 */
	public static final int PARENTHESIS_OPEN = 16;
	/**
	 * Parenthesis close token ")"
	 */
	public static final int PARENTHESIS_CLOSE = 17;
	/**
	 * Parenthesis open token "{"
	 */
	public static final int BRACE_OPEN = 18;
	/**
	 * Parenthesis close token "}"
	 */
	public static final int BRACE_CLOSE = 19;
	/**
	 * Parenthesis open token "["
	 */
	public static final int BRACKET_OPEN = 20;
	/**
	 * Parenthesis close token "]"
	 */
	public static final int BRACKET_CLOSE = 21;

	/**
	 * separator token ";"
	 */
	public static final int SEPARATOR = 32;
	/**
	 * function separator token ","
	 */
	public static final int FUNCTION_SEPARATOR = 33;
	/**
	 * ternary operator "?"
	 */
	public static final int TERNARY_QMARK = 34;
	/**
	 * colon ":" used as ternary sub-operator (but maybe also in switch case in the future)
	 */
	public static final int COLON = 35;
	/**
	 * Remember size of vstack
	 */
	public static final int STORE_VSTACK_SIZE = 250;
	/**
	 * Push one dummy on stack if it's needed to increase the vstack
	 */
	public static final int POP_IF_VSTACK_UNEQUAL = 251;
	/**
	 * Push one dummy on stack if it's needed to increase the vstack
	 */
	public static final int PUSH_DUMMY_IF_VSTACK_EQUAL = 252;
	/**
	 * Push one dummy on stack
	 */
	public static final int PUSH_DUMMY = 253;
	/**
	 * End token
	 */
	public static final int END = 254;
	/**
	 * Invalid token
	 */
	public static final int INVALID = 255;
	/**
	 * Shortcut start token
	 */
	public static final int SHORTCUT_START = 64;

	/**
	 * Conditiona: jump if condition fulfilles
	 */
	public static final int JMP_TRUE = 65;

	/**
	 * Conditional: jump if condition fulfilled
	 */
	public static final int JMP_FALSE = 66;

	/**
	 * Conditional: unconditional jump
	 */

	public static final int JMP = 67;
	/**
	 * Pop N elements from value stack
	 */
	public static final int POP_VALUE = 68;

	/**
	 * get first element of list
	 */
	public static final int FOREACH_INIT = 69;

	/**
	 * get first element of list
	 */
	public static final int FOREACH_NEXT = 70;

	/**
	 * Keyword user function
	 */
	public static final int KEYWORD_FUNCTION = 128;

	/**
	 * Keyword if
	 */
	public static final int KEYWORD_IF = 129;

	/**
	 * Keyword else
	 */
	public static final int KEYWORD_ELSE = 130;

	/**
	 * Keyword for
	 */
	public static final int KEYWORD_FOR = 131;

	/**
	 * Keyword do
	 */
	public static final int KEYWORD_DO = 132;

	/**
	 * Keyword while
	 */
	public static final int KEYWORD_WHILE = 133;
	/**
	 * Keyword foreach
	 */
	public static final int KEYWORD_FOREACH = 134;
	/**
	 * indexed variables are stored var index INDEXED_ACCESS on RPN stack
	 */
	public static final int INDEXED_ACCESS = 164;
	/**
	 * LISTs are stored elem 0, ... , elem N-1 LIST(N)
	 */
	public static final int LIST = 165;


	/**
	 * Token type
	 */
	int type;    
	/**
	 * parenthesis nesting level
	 */
	int pLevel;    
	/**
	 * function nesting level
	 */
	int fLevel;    // function nesting level
	/**
	 * character position of this token in original text line
	 */
	ErrorPos pos;       // char position in original text line
	/**
	 * Token name
	 */
	String name;
	/**
	 * Token value: can be a Value, a Function or an Operator or null
	 */
	Object content; 

	/**
	 * Copy constructor
	 * @param token Token to construct new Token from
	 */
	public Token(Token token) {
		name   = token.name;
		type   = token.type;
		content  = token.content;
		pLevel = token.pLevel;
		fLevel = token.fLevel;
		pos = new ErrorPos(token.pos);
	}

	/**
	 * ctor
	 * @param type_ Construct token of this type 
	 */
	public Token(int type_) {
		name = null;
		type = type_;
		content = null;
		pLevel = 0;
		fLevel = 0;
		pos = new ErrorPos(0,0,"");
	}

	/**
	 * ctor
	 * @param type_ Token type
	 * @param name_ Token name 
	 */
	public Token(int type_, String name_) {
		name = name_ /*new String(name_)*/;
		type  = type_;
		content = null;
		pLevel = 0;
		fLevel = 0;
		pos = new ErrorPos(0,0,"");
	}

	/**
	 * ctor
	 * @param type_ Token type
	 * @param value_ Token value
	 */
	public Token(int type_, Value value_) {
		name = null;
		type  = type_;
		content = new Value(value_);
		pLevel = 0;
		fLevel = 0;
		pos = new ErrorPos(0,0,"");
	}

	/**
	 * Return Token value as JNumber
	 * @return Token value as JNumber
	 * @throws ParseException
	 */
	public JNumber getNumber() throws ParseException{
		Value val = (Value)content;
		if (val.type!=Value.NUMBER)
			throw new ParseException("token is not a number");
		return (JNumber)val.value;
	}

	/**
	 * Return Token value as Boolean 
	 * @return Token value as Boolean
	 * @throws ParseException
	 */
	public boolean getBoolean() throws ParseException{
		Value val = (Value)content;
		if (val.type!=Value.BOOL)
			throw new ParseException("token is not a boolean");
		return ((Boolean)val.value).booleanValue();
	}

	/**
	 * Return Token value as Boolean
	 * @return Token value as Boolean
	 * @throws ParseException
	 */
	public String getString() throws ParseException{        
		Value val = (Value)content;
		if (val.type!=Value.STRING)
			throw new ParseException("variable \""+name+"\" is not a string");
		return (String)val.value;
	}

	/** 
	 * Returns string representation of Token value
	 * @see java.lang.Object#toString()
	 * @return string representation of Token value
	 */
	@Override
	public String toString() {
		if (name!=null)
			return name;
		switch (type) {
			case VALUE:
				return ((Value)content).toString();
			case PARENTHESIS_OPEN:
				return "(";
				case PARENTHESIS_CLOSE:
					return ")";
				case BRACKET_OPEN:
					return "[";
				case BRACKET_CLOSE:
					return "]";
				case BRACE_OPEN:
					return "{";
				case BRACE_CLOSE:
					return "}";
				case START:
					return "<START>";
				case PUSH_DUMMY:
					return "<PUSH_DUMMY>";
				case STORE_VSTACK_SIZE:
					return "<STORE_VSTACK>";
				case PUSH_DUMMY_IF_VSTACK_EQUAL:
					return "<PUSH_DUMMY_IF_VS_EQ>";
				case POP_IF_VSTACK_UNEQUAL:
					return "<POP_IF_VS_NEQ>";
				case END:
					return "<END>";
				case INVALID:
					return "<INVALID>";
				case SEPARATOR:
					return ";";
				case Token.FUNCTION_SEPARATOR: 
					return ",";
				case Token.TERNARY_QMARK:
					return "?";
				case Token.COLON:
					return ":";
				case Token.SHORTCUT_START: 
					return "<SC_START>";
				case Token.FOREACH_INIT: 
					return "<FOREACH_INIT>";
				case Token.FOREACH_NEXT: 
					return "<FOREACH_NEXT:"+((Integer)content).intValue()+">";
				case Token.JMP: 
					return "<JMP:"+((Integer)content).intValue()+">";
				case Token.JMP_TRUE: 
					return "<JMP_TRUE:"+((Integer)content).intValue()+">";
				case Token.JMP_FALSE: 
					return "<JMP_FALSE:"+((Integer)content).intValue()+">";
				case Token.POP_VALUE:
					return "<POP_VALUE:"+((Integer)content).intValue()+">";
				case Token.LIST:
					return "<LIST:"+((Integer)content).intValue()+">";
				case Token.INDEXED_ACCESS:
					return "<INDEXEC_ACCESS>";
				default:
					if (this.name != null)
						return name;
					return "<UNDEF TOKEN>";                
		}
	}
}