package Parser;

/*
 * Copyright 2009 Volker Oth
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * Value class used by the parser. Internally stores numbers, booleans, strings and so on.
 *
 * @author Volker Oth
 */
public class Value {

	final static long serialVersionUID = 0x000000001;
	/**
	 * Value is a number
	 */
	public static final int NUMBER=0;

	/**
	 * Value is a boolean
	 */
	public static final int BOOL=1;

	/**
	 * Value is a string
	 */
	public static final int STRING=2;

	/**
	 * Value is a numeric string (treated as a number, but can be e.g. printed in hex format)
	 */
	public static final int NUMSTRING=4;

	/**
	 * Value is a list (can contain any other value, including list)
	 */
	public static final int LIST=8;

	/**
	 * Value is undefined (NIL/NULL/UNDEF)
	 */
	public static final int UNDEF=16;

	/**
	 * Type of value (NUMBER, BOOL, STRING,UNDEF)
	 */
	public int type;

	/**
	 * Value: JNumber, Boolean, String
	 */
	public Object value;

	/**
	 *  UNDEF ctor
	 */
	public Value() {
		type = UNDEF;
		value = null;
	}

	/**
	 * Number ctor
	 * @param num Number
	 */
	public Value(JNumber num) {
		type = NUMBER;
		value = new JNumber(num);
	}

	/**
	 * List ctor (creates only a shallow copy!)
	 * @param list ValueList
	 */
	public Value(ValueList list) {
		type = LIST;
		value = new ValueList(list);
	}

	/**
	 * String ctor
	 * @param s String to store
	 */
	public Value(String s) {
		type = STRING;
		value = s; // Strings are immutable
	}

	/**
	 * Boolean ctor
	 * @see #Value(boolean)
	 * @param b Boolean to store
	 */
	public Value(Boolean b) {
		type = BOOL;
		value = Boolean.valueOf(b.booleanValue()); // no copy constructor
	}

	/**
	 * boolean ctor
	 * @see #Value(Boolean)
	 * @param b Boolean basic type to store
	 */
	public Value(boolean b) {
		type = BOOL;
		value = Boolean.valueOf(b);
	}

	/**
	 * Copy ctor
	 * @param v Value to copy
	 */
	public Value(Value v) {
		type = v.type;
		switch (v.type) {
			case BOOL:
				value = Boolean.valueOf(((Boolean)v.value).booleanValue());
				break;
			case STRING:
				value = new String((String)v.value);
				break;
			case NUMBER:
				value = new JNumber((JNumber)v.value);
				break;
			case NUMSTRING:
				value = new JNumber((String)v.value);
				type = NUMBER; // differs from "copyFrom" -> here: conversion to JNumber
				break;
			case UNDEF:
				value = null;
				type = UNDEF;
				break;
			case LIST:
				value = ((ValueList)v.value).clone();
				break;
		}
	}

	/**
	 * Returns string representation of stored value
	 * @see java.lang.Object#toString()
	 * @return string representation of stored value
	 */
	@Override
	public String toString() {
		switch (type) {
			case Value.NUMBER:
				return ((JNumber)value).toString();
			case Value.BOOL:
				return ((Boolean)value).toString();
			case Value.STRING:
				return "\""+(String)value+"\"";
			case Value.NUMSTRING:
				return (String)value;
			case Value.LIST:
				return listString();
			default:
				return "<UNDEF VALUE>";
		}
	}

	private String listString() {
		StringBuffer sb = new StringBuffer("{");
		ValueList jl = (ValueList)value;
		Value v;
		for (int i=0; i<jl.size(); i++) {
			v = (jl.get(i));
			sb.append(v.toString()); // this could create recursion!
			sb.append(",");
		}
		if (jl.size() > 0)
			sb.setLength(sb.length()-1);
		sb.append("}");
		return sb.toString();
	}

	/**
	 * Calculate equality between this and Value v
	 * @param v Value to compare this with
	 * @return boolean expressing if this equals v
	 */
	public boolean equals(Value v) {
		if (type != v.type)
			return false;
		switch (type) {
			case Value.NUMBER:
				return ((JNumber)value).equals((JNumber)v.value);
			case Value.BOOL:
				return ((Boolean)value).equals(v.value);
			case Value.STRING:
				return ((String)value).equals(v.value);
			case Value.LIST:
				return ((ValueList)value).equals((ValueList)v.value);
		}
		return false;
	}

	/**
	 * Calculate equality between this and Object obj
	 * @param obj Object to compare this with
	 * @return boolean expressing if this equals obj
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Value)
			return equals((Value)obj);
		return false;
	}

	/**
	 * Copy contents of from to this
	 * @param from Source object
	 */
	public void copyFrom(Value from) {
		type = from.type;
		switch (from.type) {
			case BOOL:
				value = Boolean.valueOf(((Boolean)from.value).booleanValue());
				break;
			case STRING:
				value = new String((String)from.value);
				break;
			case NUMBER:
				value = new JNumber((JNumber)from.value);
				break;
			case NUMSTRING:
				value = new String((String)from.value); // differs from copy constructor: here: keep NUMSTRING
				break;
			case LIST:
				value = ((ValueList)from.value).clone();
				break;
			case UNDEF:
				value = null;
				type = UNDEF;
				break;
		}
	}
}
