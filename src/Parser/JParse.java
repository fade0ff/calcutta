package Parser;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Date;
import java.util.EmptyStackException;
import java.util.HashMap;
import java.util.Iterator;

/*
 * Copyright 2009 Volker Oth
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * Main parser class.
 *
 * @author Volker Oth
 */
public class JParse {

	/*
	 * inner function classes
	 */
	/**
	 * Calculate absolute value: abs(x)
	 */
	class FunctionAbs extends Function {
		/**
		 * Default ctor
		 */
		public FunctionAbs() {
			super(1);
		}
		/**
		 * @see Function#result(Token[])
		 */
		@Override
		public Value result(Token param[]) throws ParseException {
			JNumber result = getNumber(param[0],Token.VALUE|Token.VARIABLE,1).abs();
			return new Value(result);
		}
	}

	/**
	 * Calculate random value: rnd(x)
	 */
	class FunctionRnd extends Function {
		/**
		 * Default ctor
		 */
		public FunctionRnd() {
			super(0);
		}
		/**
		 * @see Function#result(Token[])
		 */
		@Override
		public Value result(Token param[]) throws ParseException {
			JNumber result = new JNumber(Math.random());
			return new Value(result);
		}
	}
	/**
	 * Calculate Nth power of x: x^N
	 */
	class FunctionPow extends Function {
		/**
		 * @see Function#Function()
		 */
		public FunctionPow() {
			super(2);
		}
		/**
		 * @see Function#result(Token[])
		 */
		@Override
		public Value result(Token param[]) throws ParseException {
			try {
				JNumber p1, p2, result;
				p1 = getNumber(param[0],Token.VALUE|Token.VARIABLE,1);
				p2 = getNumber(param[1],Token.VALUE|Token.VARIABLE,2);
				result = p1.pow(p2);
				return new Value(result);
			} catch (JNumberException ex) {
				throw new ParseException(ex.getMessage());
			}
		}
	}

	class FunctionFact extends Function {
		/**
		 * @see Function#Function()
		 */
		public FunctionFact() {
			super(1);
		}
		/**
		 * @see Function#result(Token[])
		 */
		@Override
		public Value result(Token param[]) throws ParseException {
			JNumber p1;
			JNumber result;
			p1 = getInteger(param[0], Token.VALUE | Token.VARIABLE, 1);
			try {
				result = p1.fac();
			} catch (JNumberException ex) {
				throw new ParseException(ex.getMessage());
			}
			return new Value(result);
		}
	}

	/**
	 * Round number down to nearest integer
	 */
	class FunctionTrunc extends Function {

		/**
		 * @see Function#Function()
		 */
		public FunctionTrunc() {
			super(1);
		}
		/**
		 * @see Function#result(Token[])
		 */
		@Override
		public Value result(Token param[]) throws ParseException {
			JNumber result = getNumber(param[0],Token.VALUE|Token.VARIABLE,1).trunc();
			return new Value(result);
		}
	}

	/**
	 * Round number down to nearest integer
	 */
	class FunctionFloor extends Function {

		/**
		 * @see Function#Function()
		 */
		public FunctionFloor() {
			super(1);
		}
		/**
		 * @see Function#result(Token[])
		 */
		@Override
		public Value result(Token param[]) throws ParseException {
			JNumber result = getNumber(param[0],Token.VALUE|Token.VARIABLE,1).floor();
			return new Value(result);
		}
	}

	/**
	 * Round number down to nearest integer
	 */
	class FunctionCeil extends Function {

		/**
		 * @see Function#Function()
		 */
		public FunctionCeil() {
			super(1);
		}
		/**
		 * @see Function#result(Token[])
		 */
		@Override
		public Value result(Token param[]) throws ParseException {
			JNumber result = getNumber(param[0],Token.VALUE|Token.VARIABLE,1).ceil();
			return new Value(result);
		}
	}

	/**
	 * Round number down to nearest integer
	 */
	class FunctionRound extends Function {

		/**
		 * @see Function#Function()
		 */
		public FunctionRound() {
			super(1);
		}
		/**
		 * @see Function#result(Token[])
		 */
		@Override
		public Value result(Token param[]) throws ParseException {
			JNumber result = getNumber(param[0],Token.VALUE|Token.VARIABLE,1).round();
			return new Value(result);
		}
	}


	class FunctionPrime extends Function {
		/**
		 * @see Function#Function()
		 */
		public FunctionPrime() {
			super(1);
		}
		/**
		 * @see Function#result(Token[])
		 */
		@Override
		public Value result(Token param[]) throws ParseException {
			JNumber p1;
			boolean result;
			p1 = getInteger(param[0], Token.VALUE | Token.VARIABLE, 1);
			result = p1.isPrime();
			return new Value(result);
		}
	}

	class FunctionSqrt extends Function {
		/**
		 * @see Function#Function()
		 */
		public FunctionSqrt() {
			super(1);
		}
		/**
		 * @see Function#result(Token[])
		 */
		@Override
		public Value result(Token param[]) throws ParseException {
			JNumber p1;
			JNumber result;
			p1 = getNumber(param[0], Token.VALUE | Token.VARIABLE, 1);
			try {
				result = p1.sqrt();
			} catch (JNumberException ex) {
				throw new ParseException(ex.getMessage());
			}
			return new Value(result);
		}
	}

	class FunctionRoot extends Function {
		/**
		 * @see Function#Function()
		 */
		public FunctionRoot() {
			super(2);
		}
		/**
		 * @see Function#result(Token[])
		 */
		@Override
		public Value result(Token param[]) throws ParseException {
			JNumber p1,p2;
			JNumber result;
			p1 = getNumber(param[0], Token.VALUE | Token.VARIABLE, 1);
			p2 = getNumber(param[1], Token.VALUE | Token.VARIABLE, 2);
			try {
				result = p1.root(p2);
			} catch (JNumberException ex) {
				throw new ParseException(ex.getMessage());
			}
			return new Value(result);
		}
	}

	class FunctionSin extends Function {
		/**
		 * @see Function#Function()
		 */
		public FunctionSin() {
			super(1);
		}
		/**
		 * @see Function#result(Token[])
		 */
		@Override
		public Value result(Token param[]) throws ParseException {
			JNumber p1;
			JNumber result;
			p1 = getNumber(param[0], Token.VALUE | Token.VARIABLE, 1);
			result = p1.sin();
			return new Value(result);
		}
	}

	class FunctionSinH extends Function {
		/**
		 * @see Function#Function()
		 */
		public FunctionSinH() {
			super(1);
		}
		/**
		 * @see Function#result(Token[])
		 */
		@Override
		public Value result(Token param[]) throws ParseException {
			JNumber p1;
			JNumber result;
			p1 = getNumber(param[0], Token.VALUE | Token.VARIABLE, 1);
			result = p1.sinh();
			return new Value(result);
		}
	}

	class FunctionAsin extends Function {
		/**
		 * @see Function#Function()
		 */
		public FunctionAsin() {
			super(1);
		}
		/**
		 * @see Function#result(Token[])
		 */
		@Override
		public Value result(Token param[]) throws ParseException {
			JNumber p1;
			JNumber result;
			p1 = getNumber(param[0], Token.VALUE | Token.VARIABLE, 1);
			try {
				result = p1.asin();
			} catch (JNumberException ex) {
				throw new ParseException(ex.getMessage());
			}
			return new Value(result);
		}
	}

	class FunctionCos extends Function {
		/**
		 * @see Function#Function()
		 */
		public FunctionCos() {
			super(1);
		}
		/**
		 * @see Function#result(Token[])
		 */
		@Override
		public Value result(Token param[]) throws ParseException {
			JNumber p1;
			JNumber result;
			p1 = getNumber(param[0], Token.VALUE | Token.VARIABLE, 1);
			result = p1.cos();
			return new Value(result);
		}
	}

	class FunctionCosH extends Function {
		/**
		 * @see Function#Function()
		 */
		public FunctionCosH() {
			super(1);
		}
		/**
		 * @see Function#result(Token[])
		 */
		@Override
		public Value result(Token param[]) throws ParseException {
			JNumber p1;
			JNumber result;
			p1 = getNumber(param[0], Token.VALUE | Token.VARIABLE, 1);
			result = p1.cosh();
			return new Value(result);
		}
	}

	class FunctionAcos extends Function {
		/**
		 * @see Function#Function()
		 */
		public FunctionAcos() {
			super(1);
		}
		/**
		 * @see Function#result(Token[])
		 */
		@Override
		public Value result(Token param[]) throws ParseException {
			JNumber p1;
			JNumber result;
			p1 = getNumber(param[0], Token.VALUE | Token.VARIABLE, 1);
			try {
				result = p1.acos();
			} catch (JNumberException ex) {
				throw new ParseException(ex.getMessage());
			}
			return new Value(result);
		}
	}

	class FunctionTan extends Function {
		/**
		 * @see Function#Function()
		 */
		public FunctionTan() {
			super(1);
		}
		/**
		 * @see Function#result(Token[])
		 */
		@Override
		public Value result(Token param[]) throws ParseException {
			JNumber p1;
			JNumber result;
			p1 = getNumber(param[0], Token.VALUE | Token.VARIABLE, 1);
			result = p1.tan();
			return new Value(result);
		}
	}

	class FunctionTanH extends Function {
		/**
		 * @see Function#Function()
		 */
		public FunctionTanH() {
			super(1);
		}
		/**
		 * @see Function#result(Token[])
		 */
		@Override
		public Value result(Token param[]) throws ParseException {
			JNumber p1;
			JNumber result;
			p1 = getNumber(param[0], Token.VALUE | Token.VARIABLE, 1);
			result = p1.tanh();
			return new Value(result);
		}
	}

	class FunctionAtan extends Function {
		/**
		 * @see Function#Function()
		 */
		public FunctionAtan() {
			super(1);
		}
		/**
		 * @see Function#result(Token[])
		 */
		@Override
		public Value result(Token param[]) throws ParseException {
			JNumber p1;
			JNumber result;
			p1 = getNumber(param[0], Token.VALUE | Token.VARIABLE, 1);
			result = p1.atan();
			return new Value(result);
		}
	}

	class FunctionLog extends Function {
		/**
		 * @see Function#Function()
		 */
		public FunctionLog() {
			super(1);
		}
		/**
		 * @see Function#result(Token[])
		 */
		@Override
		public Value result(Token param[]) throws ParseException {
			JNumber p1;
			JNumber result;
			p1 = getNumber(param[0], Token.VALUE | Token.VARIABLE, 1);
			result = p1.log();
			return new Value(result);
		}
	}

	class FunctionLog2 extends Function {
		/**
		 * @see Function#Function()
		 */
		public FunctionLog2() {
			super(1);
		}
		/**
		 * @see Function#result(Token[])
		 */
		@Override
		public Value result(Token param[]) throws ParseException {
			JNumber p1;
			JNumber result;
			p1 = getNumber(param[0], Token.VALUE | Token.VARIABLE, 1);
			result = p1.ld();
			return new Value(result);
		}
	}

	class FunctionLog10 extends Function {
		/**
		 * @see Function#Function()
		 */
		public FunctionLog10() {
			super(1);
		}
		/**
		 * @see Function#result(Token[])
		 */
		@Override
		public Value result(Token param[]) throws ParseException {
			JNumber p1;
			JNumber result;
			p1 = getNumber(param[0], Token.VALUE | Token.VARIABLE, 1);
			result = p1.log10();
			return new Value(result);
		}
	}

	class FunctionExp extends Function {
		/**
		 * @see Function#Function()
		 */
		public FunctionExp() {
			super(1);
		}
		/**
		 * @see Function#result(Token[])
		 */
		@Override
		public Value result(Token param[]) throws ParseException {
			JNumber p1;
			JNumber result;
			p1 = getNumber(param[0], Token.VALUE | Token.VARIABLE, 1);
			result = p1.exp();
			return new Value(result);
		}
	}


	class FunctionNumBits extends Function {
		/**
		 * @see Function#Function()
		 */
		public FunctionNumBits() {
			super(1);
		}
		/**
		 * @see Function#result(Token[])
		 */
		@Override
		public Value result(Token param[]) throws ParseException {
			JNumber p1;
			int result;
			p1 = getInteger(param[0], Token.VALUE | Token.VARIABLE, 1);
			result = p1.numBits();
			return new Value(new JNumber(result));
		}
	}

	class FunctionPrintHex extends Function {
		/**
		 * @see Function#Function()
		 */
		public FunctionPrintHex() {
			super(1);
		}
		/**
		 * @see Function#result(Token[])
		 */
		@Override
		public Value result(Token param[]) throws ParseException {
			JNumber p1;
			Value result;
			p1 = getInteger(param[0], Token.VALUE | Token.VARIABLE, 1);
			String out = "0x"+p1.toHexString();
			result = new Value(out);
			result.type = Value.NUMSTRING;
			return result;
		}
	}

	class FunctionPrintOct extends Function {
		/**
		 * @see Function#Function()
		 */
		public FunctionPrintOct() {
			super(1);
		}
		/**
		 * @see Function#result(Token[])
		 */
		@Override
		public Value result(Token param[]) throws ParseException {
			JNumber p1;
			Value result;
			p1 = getInteger(param[0], Token.VALUE | Token.VARIABLE, 1);
			String out = "0"+p1.toOctalString();
			result = new Value(out);
			result.type = Value.NUMSTRING;
			return result;
		}
	}


	class FunctionPrintFraction extends Function {
		/**
		 * @see Function#Function()
		 */
		public FunctionPrintFraction() {
			super(1);
		}
		/**
		 * @see Function#result(Token[])
		 */
		@Override
		public Value result(Token param[]) throws ParseException {
			JNumber p1;
			Value result;
			p1 = getNumber(param[0], Token.VALUE | Token.VARIABLE, 1);
			result = new Value(p1.toFraction());
			result.type = Value.NUMSTRING;
			return result;
		}
	}

	class FunctionPrintFull extends Function {
		/**
		 * @see Function#Function()
		 */
		public FunctionPrintFull() {
			super(1);
		}
		/**
		 * @see Function#result(Token[])
		 */
		@Override
		public Value result(Token param[]) throws ParseException {
			JNumber p1;
			Value result;
			p1 = getNumber(param[0], Token.VALUE | Token.VARIABLE, 1);
			String out = p1.toFullString();
			result = new Value(out);
			result.type = Value.NUMSTRING;
			return result;
		}
	}


	class FunctionPrintBinary extends Function {
		/**
		 * @see Function#Function()
		 */
		public FunctionPrintBinary() {
			super(1);
		}
		/**
		 * @see Function#result(Token[])
		 */
		@Override
		public Value result(Token param[]) throws ParseException {
			JNumber p1;
			Value result;
			p1 = getInteger(param[0], Token.VALUE | Token.VARIABLE, 1);
			String out = "0b"+p1.toBinaryString();
			result = new Value(out);
			result.type = Value.NUMSTRING;
			return result;
		}
	}

	class FunctionFillString extends Function {
		/**
		 * @see Function#Function()
		 * fill (object, value, size)
		 */
		public FunctionFillString() {
			super(2);
		}
		/**
		 * @see Function#result(Token[])
		 */
		@Override
		public Value result(Token param[]) throws ParseException {
			String s = getString(param[0], Token.VALUE | Token.VARIABLE, 1);
			int size = getUInt(param[1], Token.VALUE | Token.VARIABLE, 2);
			StringBuffer sb = new StringBuffer();
			while (sb.length() < size)
				sb.append(s);
			return new Value(sb.substring(0,size));
		}
	}


	class FunctionFillList extends Function {
		/**
		 * @see Function#Function()
		 * fill (object, value, size)
		 */
		public FunctionFillList() {
			super(2);
		}
		/**
		 * @see Function#result(Token[])
		 */
		@Override
		public Value result(Token param[]) throws ParseException {
			Value v = getValue(param[0], Token.VALUE | Token.VARIABLE, 1);
			int size = getUInt(param[1], Token.VALUE | Token.VARIABLE, 2);
			ValueList jl = new ValueList();
			if (v.type == Value.LIST)
				v.value = ((ValueList)v.value).clone(); // clone to make sure
			while (jl.size() < size)
				jl.add(new Value(v));
			return new Value(jl);
		}
	}

	class FunctionSize extends Function {
		/**
		 * @see Function#Function()
		 * size (object)
		 */
		public FunctionSize() {
			super(1);
		}
		/**
		 * @see Function#result(Token[])
		 */
		@Override
		public Value result(Token param[]) throws ParseException {
			Value p1 = getValue(param[0], Token.VARIABLE, 1);
			int size;
			switch (p1.type) {
				case Value.LIST:
					size = ((ValueList)p1.value).size();;
					break;
				case Value.STRING:
					size = ((String)p1.value).length();;
					break;
				default:
					size = 1;
			}
			return new Value(new JNumber(size));
		}
	}


	class FunctionPrint extends Function {
		/**
		 * @see Function#Function()
		 */
		public FunctionPrint() {
			super(1);
		}
		/**
		 * @see Function#result(Token[])
		 */
		@Override
		public Value result(Token param[]) throws ParseException {
			Value p1;
			p1 = getValue(param[0], Token.VALUE | Token.VARIABLE, 1);
			if (p1.type == Value.STRING)
				output((String)p1.value);
			else
				output(p1.toString());
			return new Value(p1);
		}
	}

	class FunctionSub extends Function {
		/**
		 * @see Function#Function()
		 * fill (object, value, size)
		 */
		public FunctionSub() {
			super(3);
		}
		/**
		 * @see Function#result(Token[])
		 */
		@Override
		public Value result(Token param[]) throws ParseException {
			Value p1 = getValue(param[0], Token.VALUE | Token.VARIABLE, 1);
			int from = getUInt(param[1], Token.VALUE | Token.VARIABLE, 2);
			int len = getUInt(param[2], Token.VALUE | Token.VARIABLE, 3);
			Value v;
			switch (p1.type) {
				case Value.LIST: {
					if (from+len > ((ValueList)p1.value).size())
						throw new ParseException("Index exceeds size.");
					ValueList jl = new ValueList(((ValueList)p1.value).subList(from, from+len));
					v = new Value(jl.clone()); // deep copy
					break; }
				case Value.STRING: {
					if (from+len > ((String)p1.value).length())
						throw new ParseException("Index exceeds size.");
					String s = ((String)p1.value).substring(from, from+len);
					v = new Value(s); // string is immutable
					break; }
				default:
					throw new ParseException("Only allowed on Strings and lists.");
			}
			return v;
		}
	}

	class FunctionInsert extends Function {
		/**
		 * @see Function#Function()
		 * fill (object, value, size)
		 */
		public FunctionInsert() {
			super(3);
		}
		/**
		 * @see Function#result(Token[])
		 */
		@Override
		public Value result(Token param[]) throws ParseException {
			Value p1 = getValue(param[0], Token.VALUE | Token.VARIABLE, 1);
			Value p2 = getValue(param[1], Token.VALUE | Token.VARIABLE, 2);
			int from = getUInt(param[2], Token.VALUE | Token.VARIABLE, 3);
			Value v;
			if (p1.type != p2.type)
				throw new ParseException("Parameters 1 and 2 have to be of the same type (list or string).");
			switch (p1.type) {
				case Value.LIST: {
					ValueList container = ((ValueList)p1.value).clone(); // deep copy of container
					if (from > container.size())
						throw new ParseException("Index exceeds size.");
					ValueList insert = ((ValueList)p2.value).clone(); // deep copy of list to insert
					container.addAll(from, insert);
					v = new Value(container); // shallow copy since already copied deeply
					break; }
				case Value.STRING: {
					StringBuffer sb = new StringBuffer((String)p1.value);
					if (from > sb.length())
						throw new ParseException("Index exceeds size.");
					sb.insert(from,(String)p2.value);
					v = new Value(sb.toString());
					break; }
				default:
					throw new ParseException("Only allowed on Strings and lists.");
			}
			return v;
		}
	}

	class FunctionIndexOf extends Function {
		/**
		 * @see Function#Function()
		 * fill (object, value, size)
		 */
		public FunctionIndexOf() {
			super(2);
		}
		/**
		 * @see Function#result(Token[])
		 */
		@Override
		public Value result(Token param[]) throws ParseException {
			Value p1 = getValue(param[0], Token.VALUE | Token.VARIABLE, 1);
			Value p2 = getValue(param[1], Token.VALUE | Token.VARIABLE, 2);
			Value v;
			switch (p1.type) {
				case Value.LIST: {
					v = new Value( new JNumber( ((ValueList)p1.value).indexOf(p2)  ));
					break; }
				case Value.STRING: {
					if (p1.type != p2.type)
						throw new ParseException("Parameters 1 and 2 have to be of the same type.");
					v = new Value( new JNumber( ((String)p1.value).indexOf((String)p2.value)  ));
					break; }
				default:
					throw new ParseException("Only allowed on Strings and lists.");
			}
			return v;
		}
	}

	class FunctionLastIndexOf extends Function {
		/**
		 * @see Function#Function()
		 * fill (object, value, size)
		 */
		public FunctionLastIndexOf() {
			super(2);
		}
		/**
		 * @see Function#result(Token[])
		 */
		@Override
		public Value result(Token param[]) throws ParseException {
			Value p1 = getValue(param[0], Token.VALUE | Token.VARIABLE, 1);
			Value p2 = getValue(param[1], Token.VALUE | Token.VARIABLE, 2);
			Value v;
			switch (p1.type) {
				case Value.LIST: {
					v = new Value( new JNumber( ((ValueList)p1.value).lastIndexOf(p2)  ));
					break; }
				case Value.STRING: {
					if (p1.type != p2.type)
						throw new ParseException("Parameters 1 and 2 have to be of the same type.");
					v = new Value( new JNumber( ((String)p1.value).lastIndexOf((String)p2.value)  ));
					break; }
				default:
					throw new ParseException("Only allowed on Strings and lists.");
			}
			return v;
		}
	}


	class FunctionReplace extends Function {
		/**
		 * @see Function#Function()
		 * fill (object, value, size)
		 */
		public FunctionReplace() {
			super(3);
		}
		/**
		 * @see Function#result(Token[])
		 */
		@Override
		public Value result(Token param[]) throws ParseException {
			Value p1 = getValue(param[0], Token.VALUE | Token.VARIABLE, 1);
			Value p2 = getValue(param[1], Token.VALUE | Token.VARIABLE, 2);
			int from = getUInt(param[2], Token.VALUE | Token.VARIABLE, 3);
			Value v;
			int len;
			if (p1.type != p2.type)
				throw new ParseException("Parameters 1 and 2 have to be of the same type (list or string).");
			switch (p1.type) {
				case Value.LIST: {
					ValueList container = ((ValueList)p1.value).clone(); // deep copy of container
					ValueList replace = ((ValueList)p2.value).clone(); // deep copy of objects to insert
					len = replace.size();
					if (from+len > container.size())
						throw new ParseException("Index exceeds size.");
					for (int i = from; i<from+len; i++)
						container.set(i,replace.get(i-from));
					v = new Value(container); // shallow copy since already copied deeply
					break; }
				case Value.STRING: {
					StringBuffer sb = new StringBuffer((String)p1.value);
					len = ((String)p2.value).length();
					if (from+len > sb.length())
						throw new ParseException("Index exceeds size.");
					sb.replace(from,from+len,(String)p2.value);
					v = new Value(sb.toString());
					break; }
				default:
					throw new ParseException("Only allowed on Strings and lists.");
			}
			return v;
		}
	}

	class FunctionPrintLn extends Function {
		/**
		 * @see Function#Function()
		 */
		public FunctionPrintLn() {
			super(1);
		}
		/**
		 * @see Function#result(Token[])
		 */
		@Override
		public Value result(Token param[]) throws ParseException {
			Value p1;
			p1 = getValue(param[0], Token.VALUE | Token.VARIABLE, 1);
			if (p1.type == Value.STRING)
				output((String)p1.value);
			else
				output(p1.toString());
			output("\n");
			return new Value(p1);
		}
	}

	class FunctionTime extends Function {
		/**
		 * @see Function#Function()
		 */
		public FunctionTime() {
			super(0);
		}
		/**
		 * @see Function#result(Token[])
		 */
		@Override
		public Value result(Token param[]) throws ParseException {
			Date date = new Date(); // maybe put into global context
			return new Value(new JNumber(date.getTime()));
		}
	}

	class FunctionDie extends Function {
		/**
		 * @see Function#Function()
		 */
		public FunctionDie() {
			super(1);
		}
		/**
		 * @see Function#result(Token[])
		 */
		@Override
		public Value result(Token param[]) throws ParseException {
			String p1;
			p1 = getString(param[0], Token.VALUE | Token.VARIABLE, 1);
			throw new ParseException("User Exception: "+p1);
		}
	}

	class FunctionDefined extends Function {
		/**
		 * @see Function#Function()
		 */
		public FunctionDefined() {
			super(1);
		}
		/**
		 * @see Function#result(Token[])
		 */
		@Override
		public Value result(Token param[]) throws ParseException {
			try {
				getValue(param[0], Token.VARIABLE, 1);
				return new Value(true);
			} catch (ParseException ex) {
				return new Value(false);
			}
		}
	}

	class FunctionLoad extends Function {
		/**
		 * @see Function#Function()
		 */
		public FunctionLoad() {
			super(1);
		}
		/**
		 * @see Function#result(Token[])
		 */
		@Override
		public Value result(Token param[]) throws ParseException {
			String p1;
			Value retVal;
			p1 = scriptPath+"\\"+getString(param[0], Token.VALUE | Token.VARIABLE, 1);
			BufferedReader in=null;
			// Load
			try {
				String buffer;
				StringBuffer text = new StringBuffer();
				in = new BufferedReader(new FileReader(p1));
				errorFiles.push(errorFile);
				errorFile = "FILE:"+p1;
				for (;;) {
					buffer = in.readLine();
					if (buffer != null) {
						text.append(buffer);
						text.append("\n");
					}else
						break;
				}
				in.close();
				retVal = evaluate(text.toString());
				errorFile = errorFiles.pop();
				return retVal;
			} catch (FileNotFoundException ex) {
				throw new ParseException("File not found: "+p1);
			} catch (IOException ex) {
				throw new ParseException("IO Exception while reading: "+p1);
			} finally { try {if (in!=null)in.close();} catch (IOException ex) {}  }
		}
	}

	class FunctionEval extends Function {
		/**
		 * @see Function#Function()
		 */
		public FunctionEval() {
			super(1);
		}
		/**
		 * @see Function#result(Token[])
		 */
		@Override
		public Value result(Token param[]) throws ParseException {
			String p1;
			p1 = getString(param[0], Token.VALUE | Token.VARIABLE, 1);
			return evaluate(p1);
		}
	}

	class FunctionClear extends Function {
		/**
		 * @see Function#Function()
		 */
		public FunctionClear() {
			super(0);
		}
		/**
		 * @see Function#result(Token[])
		 */
		@Override
		public Value result(Token param[]) throws ParseException {
			variables.clear();
			userFunctions.clear();
			addDefaultConstants();
			return new Value();
		}
	}

	class FunctionListVar extends Function {
		/**
		 * @see Function#Function()
		 */
		public FunctionListVar() {
			super(0);
		}
		/**
		 * @see Function#result(Token[])
		 */
		@Override
		public Value result(Token param[]) throws ParseException {
			ArrayList<String> vars = variables.getList();
			ValueList jl = new ValueList();
			for (int i=0; i<vars.size();i++)
				jl.add(new Value(vars.get(i)));
			Value v = new Value();
			v.type = Value.LIST;
			v.value = jl;
			return v;
		}
	}

	class FunctionListFunctions extends Function {
		/**
		 * @see Function#Function()
		 */
		public FunctionListFunctions() {
			super(0);
		}
		/**
		 * @see Function#result(Token[])
		 */
		@Override
		public Value result(Token param[]) throws ParseException {
			ArrayList<String> fons = userFunctions.getList();
			ValueList jl = new ValueList();
			for (int i=0; i<fons.size();i++)
				jl.add(new Value(fons.get(i)));
			Value v = new Value();
			v.type = Value.LIST;
			v.value = jl;
			return v;
		}
	}

	/*
	 * inner operator classes
	 */
	class OperatorDivide extends Operator {
		/**
		 * Default ctor
		 * @param precedence_ Precedence of operator (smaller number == higher precedence)
		 */
		public OperatorDivide(int precedence_) {
			super(Operator.BINARY_LEFT, precedence_);
		}

		/**
		 * @see Operator#result(Token[])
		 */
		@Override
		public Value result (Token operand[]) throws ParseException {
			// works on values
			JNumber left, right;
			Value result;
			try {
				left  = getNumber(operand[0],Token.VALUE|Token.VARIABLE,OP_LEFT);
				right = getNumber(operand[1],Token.VALUE|Token.VARIABLE,OP_RIGHT);
				result = new Value(left.divide(right));
			} catch(ArithmeticException ex) {
				throw new ParseException("Division by Zero");
			}
			return result;
		}
	}

	class OperatorDivideSet extends Operator {
		/**
		 * Default ctor
		 * @param precedence_ Precedence of operator (smaller number == higher precedence)
		 */
		public OperatorDivideSet(int precedence_) {
			super(Operator.BINARY_RIGHT, precedence_);
		}

		/**
		 * @see Operator#result(Token[])
		 */
		@Override
		public Value result (Token operand[]) throws ParseException {
			JNumber left,right;
			Value result;
			// left side variable, right side value
			left  = getNumber(operand[0],Token.VARIABLE,OP_LEFT);
			right = getNumber(operand[1],Token.VALUE|Token.VARIABLE,OP_RIGHT);
			try {
				result = new Value(left.divide(right));
				variables.putValue(operand[0].name,result);
				return new Value(result);
			} catch(ArithmeticException ex) {
				throw new ParseException("Division by Zero");
			}
		}
	}

	class OperatorMultiplySet extends Operator {
		/**
		 * Default ctor
		 * @param precedence_ Precedence of operator (smaller number == higher precedence)
		 */
		public OperatorMultiplySet(int precedence_) {
			super(Operator.BINARY_LEFT, precedence_);
		}

		/**
		 * @see Operator#result(Token[])
		 */
		@Override
		public Value result (Token operand[]) throws ParseException {
			JNumber left,right;
			Value result;
			// left side variable, right side value
			left  = getNumber(operand[0],Token.VARIABLE,OP_LEFT);
			right = getNumber(operand[1],Token.VALUE|Token.VARIABLE,OP_RIGHT);
			result = new Value(left.multiply(right));
			variables.putValue(operand[0].name,result);
			return new Value(result);
		}
	}

	class OperatorAddSet extends Operator {
		/**
		 * Default ctor
		 * @param precedence_ Precedence of operator (smaller number == higher precedence)
		 */
		public OperatorAddSet(int precedence_) {
			super(Operator.BINARY_LEFT, precedence_);
		}

		/**
		 * @see Operator#result(Token[])
		 */
		@Override
		public Value result (Token operand[]) throws ParseException {
			Value left, result;
			left  = getValue(operand[0],Token.VALUE|Token.VARIABLE,OP_LEFT);
			switch (left.type) {
				case Value.NUMBER:
					JNumber lnum,rnum;
					lnum = getNumber(operand[0],Token.VALUE|Token.VARIABLE,OP_LEFT);
					rnum = getNumber(operand[1],Token.VALUE|Token.VARIABLE,OP_RIGHT);
					result =  new Value(lnum.add(rnum));
					break;
				case Value.STRING:
					String ls,rs;
					ls = getString(operand[0],Token.VALUE|Token.VARIABLE,OP_LEFT);
					rs = getString(operand[1],Token.VALUE|Token.VARIABLE,OP_RIGHT);
					result = new Value(ls+rs);
					break;
				case Value.LIST:
					ValueList ll,rl,ret;
					ll = getList(operand[0],Token.VALUE|Token.VARIABLE,OP_LEFT);
					rl = getList(operand[1],Token.VALUE|Token.VARIABLE,OP_RIGHT);
					ret = new ValueList(ll); // first a shallow copy
					ret.addAll(rl);      // now the list is complete
					result = new Value(ret.clone()); // return deep copy
					break;
				default:
					throw new ParseException("Invalid value type at left side of operator");
			}
			variables.putValue(operand[0].name,result);
			return new Value(result);

		}
	}

	class OperatorSubtractSet extends Operator {
		/**
		 * Default ctor
		 * @param precedence_ Precedence of operator (smaller number == higher precedence)
		 */
		public OperatorSubtractSet(int precedence_) {
			super(Operator.BINARY_LEFT, precedence_);
		}

		/**
		 * @see Operator#result(Token[])
		 */
		@Override
		public Value result (Token operand[]) throws ParseException {
			JNumber left,right;
			Value result;
			// left side variable, right side value
			left  = getNumber(operand[0],Token.VARIABLE,OP_LEFT);
			right = getNumber(operand[1],Token.VALUE|Token.VARIABLE,OP_RIGHT);
			result = new Value(left.subtract(right));
			variables.putValue(operand[0].name,result);
			return new Value(result);
		}
	}

	class OperatorShiftLeftSet extends Operator {
		/**
		 * Default ctor
		 * @param precedence_ Precedence of operator (smaller number == higher precedence)
		 */
		public OperatorShiftLeftSet(int precedence_) {
			super(Operator.BINARY_LEFT, precedence_);
		}

		/**
		 * @see Operator#result(Token[])
		 */
		@Override
		public Value result (Token operand[]) throws ParseException {
			JNumber left,right;
			Value result;
			// left side variable, right side value
			left  = getInteger(operand[0],Token.VARIABLE,OP_LEFT);
			right = getInteger(operand[1],Token.VALUE|Token.VARIABLE,OP_RIGHT);
			try {
				result = new Value(left.shiftLeft(right));
				variables.putValue(operand[0].name,result);
				return new Value(result);
			} catch(JNumberException ex) {
				throw new ParseException(ex.getMessage());
			}
		}
	}

	class OperatorShiftRightSet extends Operator {
		/**
		 * Default ctor
		 * @param precedence_ Precedence of operator (smaller number == higher precedence)
		 */
		public OperatorShiftRightSet(int precedence_) {
			super(Operator.BINARY_LEFT, precedence_);
		}

		/**
		 * @see Operator#result(Token[])
		 */
		@Override
		public Value result (Token operand[]) throws ParseException {
			JNumber left,right;
			Value result;
			// left side variable, right side value
			left  = getInteger(operand[0],Token.VARIABLE,OP_LEFT);
			right = getInteger(operand[1],Token.VALUE|Token.VARIABLE,OP_RIGHT);
			try {
				result = new Value(left.shiftRight(right));
				variables.putValue(operand[0].name,result);
				return new Value(result);
			} catch(JNumberException ex) {
				throw new ParseException(ex.getMessage());
			}
		}
	}

	class OperatorOrSet extends Operator {
		/**
		 * Default ctor
		 * @param precedence_ Precedence of operator (smaller number == higher precedence)
		 */
		public OperatorOrSet(int precedence_) {
			super(Operator.BINARY_LEFT, precedence_);
		}

		/**
		 * @see Operator#result(Token[])
		 */
		@Override
		public Value result (Token operand[]) throws ParseException {
			Value left, right, result;
			left  = getValue(operand[0],Token.VARIABLE,OP_LEFT);
			right = getValue(operand[1],Token.VALUE|Token.VARIABLE,OP_RIGHT);
			switch (left.type) {
				case Value.NUMBER:
					if (right.type != Value.NUMBER || !((JNumber)left.value).isInteger())
						throw new ParseException("Only defined on integer values");
					try {
						result = new Value(((JNumber)left.value).or((JNumber)right.value));
					} catch(JNumberException ex) {
						throw new ParseException(ex.getMessage());
					}
					break;
				case Value.BOOL:
					if (right.type != Value.BOOL)
						throw new ParseException("Right side must be boolean");
					result = new Value(((Boolean)left.value).booleanValue() | ((Boolean)right.value).booleanValue());
					break;
				default:
					throw new ParseException("Operands must be either both boolean or both integer");
			}
			variables.putValue(operand[0].name,result);
			return new Value(result);
		}
	}

	class OperatorAndSet extends Operator {
		/**
		 * Default ctor
		 * @param precedence_ Precedence of operator (smaller number == higher precedence)
		 */
		public OperatorAndSet(int precedence_) {
			super(Operator.BINARY_LEFT, precedence_);
		}
		/**
		 * @see Operator#result(Token[])
		 */
		@Override
		public Value result (Token operand[]) throws ParseException {
			Value left, right, result;
			left  = getValue(operand[0],Token.VARIABLE,OP_LEFT);
			right = getValue(operand[1],Token.VALUE|Token.VARIABLE,OP_RIGHT);
			switch (left.type) {
				case Value.NUMBER:
					if (right.type != Value.NUMBER || !((JNumber)left.value).isInteger())
						throw new ParseException("Only defined on integer values");
					try {
						result = new Value(((JNumber)left.value).and((JNumber)right.value));
					} catch(JNumberException ex) {
						throw new ParseException(ex.getMessage());
					}
					break;
				case Value.BOOL:
					if (right.type != Value.BOOL)
						throw new ParseException("Right side must be boolean");
					result = new Value(((Boolean)left.value).booleanValue() & ((Boolean)right.value).booleanValue());
					break;
				default:
					throw new ParseException("Operands must be either both boolean or both integer");
			}
			variables.putValue(operand[0].name,result);
			return new Value(result);
		}
	}

	class OperatorXorSet extends Operator {
		/**
		 * Default ctor
		 * @param precedence_ Precedence of operator (smaller number == higher precedence)
		 */
		public OperatorXorSet(int precedence_) {
			super(Operator.BINARY_LEFT, precedence_);
		}

		/**
		 * @see Operator#result(Token[])
		 */
		@Override
		public Value result (Token operand[]) throws ParseException {
			Value left, right, result;
			left  = getValue(operand[0],Token.VARIABLE,OP_LEFT);
			right = getValue(operand[1],Token.VALUE|Token.VARIABLE,OP_RIGHT);
			switch (left.type) {
				case Value.NUMBER:
					if (right.type != Value.NUMBER || !((JNumber)left.value).isInteger())
						throw new ParseException("Only defined on integer values");
					try {
						result = new Value(((JNumber)left.value).xor((JNumber)right.value));
					} catch(JNumberException ex) {
						throw new ParseException(ex.getMessage());
					}
					break;
				case Value.BOOL:
					if (right.type != Value.BOOL)
						throw new ParseException("Right side must be boolean");
					result = new Value(((Boolean)left.value).booleanValue() ^ ((Boolean)right.value).booleanValue());
					break;
				default:
					throw new ParseException("Operands must be either both boolean or both integer");
			}
			variables.putValue(operand[0].name,result);
			return new Value(result);
		}
	}

	class OperatorModSet extends Operator {
		/**
		 * Default ctor
		 * @param precedence_ Precedence of operator (smaller number == higher precedence)
		 */
		public OperatorModSet(int precedence_) {
			super(Operator.BINARY_LEFT, precedence_);
		}

		/**
		 * @see Operator#result(Token[])
		 */
		@Override
		public Value result (Token operand[]) throws ParseException {
			JNumber left,right;
			Value result;
			// left side variable, right side value
			left  = getInteger(operand[0],Token.VARIABLE,OP_LEFT);
			right = getInteger(operand[1],Token.VALUE|Token.VARIABLE,OP_RIGHT);
			try {
				result = new Value(left.mod(right));
				variables.putValue(operand[0].name,result);
				return new Value(result);
			} catch(ArithmeticException ex) {
				throw new ParseException("Division by Zero");
			} catch(JNumberException ex) {
				throw new ParseException(ex.getMessage());
			}
		}
	}

	class OperatorMod extends Operator {
		/**
		 * Default ctor
		 * @param precedence_ Precedence of operator (smaller number == higher precedence)
		 */
		public OperatorMod(int precedence_) {
			super(Operator.BINARY_LEFT, precedence_);
		}

		/**
		 * @see Operator#result(Token[])
		 */
		@Override
		public Value result (Token operand[]) throws ParseException {
			// works on values
			JNumber left, right;
			Value result;
			try {
				left  = getInteger(operand[0],Token.VALUE|Token.VARIABLE,OP_LEFT);
				right = getInteger(operand[1],Token.VALUE|Token.VARIABLE,OP_RIGHT);
				result = new Value(left.mod(right));
			}
			catch(ArithmeticException ex) {
				throw new ParseException("Division by Zero");
			}
			catch(JNumberException ex) {
				throw new ParseException(ex.getMessage());
			}
			return result;
		}
	}

	class OperatorMinusBinary extends Operator {
		/**
		 * Default ctor
		 * @param precedence_ Precedence of operator (smaller number == higher precedence)
		 */
		OperatorMinusBinary(int precedence_) {
			super(Operator.BINARY_LEFT, precedence_);
		}

		/**
		 * @see Operator#result(Token[])
		 */
		@Override
		public Value result (Token operand[])  throws ParseException {
			// works on values
			JNumber left, right;
			left  = getNumber(operand[0],Token.VALUE|Token.VARIABLE,OP_LEFT);
			right = getNumber(operand[1],Token.VALUE|Token.VARIABLE,OP_RIGHT);
			return new Value(left.subtract(right));
		}
	}

	class OperatorMinusMinusUnaryL extends Operator {
		/**
		 * Default ctor
		 * @param precedence_ Precedence of operator (smaller number == higher precedence)
		 */
		public OperatorMinusMinusUnaryL(int precedence_) {
			super(Operator.UNARY_LEFT,precedence_);
		}

		/**
		 * @see Operator#result(Token[])
		 */
		@Override
		public Value result (Token operand[])  throws ParseException {
			// only works on variables
			JNumber op, result;
			op = getNumber(operand[0],Token.VARIABLE,OP_LEFT);
			// change variable, but not the result!
			result = new JNumber(op);
			op.subtract(JNumber.ONE);
			variables.putNumber(operand[0].name,op);
			return new Value(result);
		}
	}

	class OperatorMinusMinusUnaryR extends Operator {
		/**
		 * Default ctor
		 * @param precedence_ Precedence of operator (smaller number == higher precedence)
		 */
		public OperatorMinusMinusUnaryR(int precedence_) {
			super(Operator.UNARY_RIGHT,precedence_);
		}

		/**
		 * @see Operator#result(Token[])
		 */
		@Override
		public Value result (Token operand[]) throws ParseException {
			// only works on variables
			JNumber op;
			op = getNumber(operand[0],Token.VARIABLE,OP_RIGHT);
			// change variable and the result!
			op.subtract(JNumber.ONE);
			variables.putNumber(operand[0].name,op);
			return new Value(op);
		}
	}

	class OperatorMinusUnary extends Operator {
		/**
		 * Default ctor
		 * @param precedence_ Precedence of operator (smaller number == higher precedence)
		 */
		OperatorMinusUnary(int precedence_) {
			super(Operator.UNARY_RIGHT,precedence_);
		}

		/**
		 * @see Operator#result(Token[])
		 */
		@Override
		public Value result (Token operand[]) throws ParseException {
			JNumber op;
			// works on values
			op = getNumber(operand[0],Token.VALUE|Token.VARIABLE,OP_RIGHT);
			return new Value(op.negate());
		}
	}

	class OperatorMultiply extends Operator {
		/**
		 * Default ctor
		 * @param precedence_ Precedence of operator (smaller number == higher precedence)
		 */
		public OperatorMultiply(int precedence_) {
			super(Operator.BINARY_LEFT, precedence_);
		}

		/**
		 * @see Operator#result(Token[])
		 */
		@Override
		public Value result (Token operand[]) throws ParseException {
			// works on values
			JNumber left, right;
			left  = getNumber(operand[0],Token.VALUE|Token.VARIABLE,OP_LEFT);
			right = getNumber(operand[1],Token.VALUE|Token.VARIABLE,OP_RIGHT);
			return new Value(left.multiply(right));
		}
	}

	class OperatorPlusBinary extends Operator {
		/**
		 * Default ctor
		 * @param precedence_ Precedence of operator (smaller number == higher precedence)
		 */
		public OperatorPlusBinary(int precedence_) {
			super(Operator.BINARY_LEFT,precedence_);
		}

		/**
		 * @see Operator#result(Token[])
		 */
		@Override
		public Value result (Token operand[]) throws ParseException {
			Value left;
			left  = getValue(operand[0],Token.VALUE|Token.VARIABLE,OP_LEFT);
			switch (left.type) {
				case Value.NUMBER:
					JNumber lnum,rnum;
					lnum = getNumber(operand[0],Token.VALUE|Token.VARIABLE,OP_LEFT);
					rnum = getNumber(operand[1],Token.VALUE|Token.VARIABLE,OP_RIGHT);
					return new Value(lnum.add(rnum));
				case Value.STRING:
					String ls,rs;
					ls = getString(operand[0],Token.VALUE|Token.VARIABLE,OP_LEFT);
					rs = getString(operand[1],Token.VALUE|Token.VARIABLE,OP_RIGHT);
					return new Value(ls+rs);
				case Value.LIST:
					ValueList ll,rl,ret;
					ll = getList(operand[0],Token.VALUE|Token.VARIABLE,OP_LEFT);
					rl = getList(operand[1],Token.VALUE|Token.VARIABLE,OP_RIGHT);
					ret = new ValueList(ll); // first a shallow copy
					ret.addAll(rl);      // now the list is complete
					return new Value(ret.clone()); // return deep copy

			}
			throw new ParseException("Invalid value type at left side of operator");
		}
	}

	class OperatorPlusPlusUnaryL extends Operator {
		/**
		 * Default ctor
		 * @param precedence_ Precedence of operator (smaller number == higher precedence)
		 */
		public OperatorPlusPlusUnaryL(int precedence_) {
			super(Operator.UNARY_LEFT,precedence_);
		}

		/**
		 * @see Operator#result(Token[])
		 */
		@Override
		public Value result (Token operand[]) throws ParseException {
			// only works on variables
			JNumber op, result;
			op = getNumber(operand[0],Token.VARIABLE,OP_LEFT);
			// change variable, but not the result!
			result = new JNumber(op);
			op.add(JNumber.ONE);
			variables.putNumber(operand[0].name,op);
			return new Value(result);
		}
	}

	class OperatorPlusPlusUnaryR extends Operator {
		/**
		 * Default ctor
		 * @param precedence_ Precedence of operator (smaller number == higher precedence)
		 */
		public OperatorPlusPlusUnaryR(int precedence_) {
			super(Operator.UNARY_RIGHT,precedence_);
		}

		/**
		 * @see Operator#result(Token[])
		 */
		@Override
		public Value result (Token operand[]) throws ParseException {
			// only works on variables
			JNumber op;
			op = getNumber(operand[0],Token.VARIABLE,OP_RIGHT);
			// change variable and the result!
			op.add(JNumber.ONE);
			variables.putNumber(operand[0].name,op);
			return new Value(op);
		}
	}

	class OperatorPlusUnary extends Operator {
		/**
		 * Default ctor
		 * @param precedence_ Precedence of operator (smaller number == higher precedence)
		 */
		OperatorPlusUnary(int precedence_) {
			super(Operator.UNARY_RIGHT,precedence_);
		}
		/**
		 * @see Operator#result(Token[])
		 */
		@Override
		public Value result (Token operand[]) throws ParseException {
			JNumber op;
			// works on values
			op = getNumber(operand[0],Token.VALUE|Token.VARIABLE,OP_RIGHT);
			return new Value(op);
		}
	}

	class OperatorLogicalComplement extends Operator {
		/**
		 * Default ctor
		 * @param precedence_ Precedence of operator (smaller number == higher precedence)
		 */
		OperatorLogicalComplement(int precedence_) {
			super(Operator.UNARY_RIGHT,precedence_);
		}
		/**
		 * @see Operator#result(Token[])
		 */
		@Override
		public Value result (Token operand[]) throws ParseException {
			Value op;
			// works on values
			op = getValue(operand[0],Token.VALUE|Token.VARIABLE,OP_RIGHT);
			switch (op.type) {
				case Value.NUMBER:
					// ok, this is C, not Java
					if (!((JNumber)op.value).isInteger())
						throw new ParseException("Operand must be integer or boolean");
					if ( ((JNumber)op.value).signum() == 0 )
						return new Value(JNumber.ONE);
					return new Value(JNumber.ZERO);
				case Value.BOOL:
					return new Value(!((Boolean)op.value).booleanValue());
				default:
					throw new ParseException("Operand must be integer or boolean");
			}
		}
	}

	class OperatorSet extends Operator {
		/**
		 * Default ctor
		 * @param precedence_ Precedence of operator (smaller number == higher precedence)
		 */
		OperatorSet(int precedence_) {
			super(Operator.BINARY_RIGHT, precedence_);
		}
		/**
		 * @see Operator#result(Token[])
		 */
		@Override
		public Value result (Token operand[]) throws ParseException {
			Value right;
			// left side variable, right side value
			if (operand[0].type != Token.VARIABLE)
				throw new ParseException("left side of = must be a variable");
			right = getValue(operand[1],Token.VALUE|Token.VARIABLE,OP_RIGHT);
			variables.putValue(operand[0].name,right);
			return new Value(right);
		}
	}

	class OperatorCastInteger extends Operator {
		/**
		 * Default ctor
		 * @param precedence_ Precedence of operator (smaller number == higher precedence)
		 */
		OperatorCastInteger(int precedence_) {
			super(Operator.UNARY_RIGHT,precedence_);
		}
		/**
		 * @see Operator#result(Token[])
		 */
		@Override
		public Value result (Token operand[]) throws ParseException {
			JNumber op;
			op = getNumber(operand[0],Token.VALUE|Token.VARIABLE,OP_RIGHT).trunc();
			return new Value(op);
		}
	}

	class OperatorCastU32 extends Operator {
		/**
		 * Default ctor
		 * @param precedence_ Precedence of operator (smaller number == higher precedence)
		 */
		OperatorCastU32(int precedence_) {
			super(Operator.UNARY_RIGHT,precedence_);
		}
		/**
		 * @see Operator#result(Token[])
		 */
		@Override
		public Value result (Token operand[]) throws ParseException {
			JNumber op;
			op = getNumber(operand[0],Token.VALUE|Token.VARIABLE,OP_RIGHT).trunc();
			op.limitUnsigned(MAX_U32);
			return new Value(op);
		}
	}

	class OperatorCastS32 extends Operator {
		/**
		 * Default ctor
		 * @param precedence_ Precedence of operator (smaller number == higher precedence)
		 */
		OperatorCastS32(int precedence_) {
			super(Operator.UNARY_RIGHT,precedence_);
		}
		/**
		 * @see Operator#result(Token[])
		 */
		@Override
		public Value result (Token operand[]) throws ParseException {
			JNumber op;
			op = getNumber(operand[0],Token.VALUE|Token.VARIABLE,OP_RIGHT).trunc();
			op.limitSigned(MAX_U32,MAX_S32, MIN_S32);
			return new Value(op);
		}
	}

	class OperatorCastU64 extends Operator {
		/**
		 * Default ctor
		 * @param precedence_ Precedence of operator (smaller number == higher precedence)
		 */
		OperatorCastU64(int precedence_) {
			super(Operator.UNARY_RIGHT,precedence_);
		}
		/**
		 * @see Operator#result(Token[])
		 */
		@Override
		public Value result (Token operand[]) throws ParseException {
			JNumber op;
			op = getNumber(operand[0],Token.VALUE|Token.VARIABLE,OP_RIGHT).trunc();
			op.limitUnsigned(MAX_U64);
			return new Value(op);
		}
	}

	class OperatorCastS64 extends Operator {
		/**
		 * Default ctor
		 * @param precedence_ Precedence of operator (smaller number == higher precedence)
		 */
		OperatorCastS64(int precedence_) {
			super(Operator.UNARY_RIGHT,precedence_);
		}
		/**
		 * @see Operator#result(Token[])
		 */
		@Override
		public Value result (Token operand[]) throws ParseException {
			JNumber op;
			op = getNumber(operand[0],Token.VALUE|Token.VARIABLE,OP_RIGHT).trunc();
			op.limitSigned(MAX_U64,MAX_S64, MIN_S64);
			return new Value(op);
		}
	}

	class OperatorCastU16 extends Operator {
		/**
		 * Default ctor
		 * @param precedence_ Precedence of operator (smaller number == higher precedence)
		 */
		OperatorCastU16(int precedence_) {
			super(Operator.UNARY_RIGHT,precedence_);
		}
		/**
		 * @see Operator#result(Token[])
		 */
		@Override
		public Value result (Token operand[]) throws ParseException {
			JNumber op;
			op = getNumber(operand[0],Token.VALUE|Token.VARIABLE,OP_RIGHT).trunc();
			op.limitUnsigned(MAX_U16);
			return new Value(op);
		}
	}

	class OperatorCastS16 extends Operator {
		/**
		 * Default ctor
		 * @param precedence_ Precedence of operator (smaller number == higher precedence)
		 */
		OperatorCastS16(int precedence_) {
			super(Operator.UNARY_RIGHT,precedence_);
		}
		/**
		 * @see Operator#result(Token[])
		 */
		@Override
		public Value result (Token operand[]) throws ParseException {
			JNumber op;
			op = getNumber(operand[0],Token.VALUE|Token.VARIABLE,OP_RIGHT).trunc();
			op.limitSigned(MAX_U16,MAX_S16, MIN_S16);
			return new Value(op);
		}
	}

	class OperatorCastU8 extends Operator {
		/**
		 * Default ctor
		 * @param precedence_ Precedence of operator (smaller number == higher precedence)
		 */
		OperatorCastU8(int precedence_) {
			super(Operator.UNARY_RIGHT,precedence_);
		}
		/**
		 * @see Operator#result(Token[])
		 */
		@Override
		public Value result (Token operand[]) throws ParseException {
			JNumber op;
			op = getNumber(operand[0],Token.VALUE|Token.VARIABLE,OP_RIGHT).trunc();
			op.limitUnsigned(MAX_U8);
			return new Value(op);
		}
	}

	class OperatorCastS8 extends Operator {
		/**
		 * Default ctor
		 * @param precedence_ Precedence of operator (smaller number == higher precedence)
		 */
		OperatorCastS8(int precedence_) {
			super(Operator.UNARY_RIGHT,precedence_);
		}
		/**
		 * @see Operator#result(Token[])
		 */
		@Override
		public Value result (Token operand[]) throws ParseException {
			JNumber op;
			op = getNumber(operand[0],Token.VALUE|Token.VARIABLE,OP_RIGHT).trunc();
			op.limitSigned(MAX_U8,MAX_S8, MIN_S8);
			return new Value(op);
		}
	}

	class OperatorCastDouble extends Operator {
		/**
		 * Default ctor
		 * @param precedence_ Precedence of operator (smaller number == higher precedence)
		 */
		OperatorCastDouble(int precedence_) {
			super(Operator.UNARY_RIGHT,precedence_);
		}
		/**
		 * @see Operator#result(Token[])
		 */
		@Override
		public Value result (Token operand[]) throws ParseException {
			double num;
			num = getNumber(operand[0],Token.VALUE|Token.VARIABLE,OP_RIGHT).toDouble();
			return new Value(new JNumber(num));
		}
	}

	class OperatorCastString extends Operator {
		/**
		 * Default ctor
		 * @param precedence_ Precedence of operator (smaller number == higher precedence)
		 */
		OperatorCastString(int precedence_) {
			super(Operator.UNARY_RIGHT,precedence_);
		}
		/**
		 * @see Operator#result(Token[])
		 */
		@Override
		public Value result (Token operand[]) throws ParseException {
			String s;
			//s = getValue(operand[0],Token.VALUE|Token.VARIABLE,OP_RIGHT).toString();
			Value p1 = getValue(operand[0], Token.VALUE | Token.VARIABLE, OP_RIGHT);
			if (p1.type == Value.STRING)
				s = (String)p1.value;
			else
				s = p1.toString();
			return new Value(s);
		}
	}

	class OperatorPow extends Operator {
		/**
		 * Default ctor
		 * @param precedence_ Precedence of operator (smaller number == higher precedence)
		 */
		public OperatorPow(int precedence_) {
			super(Operator.BINARY_RIGHT, precedence_);
		}
		/**
		 * @see Operator#result(Token[])
		 */
		@Override
		public Value result (Token operand[]) throws ParseException {
			JNumber left, right;
			left  = getNumber(operand[0],Token.VALUE|Token.VARIABLE,OP_LEFT);
			right = getNumber(operand[1],Token.VALUE|Token.VARIABLE,OP_RIGHT);
			try {
				return new Value(left.pow(right));
			} catch (JNumberException ex) {
				throw new ParseException(ex.getMessage());
			}
		}
	}

	class OperatorNot extends Operator {
		/**
		 * Default ctor
		 * @param precedence_ Precedence of operator (smaller number == higher precedence)
		 */
		OperatorNot(int precedence_) {
			super(Operator.UNARY_RIGHT,precedence_);
		}
		/**
		 * @see Operator#result(Token[])
		 */
		@Override
		public Value result (Token operand[]) throws ParseException {
			JNumber op;
			op = getInteger(operand[0],Token.VALUE|Token.VARIABLE,OP_RIGHT);
			try {
				return new Value(op.not());
			} catch (JNumberException ex) {
				throw new ParseException(ex.getMessage());
			}
		}
	}

	class OperatorOr extends Operator {
		/**
		 * Default ctor
		 * @param precedence_ Precedence of operator (smaller number == higher precedence)
		 */
		OperatorOr(int precedence_) {
			super(Operator.BINARY_LEFT,precedence_);
		}
		/**
		 * @see Operator#result(Token[])
		 */
		@Override
		public Value result (Token operand[]) throws ParseException {
			Value left, right;
			left  = getValue(operand[0],Token.VALUE|Token.VARIABLE,OP_LEFT);
			right = getValue(operand[1],Token.VALUE|Token.VARIABLE,OP_RIGHT);
			switch (left.type) {
				case Value.NUMBER:
					if (right.type != Value.NUMBER || !((JNumber)left.value).isInteger())
						throw new ParseException("Only defined on integer values");
					try {
						return new Value(((JNumber)left.value).or((JNumber)right.value));
					} catch(JNumberException ex) {
						throw new ParseException(ex.getMessage());
					}
				case Value.BOOL:
					if (right.type != Value.BOOL)
						throw new ParseException("Right side must be boolean");
					return new Value(((Boolean)left.value).booleanValue() | ((Boolean)right.value).booleanValue());
				default:
					throw new ParseException("Operands must be either both boolean or both integer");
			}
		}
	}

	class OperatorXor extends Operator {
		/**
		 * Default ctor
		 * @param precedence_ Precedence of operator (smaller number == higher precedence)
		 */
		OperatorXor(int precedence_) {
			super(Operator.BINARY_LEFT,precedence_);
		}
		/**
		 * @see Operator#result(Token[])
		 */
		@Override
		public Value result (Token operand[]) throws ParseException {
			Value left, right;
			left  = getValue(operand[0],Token.VALUE|Token.VARIABLE,OP_LEFT);
			right = getValue(operand[1],Token.VALUE|Token.VARIABLE,OP_RIGHT);
			switch (left.type) {
				case Value.NUMBER:
					if (right.type != Value.NUMBER || !((JNumber)left.value).isInteger())
						throw new ParseException("Only defined on integer values");
					try {
						return new Value(((JNumber)left.value).xor((JNumber)right.value));
					} catch(JNumberException ex) {
						throw new ParseException(ex.getMessage());
					}
				case Value.BOOL:
					if (right.type != Value.BOOL)
						throw new ParseException("Right side must be boolean");
					return new Value(((Boolean)left.value).booleanValue() ^ ((Boolean)right.value).booleanValue());
				default:
					throw new ParseException("Operands must be either both boolean or both integer");
			}
		}
	}

	class OperatorAnd extends Operator {
		/**
		 * Default ctor
		 * @param precedence_ Precedence of operator (smaller number == higher precedence)
		 */
		OperatorAnd(int precedence_) {
			super(Operator.BINARY_LEFT,precedence_);
		}
		/**
		 * @see Operator#result(Token[])
		 */
		@Override
		public Value result (Token operand[]) throws ParseException {
			Value left, right;
			left  = getValue(operand[0],Token.VALUE|Token.VARIABLE,OP_LEFT);
			right = getValue(operand[1],Token.VALUE|Token.VARIABLE,OP_RIGHT);
			switch (left.type) {
				case Value.NUMBER:
					if (right.type != Value.NUMBER || !((JNumber)left.value).isInteger())
						throw new ParseException("Only defined on integer values");
					try {
						return new Value(((JNumber)left.value).and((JNumber)right.value));
					} catch(JNumberException ex) {
						throw new ParseException(ex.getMessage());
					}
				case Value.BOOL:
					if (right.type != Value.BOOL)
						throw new ParseException("Right side must be boolean");
					return new Value(((Boolean)left.value).booleanValue() & ((Boolean)right.value).booleanValue());
				default:
					throw new ParseException("Operands must be either both boolean or both integer");
			}
		}
	}

	class OperatorAndAnd extends Operator {
		/**
		 * Default ctor
		 * @param precedence_ Precedence of operator (smaller number == higher precedence)
		 */
		OperatorAndAnd(int precedence_) {
			super(Operator.BINARY_LEFT,precedence_);
		}
		/**
		 * @see Operator#result(Token[])
		 */
		@Override
		public Value result (Token operand[]) throws ParseException {
			boolean left,right;
			// works on values
			left = getBoolean(operand[0],Token.VALUE|Token.VARIABLE,OP_LEFT);
			right = getBoolean(operand[1],Token.VALUE|Token.VARIABLE,OP_RIGHT);
			return new Value(left && right);
		}
		/**
		 *  @see Operator#canShortCut()
		 */
		@Override
		public boolean canShortCut() {
			return true;
		}

		/**
		 *  @see Operator#isShortCut(Token)
		 */
		@Override
		public boolean isShortCut(Token operand) throws ParseException {
			boolean left = getBoolean(operand,Token.VALUE|Token.VARIABLE,OP_LEFT);
			return (left == false); // result can't be true
		}

		/**
		 * @see Operator#resultShortCut()
		 */
		@Override
		public Value resultShortCut() {
			return new Value(false);
		}
	}

	class OperatorOrOr extends Operator {
		/**
		 * Default ctor
		 * @param precedence_ Precedence of operator (smaller number == higher precedence)
		 */
		OperatorOrOr(int precedence_) {
			super(Operator.BINARY_LEFT,precedence_);
		}
		/**
		 * @see Operator#result(Token[])
		 */
		@Override
		public Value result (Token operand[]) throws ParseException {
			boolean left,right;
			// works on values
			left = getBoolean(operand[0],Token.VALUE|Token.VARIABLE,OP_LEFT);
			right = getBoolean(operand[1],Token.VALUE|Token.VARIABLE,OP_RIGHT);
			return new Value(left || right);
		}
		/**
		 *  @see Operator#canShortCut()
		 */
		@Override
		public boolean canShortCut() {
			return true;
		}

		/**
		 *  @see Operator#isShortCut(Token)
		 */
		@Override
		public boolean isShortCut(Token operand) throws ParseException {
			boolean left = getBoolean(operand,Token.VALUE|Token.VARIABLE,OP_LEFT);
			return (left == true); // result can't be false
		}

		/**
		 * @see Operator#resultShortCut()
		 */
		@Override
		public Value resultShortCut() {
			return new Value(true);
		}
	}


	class OperatorShiftLeft extends Operator {
		/**
		 * Default ctor
		 * @param precedence_ Precedence of operator (smaller number == higher precedence)
		 */
		OperatorShiftLeft(int precedence_) {
			super(Operator.BINARY_LEFT,precedence_);
		}
		/**
		 * @see Operator#result(Token[])
		 */
		@Override
		public Value result (Token operand[]) throws ParseException {
			JNumber left,right;
			Value val;
			// works on values
			left = getInteger(operand[0],Token.VALUE|Token.VARIABLE,OP_LEFT);
			right = getInteger(operand[1],Token.VALUE|Token.VARIABLE,OP_RIGHT);
			try {
				val = new Value(left.shiftLeft(right));
			} catch (JNumberException ex) {
				throw new ParseException(ex.getMessage());
			}
			return val;
		}
	}

	class OperatorShiftRight extends Operator {
		/**
		 * Default ctor
		 * @param precedence_ Precedence of operator (smaller number == higher precedence)
		 */
		OperatorShiftRight(int precedence_) {
			super(Operator.BINARY_LEFT,precedence_);
		}
		/**
		 * @see Operator#result(Token[])
		 */
		@Override
		public Value result (Token operand[]) throws ParseException {
			JNumber left,right;
			Value val;
			// works on values
			left = getInteger(operand[0],Token.VALUE|Token.VARIABLE,OP_LEFT);
			right = getInteger(operand[1],Token.VALUE|Token.VARIABLE,OP_RIGHT);
			try {
				val = new Value(left.shiftRight(right));
			} catch (JNumberException ex) {
				throw new ParseException(ex.getMessage());
			}
			return val;
		}
	}

	class OperatorEqual extends Operator {
		/**
		 * Default ctor
		 * @param precedence_ Precedence of operator (smaller number == higher precedence)
		 */
		public OperatorEqual(int precedence_) {
			super(Operator.BINARY_LEFT, precedence_);
		}
		/**
		 * @see Operator#result(Token[])
		 */
		@Override
		public Value result (Token operand[]) throws ParseException {
			Value left, right;
			left  = getValue(operand[0],Token.VALUE|Token.VARIABLE,OP_LEFT);
			right = getValue(operand[1],Token.VALUE|Token.VARIABLE,OP_RIGHT);
			if (left.type != right.type)
				throw new ParseException("Operands must be of equal type.");
			switch (left.type) {
				case Value.NUMBER:
				case Value.BOOL:
				case Value.STRING:
				case Value.LIST:
					return new Value(left.equals(right));
			}
			throw new ParseException("Invalid value type at left side of operator");
		}
	}

	class OperatorNotEqual extends Operator {
		/**
		 * Default ctor
		 * @param precedence_ Precedence of operator (smaller number == higher precedence)
		 */
		public OperatorNotEqual(int precedence_) {
			super(Operator.BINARY_LEFT, precedence_);
		}
		/**
		 * @see Operator#result(Token[])
		 */
		@Override
		public Value result (Token operand[]) throws ParseException {
			Value left, right;
			left  = getValue(operand[0],Token.VALUE|Token.VARIABLE,OP_LEFT);
			right = getValue(operand[1],Token.VALUE|Token.VARIABLE,OP_RIGHT);
			if (left.type != right.type)
				throw new ParseException("Operands must be of equal type.");
			switch (left.type) {
				case Value.NUMBER:
				case Value.BOOL:
				case Value.STRING:
				case Value.LIST:
					return new Value(!left.equals(right));
			}
			throw new ParseException("Invalid value type at left side of operator");
		}
	}

	class OperatorGreater extends Operator {
		/**
		 * Default ctor
		 * @param precedence_ Precedence of operator (smaller number == higher precedence)
		 */
		public OperatorGreater(int precedence_) {
			super(Operator.BINARY_LEFT, precedence_);
		}
		/**
		 * @see Operator#result(Token[])
		 */
		@Override
		public Value result (Token operand[]) throws ParseException {
			return new Value(compareValue(operand)>0);
		}
	}

	class OperatorGreaterEqual extends Operator {
		/**
		 * Default ctor
		 * @param precedence_ Precedence of operator (smaller number == higher precedence)
		 */
		public OperatorGreaterEqual(int precedence_) {
			super(Operator.BINARY_LEFT, precedence_);
		}
		/**
		 * @see Operator#result(Token[])
		 */
		@Override
		public Value result (Token operand[]) throws ParseException {
			return new Value(compareValue(operand)>=0);        }
	}

	class OperatorLess extends Operator {
		/**
		 * Default ctor
		 * @param precedence_ Precedence of operator (smaller number == higher precedence)
		 */
		public OperatorLess(int precedence_) {
			super(Operator.BINARY_LEFT, precedence_);
		}
		/**
		 * @see Operator#result(Token[])
		 */
		@Override
		public Value result (Token operand[]) throws ParseException {
			return new Value(compareValue(operand)<0);
		}
	}

	class OperatorLessEqual extends Operator {
		/**
		 * Default ctor
		 * @param precedence_ Precedence of operator (smaller number == higher precedence)
		 */
		public OperatorLessEqual(int precedence_) {
			super(Operator.BINARY_LEFT, precedence_);
		}
		/**
		 * @see Operator#result(Token[])
		 */
		@Override
		public Value result (Token operand[]) throws ParseException {
			return new Value(compareValue(operand)<=0);
		}
	}

	/**
	 * Subfunction used by compare operators
	 * @param operand
	 * @return -1 (operand[0] &lt; operand[1]), 0 (operand[0] == operand[1]), 1 operand[0] &gt; operand[1]
	 * @throws ParseException
	 */
	private int compareValue(Token operand[]) throws ParseException {
		Value left, right;
		left  = getValue(operand[0],Token.VALUE|Token.VARIABLE,OP_LEFT);
		right = getValue(operand[1],Token.VALUE|Token.VARIABLE,OP_RIGHT);
		if (left.type != right.type)
			throw new ParseException("Operands must be of equal type.");
		switch (left.type) {
			case Value.NUMBER:
				return ((JNumber)left.value).compareTo((JNumber)right.value);
			case Value.STRING:
				return ((String)left.value).compareTo((String)right.value);
		}
		throw new ParseException("Invalid value type at left side of operator");
	}

	final static long serialVersionUID = 0x000000001;
	/**
	 * parser states
	 */
	private static final int IN_UNKNOWN       = 0;
	private static final int IN_OPERATOR      = 1;
	private static final int IN_TOKEN         = 2;
	private static final int IN_STRING        = 3;
	private static final int IN_LINE_COMMENT  = 4;
	private static final int IN_BLOCK_COMMENT = 5;

	private static final int OP_LEFT     = -2;
	private static final int OP_RIGHT    = -1;

	private static final BigInteger MAX_U64 = new BigInteger("18446744073709551616");
	private static final BigInteger MAX_S64 = BigInteger.valueOf(Long.MAX_VALUE);
	private static final BigInteger MIN_S64 = BigInteger.valueOf(Long.MIN_VALUE);
	private static final BigInteger MAX_U32 = new BigInteger("4294967296");
	private static final BigInteger MAX_S32 = BigInteger.valueOf(Integer.MAX_VALUE);
	private static final BigInteger MIN_S32 = BigInteger.valueOf(Integer.MIN_VALUE);
	private static final BigInteger MAX_U16 = BigInteger.valueOf(65536);
	private static final BigInteger MAX_S16 = BigInteger.valueOf(Short.MAX_VALUE);
	private static final BigInteger MIN_S16 = BigInteger.valueOf(Short.MIN_VALUE);
	private static final BigInteger MAX_U8  = BigInteger.valueOf(256);
	private static final BigInteger MAX_S8  = BigInteger.valueOf(127);
	private static final BigInteger MIN_S8  = BigInteger.valueOf(-128);

	/**
	 * Allowed characters for operators
	 */
	public static final String delimiterChars = " \t()[]{}\n\r,;:?\"";

	/**
	 * private variables
	 */
	Variables variables;
	UserFunctions userFunctions;
	private HashMap<String,Function[]>  functions;
	private HashMap<String,Operator[]>  operators;
	private ErrorPos errorPos = new ErrorPos(0,1,"");
	private JParseListener parseListener;
	private String errorString = ""; // used to print last error line
	private String errorFile = ""; // File
	private ALStack<String> errorFiles;
	private String scriptPath = ".";
	private boolean breakExec = false;


	public void breakExec() {
		breakExec = true;
	}

	/**
	 * Scale used for BigDecimal calculations
	 */
	private int internalScale = 64;

	public void setScriptPath(String path) {
		scriptPath = path;
	}

	public void setScale(int scale) {
		internalScale = scale;
		addDefaultConstants();
		JNumber.setInternalScale(scale);
	}

	/**
	 * default constructor
	 * @throws ParseException
	 */
	public JParse() throws ParseException {
		variables     = new Variables();
		userFunctions = new UserFunctions();
		functions     = new HashMap<String,Function[]>();
		operators     = new HashMap<String,Operator[]>();
		errorFiles    = new ALStack<String>();
		addDefaultOperators();
		addDefaultFunctions();
		addDefaultConstants();
	}

	/**
	 * Reset parser
	 */
	public void reset() {
		variables.clear();
		userFunctions.clear();
		functions.clear();
		operators.clear();
		errorFiles.clear();
	}

	/**
	 * Read position of Operator that caused an Exception
	 * @return Position of Operator that caused an Exception
	 */
	public ErrorPos getErrorPos() {
		return errorPos;
	}

	/**
	 * Return text line in which error occured
	 * @return String containing text line in which error occured
	 * @throws ParseException
	 */
	public String getErrorString() throws ParseException {
		errorFile = errorPos.file;
		if (errorFile.length() == 0)
			return errorString;
		// might be a stored command line
		if (errorFile.indexOf("CMD:") == 0)
			return errorFile.substring(4);

		// load line containing error position from file
		String fileName = errorFile.substring(5); // "FILE:xxx"
		BufferedReader in=null;
		// Load
		try {
			String buffer = null;
			in = new BufferedReader(new FileReader(fileName));

			for (int i=0;i<errorPos.line;i++)
				buffer = in.readLine();
			in.close();
			buffer = buffer.replaceAll("\t"," "); // replace tabs for correct error position
			return buffer;
		} catch (FileNotFoundException ex) {
			throw new ParseException("File not found: "+fileName);
		} catch (IOException ex) {
			throw new ParseException("IO Exception while reading: "+fileName);
		} finally { try {if (in!=null)in.close();} catch (IOException ex) {}  }

	}

	/**
	 * Checks is a character is a valid (starting) character for a number
	 * @param c the char to be checked
	 * @return true (is numeric) or false (is not numeric)
	 */
	private boolean isNumeric(char c) {
		return ("0123456789.".indexOf(c) != -1);
	}

	/**
	 * Used by {@link #getNumber(Token, int, int)}, {@link #getBoolean(Token, int, int)},
	 * {@link #getInteger(Token, int, int)}, {@link #getString(Token, int, int)} and
	 * {@link #getValue(Token, int, int)} to construct exception message
	 * @param pos Position of exception
	 * @return String describing location of invalid operand/parameter
	 */
	private String failurePos(int pos) {
		if (pos == OP_LEFT)
			return "on the left side of operator";
		if (pos == OP_RIGHT)
			return "on the right side of operator";
		return "for parameter "+Integer.toString(pos);

	}

	/**
	 * Read JNumber from Token, if this is possible, else throw Exception
	 * @param token Token to read value from
	 * @param opType Describes allowed operand/parameter type (value/variable)
	 * @param pos Describes position of operand/parameter (left, right or parameter number)
	 * @return JNumber read from Token
	 * @throws ParseException
	 */
	private JNumber getNumber(Token token, int opType, int pos) throws ParseException {
		Value val = getValue(token,opType,pos);
		switch (val.type) {
			case Value.NUMBER:
				return new JNumber((JNumber)val.value); // new is needed to not destroy RPN stack
			case Value.NUMSTRING:
				return new JNumber((String)val.value);
		}
		throw new ParseException("Number expected "+failurePos(pos));
	}

	/**
	 * Read integer JNumber from Token, if this is possible, else throw Exception
	 * @param token Token to read value from
	 * @param opType Describes allowed operand/parameter type (value/variable)
	 * @param pos Describes position of operand/parameter (left, right or parameter number)
	 * @return JNumber read from Token
	 * @throws ParseException
	 */
	private JNumber getInteger(Token token, int opType, int pos) throws ParseException {
		JNumber num = getNumber(token,opType,pos);
		if (num.isInteger())
			return num;
		throw new ParseException("Integer value expected "+failurePos(pos));
	}

	/**
	 * Read int (32bit) JNumber from Token, if this is possible, else throw Exception
	 * @param token Token to read value from
	 * @param opType Describes allowed operand/parameter type (value/variable)
	 * @param pos Describes position of operand/parameter (left, right or parameter number)
	 * @return JNumber read from Token
	 * @throws ParseException
	 */
	private int getUInt(Token token, int opType, int pos) throws ParseException {
		JNumber num = getNumber(token,opType,pos);
		if (num.isInteger()) {
			BigInteger bi = num.toBigInteger();
			if (bi.compareTo(MIN_S32) > 0) {
				if (bi.signum() >= 0)
					return (int)bi.longValue();
			}
		}
		throw new ParseException("unsigned integer value <="+Integer.MAX_VALUE+" expected.");
	}

	/**
	 * Read boolean value from Token, if this is possible, else throw Exception
	 * @param token Token to read value from
	 * @param opType Describes allowed operand/parameter type (value/variable)
	 * @param pos Describes position of operand/parameter (left, right or parameter number)
	 * @return boolean read from Token
	 * @throws ParseException
	 */
	private boolean getBoolean(Token token, int opType, int pos) throws ParseException {
		Value val = getValue(token,opType,pos);
		if (val.type == Value.BOOL)
			return ((Boolean)val.value).booleanValue();
		throw new ParseException("Boolean expected "+failurePos(pos));

	}

	/**
	 * Read String from Token, if this is possible, else throw Exception
	 * @param token Token to read value from
	 * @param opType Describes allowed operand/parameter type (value/variable)
	 * @param pos Describes position of operand/parameter (left, right or parameter number)
	 * @return String read from Token
	 * @throws ParseException
	 */
	private String getString(Token token, int opType, int pos) throws ParseException {
		Value val = getValue(token,opType,pos);
		if (val.type == Value.STRING)
			return (String)val.value;
		throw new ParseException("String expected "+failurePos(pos));
	}

	/**
	 * Read ValueList from Token, if this is possible, else throw Exception
	 * @param token Token to read value from
	 * @param opType Describes allowed operand/parameter type (value/variable)
	 * @param pos Describes position of operand/parameter (left, right or parameter number)
	 * @return ValueList read from Token
	 * @throws ParseException
	 */
	private ValueList getList(Token token, int opType, int pos) throws ParseException {
		Value val = getValue(token,opType,pos);
		if (val.type == Value.LIST)
			return (ValueList)val.value;
		throw new ParseException("List expected "+failurePos(pos));
	}

	/**
	 * Read Value from Token, if this is possible, else throw Exception
	 * @param token Token to read value from
	 * @param opType Describes allowed operand/parameter type (value/variable)
	 * @param pos Describes position of operand/parameter (left, right or parameter number)
	 * @return Value read from Token
	 * @throws ParseException
	 */
	private Value getValue(Token token, int opType, int pos) throws ParseException{
		Value result;
		switch (token.type) {
			case Token.VARIABLE:
				if ((opType & Token.VARIABLE) != 0) {
					try {
						result = variables.getValue(token.name);
					} catch (ParseException ex) {
						throw new ParseException(ex.getMessage()+" "+failurePos(pos));
					}
					result = new Value(result); // new instance to avoid changing the variable by chance
				} else {
					throw new ParseException("Variable "+token.name+" where value was expected "+failurePos(pos));
				}
				break;
			case Token.VALUE:
				if ((opType & Token.VALUE) == 0) {
					throw new ParseException("Value where variable was expected "+failurePos(pos));
				}
				Value v = (Value)token.content;
				if (v.type==Value.NUMSTRING)
					token.content = new Value(v); // converts NUMSTRING to NUMBER
				return (Value)token.content;
			default:
				throw new ParseException("Illegal Token after "+token.toString());
		}
		return result;
	}

	/**
	 * Add default operators to parser engine
	 */
	public void addDefaultOperators() {
		operators.put("++", new Operator[] { new OperatorPlusPlusUnaryR(1), new OperatorPlusPlusUnaryL(1) } );
		operators.put("--", new Operator[] { new OperatorMinusMinusUnaryR(1), new OperatorMinusMinusUnaryL(1) } );
		operators.put("!",  new Operator[] { new OperatorLogicalComplement(1)}); // logical complement
		operators.put("~",  new Operator[] { new OperatorNot(1)}); // binary complement
		/* casting */
		operators.put("(integer)",  new Operator[] { new OperatorCastInteger(1)});
		operators.put("(u64)",      new Operator[] { new OperatorCastU64(1)});
		operators.put("(qword)",    new Operator[] { new OperatorCastU64(1)});
		operators.put("(s64)",      new Operator[] { new OperatorCastS64(1)});
		operators.put("(long)",     new Operator[] { new OperatorCastS64(1)});
		operators.put("(u32)",      new Operator[] { new OperatorCastU32(1)});
		operators.put("(dword)",    new Operator[] { new OperatorCastU32(1)});
		operators.put("(s32)",      new Operator[] { new OperatorCastS32(1)});
		operators.put("(int)",      new Operator[] { new OperatorCastS32(1)});
		operators.put("(u16)",      new Operator[] { new OperatorCastU16(1)});
		operators.put("(word)",     new Operator[] { new OperatorCastU16(1)});
		operators.put("(char)",     new Operator[] { new OperatorCastU16(1)});
		operators.put("(s16)",      new Operator[] { new OperatorCastS16(1)});
		operators.put("(short)",    new Operator[] { new OperatorCastS16(1)});
		operators.put("(u08)",      new Operator[] { new OperatorCastU8(1)});
		operators.put("(u8)",       new Operator[] { new OperatorCastU8(1)});
		operators.put("(byte)",     new Operator[] { new OperatorCastU8(1)});
		operators.put("(s08)",      new Operator[] { new OperatorCastS8(1)});
		operators.put("(s8)",       new Operator[] { new OperatorCastS8(1)});
		operators.put("(double)",   new Operator[] { new OperatorCastDouble(1)});
		operators.put("(string)",   new Operator[] { new OperatorCastString(1)});
		/*
		operators.put("(float)", new Operator(Operator.RIGHT,  1)); // cast to float
		operators.put("(boolean)", new Operator(Operator.RIGHT,  1)); // cast to boolean
		 */
		operators.put("*",  new Operator[] { new OperatorMultiply(2)}); // multiplication
		operators.put("/",  new Operator[] { new OperatorDivide(2) }); // division
		operators.put("%",  new Operator[] { new OperatorMod(2)}); // modulo

		operators.put("+",  new Operator[] { new OperatorPlusUnary(1), new OperatorPlusBinary(3)});
		operators.put("-",  new Operator[] { new OperatorMinusUnary(1), new OperatorMinusBinary(3)});
		// this is not a bug, but the Windows Minus ACII 0x96 to avoid strange parser errors
		operators.put("�",  new Operator[] { new OperatorMinusUnary(1), new OperatorMinusBinary(3)});

		operators.put("<<", new Operator[] { new OperatorShiftLeft(4)}); // shift left
		operators.put(">>", new Operator[] { new OperatorShiftRight(4)}); // shift right
		/*
		operators.put(">>>", new Operator(Operator.BOTH,  4)); // shift right (signed)
		// missing: "?:"
		 */
		operators.put("<",  new Operator[] { new OperatorLess(5)}); // less
		operators.put("<=", new Operator[] { new OperatorLessEqual(5)}); // less equal
		operators.put(">",  new Operator[] { new OperatorGreater(5)}); // greater
		operators.put(">=", new Operator[] { new OperatorGreaterEqual(5)}); // greater equal

		operators.put("==", new Operator[] { new OperatorEqual(6)}); // equal
		operators.put("!=", new Operator[] { new OperatorNotEqual(6)}); // not equal

		operators.put("&",  new Operator[] { new OperatorAnd(7)}); // and
		operators.put("^",  new Operator[] { new OperatorXor(8)}); // xor
		operators.put("|",  new Operator[] { new OperatorOr(9)}); // or

		operators.put("&&", new Operator[] { new OperatorAndAnd(10)}); // andand
		operators.put("||", new Operator[] { new OperatorOrOr(11)}); // oror
		operators.put("=", new Operator[] { new OperatorSet(13)}); // set value

		operators.put("*=", new Operator[] { new OperatorMultiplySet(14)});
		operators.put("/=", new Operator[] { new OperatorDivideSet(14)});
		operators.put("%=", new Operator[] { new OperatorModSet(14)});
		operators.put("+=", new Operator[] { new OperatorAddSet(14)});
		operators.put("-=", new Operator[] { new OperatorSubtractSet(14)});
		operators.put("<<=", new Operator[] { new OperatorShiftLeftSet(14)});
		operators.put(">>=", new Operator[] { new OperatorShiftRightSet(14)});
		operators.put("|=", new Operator[] { new OperatorOrSet(14)});
		operators.put("&=", new Operator[] { new OperatorAndSet(14)});
		operators.put("^=", new Operator[] { new OperatorXorSet(14)});

		/*
		// missing: all the +=, -=, *=, /= etc. at 14
		 */

		/*
		operators.put("(s64)", new Operator(Operator.RIGHT,  1)); // cast to s64
		operators.put("(u64)", new Operator(Operator.RIGHT,  1)); // cast to u64
		 */
		operators.put("**", new Operator[] { new OperatorPow(1)}); // power of

	}

	/**
	 * Add default functions to parser engine
	 * @throws ParseException
	 */
	public void addDefaultFunctions () throws ParseException {
		functions.put("abs", new Function[] { new FunctionAbs()});
		functions.put("rnd", new Function[] { new FunctionRnd()});
		functions.put("trunc", new Function[] { new FunctionTrunc()});
		functions.put("floor", new Function[] { new FunctionFloor()});
		functions.put("ceil", new Function[] { new FunctionCeil()});
		functions.put("round", new Function[] { new FunctionRound()});
		functions.put("pow", new Function[] { new FunctionPow()});
		functions.put("fac", new Function[] { new FunctionFact()});
		functions.put("prime", new Function[] { new FunctionPrime()});
		functions.put("sqrt", new Function[] { new FunctionSqrt()});
		functions.put("root", new Function[] { new FunctionRoot()});
		functions.put("sin", new Function[] { new FunctionSin()});
		functions.put("sinh", new Function[] { new FunctionSinH()});
		functions.put("asin", new Function[] { new FunctionAsin()});
		functions.put("cos", new Function[] { new FunctionCos()});
		functions.put("cosh", new Function[] { new FunctionCosH()});
		functions.put("acos", new Function[] { new FunctionAcos()});
		functions.put("tan", new Function[] { new FunctionTan()});
		functions.put("tanh", new Function[] { new FunctionTanH()});
		functions.put("atan", new Function[] { new FunctionAtan()});
		functions.put("print", new Function[] { new FunctionPrint()});
		functions.put("hex", new Function[] { new FunctionPrintHex()});
		functions.put("bin", new Function[] { new FunctionPrintBinary()});
		functions.put("oct", new Function[] { new FunctionPrintOct()});
		functions.put("full", new Function[] { new FunctionPrintFull()});
		functions.put("fraction", new Function[] { new FunctionPrintFraction()});
		functions.put("bits", new Function[] { new FunctionNumBits()});
		functions.put("log", new Function[] { new FunctionLog()});
		functions.put("ld", new Function[] { new FunctionLog2()});
		functions.put("log10", new Function[] { new FunctionLog10()});
		functions.put("exp", new Function[] { new FunctionExp()});
		/* primefact */
		functions.put("die", new Function[] { new FunctionDie()});
		functions.put("defined", new Function[] { new FunctionDefined()});
		functions.put("load", new Function[] { new FunctionLoad()});
		functions.put("println", new Function[] { new FunctionPrintLn()});
		functions.put("time", new Function[] { new FunctionTime()});
		functions.put("clear", new Function[] { new FunctionClear()});
		functions.put("listv", new Function[] { new FunctionListVar()});
		functions.put("listf", new Function[] { new FunctionListFunctions()});
		functions.put("eval", new Function[] { new FunctionEval()});

		functions.put("sfill", new Function[] { new FunctionFillString()});
		functions.put("lfill", new Function[] { new FunctionFillList()});
		functions.put("sub", new Function[] { new FunctionSub()});
		functions.put("insert", new Function[] { new FunctionInsert()});
		functions.put("replace", new Function[] { new FunctionReplace()});
		functions.put("size", new Function[] { new FunctionSize()});
		functions.put("index", new Function[] { new FunctionIndexOf()});
		functions.put("lastindex", new Function[] { new FunctionLastIndexOf()});
	}

	public void addDefaultConstants() {
		try {
			variables.putNumber("PI", new JNumber(BigDecimalMath.PI(internalScale)));
			variables.putNumber("E", new JNumber(BigDecimalMath.E(internalScale)));
		} catch (ParseException ex) {}
	}


	void checkToken(Token tok, Token lastTok) throws ParseException {
		switch (tok.type) {
			case Token.VALUE:
			case Token.VARIABLE:
				switch (lastTok.type) {
					case Token.PARENTHESIS_CLOSE:
						errorPos.copyFrom(tok.pos);
						if (tok.type==Token.VALUE)
							throw new ParseException("unexpected value: "+tok.content.toString());
						throw new ParseException("unexpected variable: "+tok.name);
					case Token.VALUE:
						errorPos.copyFrom(lastTok.pos);
						throw new ParseException("unexpected value: "+lastTok.content.toString());
					case Token.VARIABLE:
						errorPos.copyFrom(lastTok.pos);
						throw new ParseException("unexpected variable: "+lastTok.name);
					case Token.FUNCTION:
					case Token.USER_FUNCTION:
					case Token.KEYWORD_FUNCTION:
					case Token.KEYWORD_IF:
					case Token.KEYWORD_FOR:
					case Token.KEYWORD_FOREACH:
					case Token.KEYWORD_DO:
					case Token.KEYWORD_WHILE:
						errorPos.copyFrom(lastTok.pos);
						throw new ParseException("unexpected token.");
				}
				break;
			case Token.FUNCTION:
			case Token.USER_FUNCTION:
			case Token.KEYWORD_FUNCTION:
			case Token.KEYWORD_IF:
			case Token.KEYWORD_FOR:
			case Token.KEYWORD_FOREACH:
			case Token.KEYWORD_DO:
			case Token.KEYWORD_WHILE:
				switch (lastTok.type) {
					case Token.PARENTHESIS_CLOSE:
					case Token.VALUE:
					case Token.VARIABLE:
					case Token.FUNCTION:
					case Token.USER_FUNCTION:
						errorPos.copyFrom(lastTok.pos);
						throw new ParseException("unexpected token.");
				}
				break;
		}
	}

	/**
	 * Return Nth line from a string containing newline characters
	 * @param text Text to search in
	 * @param line Number of text line to extract
	 * @return String holding the text line without newline characters
	 */
	private String getLine(String text, int line) {
		int lineIdx=1;
		int startIdx=0;
		int charIdx=0;
		for (; charIdx<text.length(); charIdx++) {
			if (text.charAt(charIdx) != '\n')
				continue;
			if (++lineIdx == line)
				startIdx=charIdx+1;
			else if (lineIdx == line+1)
				break;
		}
		return text.substring(startIdx,charIdx);
	}

	/**
	 * Evaluates expression
	 * @param s expression
	 * @return value of the evaluated expression
	 * @throws ParseException
	 */
	public Value eval(String s) throws ParseException {
		// reset internal info
		errorFile = "CMD:"+s; // default: no file, but command line
		userFunctions.resetNameSpace();
		errorFiles.clear();
		// call evaluator
		return evaluate(s);
	}


	/**
	 * Evaluates expression
	 * @param s expression
	 * @return value of the evaluated expression
	 * @throws ParseException
	 */
	public Value evaluate(String s) throws ParseException {
		Value result;
		errorString = s;
		try {
			// replace tabs and HTML escape characters with spaces
			// to avoid strange parser error when pasting code from the HTML help
			s = s.replace('\t', ' ');
			s = s.replace((char)0xa0, ' ');
			ALStack<Token> commandStack = parse(s);
			resolveOps(commandStack);
			ALStack<Token> rpn = createRPNStack(commandStack,0,commandStack.size(),0);
			result = evalRPNStack(rpn);
			variables.putValue("out",new Value(result));
			return result;
		} catch (ParseException ex) {
			errorString = getLine(errorString,errorPos.line);
			throw new ParseException(ex.getMessage());
		} catch (StackOverflowError ex) {
			throw new ParseException("Stack overflow: " + ex.toString());
		}
	}

	/**
	 * Just a structure holding the parsing state
	 * Thus it can be passed to sub functions etc.
	 */
	private class ParseState {
		/**
		 * current position inside current line
		 */
		int pos;
		/**
		 * current position inside whole context (e.g. file)
		 */
		int idx;
		/**
		 * Start index (whole context) of current token
		 */
		int startIdx;
		int state;
		/**
		 * Number of current line
		 */
		int line;
		int pLevel; // parenthesis level
		int braceLevel; // braces level
		int bracketLevel; // bracket level
		int fLevel; // function nesting level
		Token lastToken = new Token(Token.START);
		Token token;
		ALStack<Token> commandStack = new ALStack<Token>(); // <Token>
	}


	/**
	 * Splits expression into tokens and puts them on the commandStack, then calls the eval method
	 * @param s expression
	 * @return Command stack in linear order
	 * @throws ParseException
	 */
	public ALStack<Token> parse(String s) throws ParseException {
		ParseState ps = new ParseState();

		ps.pos = 0;
		ps.idx=0;
		ps.startIdx=0;
		ps.state = IN_UNKNOWN;
		ps.line = 1;
		ps.pLevel = 0; // parenthesis level
		ps.fLevel = 0; // function nesting level
		ps.lastToken = new Token(Token.START);
		ps.token = ps.lastToken;
		ps.commandStack = new ALStack<Token>();

		ALStack<Integer> fParamNum = new ALStack<Integer>(); // number of parameters (stacked for pLevel)
		int paramNum = 0;
		boolean ufInHeader=false;
		Token tok;
		char c;
		String tokenStr = "";
		//ALStack fpLevel = new ALStack(); //<Integer>
		//ALStack fonToken = new ALStack(); // <Token>

		// we don't need no leading/trailing spaces (but we need the ones in the middle!)
		s = s.trim()+" "; // append space to have delimiter at the end
		try {
			parseLoop: for (; ps.idx < s.length(); ps.lastToken=ps.token,ps.idx++,ps.pos++) {
				c = s.charAt(ps.idx);
				if (c=='\n') {
					ps.line++;
					ps.pos = -1; // increase to 0 at for loop
					errorPos.set(0,ps.line,errorFile);
				}
				switch (ps.state) {
					case IN_BLOCK_COMMENT: // handle comments
						if (c=='*' && ps.idx < s.length()-1 && s.charAt(ps.idx+1)=='/') {
							ps.idx++;
							ps.state = IN_UNKNOWN;
							continue;
						}
						break;
					case IN_LINE_COMMENT:
						if (ps.state==IN_LINE_COMMENT && c=='\n')
							ps.state = IN_UNKNOWN;
						if (ps.state==IN_LINE_COMMENT || ps.state==IN_BLOCK_COMMENT)
							continue;
						break;
					case IN_STRING: // handle string literals
						// check for string id
						if (c == '\"') {
							tokenStr = s.substring(ps.startIdx + 1, ps.idx);
							ps.token = new Token(Token.VALUE, new Value(tokenStr));
							ps.token.pos.set(ps.pos-tokenStr.length(),ps.line,errorFile);
							ps.commandStack.push(ps.token);
							checkToken(ps.token, ps.lastToken); // check
							ps.state = IN_UNKNOWN;
						}
						continue;
					case IN_TOKEN:
						if (delimiterChars.indexOf(c) != -1 || Operator.operatorChars.indexOf(c) != -1) {
							// Token closed, now check for token type
							tokenStr = s.substring(ps.startIdx, ps.idx);
							// first check keywords
							if (tokenStr.equalsIgnoreCase(Keyword.FUNCTION)) {
								ps.token = new Token(Token.KEYWORD_FUNCTION, tokenStr);
								ps.commandStack.push(ps.token);
								checkToken(ps.token, ps.lastToken);
							} else if (tokenStr.equalsIgnoreCase(Keyword.IF)) {
								ps.token = new Token(Token.KEYWORD_IF, tokenStr);
								ps.commandStack.push(ps.token);
								checkToken(ps.token, ps.lastToken);
							} else if (tokenStr.equalsIgnoreCase(Keyword.ELSE)) {
								ps.token = new Token(Token.KEYWORD_ELSE, tokenStr);
								ps.commandStack.push(ps.token);
								checkToken(ps.token, ps.lastToken);
							} else if (tokenStr.equalsIgnoreCase(Keyword.FOR)) {
								ps.token = new Token(Token.KEYWORD_FOR, tokenStr);
								ps.commandStack.push(ps.token);
								checkToken(ps.token, ps.lastToken);
							} else if (tokenStr.equalsIgnoreCase(Keyword.DO)) {
								ps.token = new Token(Token.KEYWORD_DO, tokenStr);
								ps.commandStack.push(ps.token);
								checkToken(ps.token, ps.lastToken);
							} else if (tokenStr.equalsIgnoreCase(Keyword.WHILE)) {
								ps.token = new Token(Token.KEYWORD_WHILE, tokenStr);
								ps.commandStack.push(ps.token);
								checkToken(ps.token, ps.lastToken);
							} else if (tokenStr.equalsIgnoreCase(Keyword.FOREACH)) {
								ps.token = new Token(Token.KEYWORD_FOREACH, tokenStr);
								ps.commandStack.push(ps.token);
								checkToken(ps.token, ps.lastToken);
							} else {
								// normal token
								// check for mistaken "e+x", "e-x" inside number literal
								// However 'e' could also be last cipher of hex number
								if (isNumeric(tokenStr.charAt(0)) && !(tokenStr.length() > 2
										&& tokenStr.charAt(0)=='0' && tokenStr.charAt(1)=='x')) {
									// now this could be a special case, namely "xe+y" or "ye-y"
									if (tokenStr.length() > 1 && s.length() - ps.idx > 1) {
										char cc = tokenStr.charAt(tokenStr.length() - 1);
										// handle two special cases: 'e' for exponent,
										// 'p' for period
										int cPos = "eEpP".indexOf(cc);
										if (cPos > -1) {
											if (cPos < 2) { // last character exponent?
												cc = s.charAt(ps.idx);
												if (cc == '+' || cc == '-') { // sign operator
													cc = s.charAt(ps.idx + 1);
													if (isNumeric(cc)) { // character numeric?
														ps.pos++; // put sign,exp and 1st num into token
														ps.idx++;
														continue; // don't break from TOKEN
													}
												}
											} else
												continue; // period: don't break from TOKEN
										}
									}
								}
								// to evaluate next relevant token, delete whitespaces
								while ( " \t\n\r".indexOf(c) != -1 && ps.idx < s.length()-1) {
									ps.idx++;ps.pos++;
									c = s.charAt(ps.idx);
								}
								// store token
								if (ps.lastToken.type == Token.KEYWORD_FUNCTION) {
									// user function definition
									if ( c != '(' ) {
										errorPos.set(ps.pos,ps.line,errorFile);
										throw new ParseException("Missing parenthesis after user function name "+tokenStr);
									}
									ps.token = new Token(Token.USER_FUNCTION_NAME, tokenStr);
									ps.commandStack.push(ps.token);
								} else if ( c == '(') {
									// function call
									Function fons[] = functions.get(tokenStr);
									if (fons==null) {
										ps.token = new Token(Token.USER_FUNCTION, tokenStr);
									} else {
										ps.token = new Token(Token.FUNCTION, tokenStr);
										ps.token.content = fons;
									}
									ps.token.pos.set(ps.pos-tokenStr.length(),ps.line,errorFile);
									ps.commandStack.push(ps.token);
									//fonToken.push(ps.token);                     // remember function token
								} else {
									// normal token
									ps.token = pushToken(ps.commandStack, tokenStr);
								}
								checkToken(ps.token, ps.lastToken);
							}
							ps.state = IN_UNKNOWN;
						}
						break;
					case IN_OPERATOR:
						// brake from OP if character is not operator
						if (Operator.operatorChars.indexOf(c) == -1) {
							tokenStr = s.substring(ps.startIdx, ps.idx);
							ps.token = pushOperator(ps.commandStack,tokenStr,ps.pLevel);
							ps.state = IN_UNKNOWN;
						}
						break;
				}
				// check for delimiters
				if (delimiterChars.indexOf(c) != -1) {
					switch (c) {
						case '\"': // begin of string literal
							ps.state = IN_STRING;
							ps.startIdx = ps.idx;
							errorPos.set(ps.pos,ps.line,errorFile);
							continue;
						case ';': // check for separator
							//if (ps.pLevel>0) {
							//    errorPos.set(ps.pos,ps.line,errorFile);
							//    throw new ParseException("Parenthesis not closed at separator");
							//}
							// create new token to store position info
							ps.token = new Token(Token.SEPARATOR);
							ps.token.pos.set(ps.pos,ps.line,errorFile);
							ps.commandStack.push(ps.token);
							ps.state = IN_UNKNOWN;
							continue;
						case '?': // check for ternary operator
							// create new token to store position info
							ps.token = new Token(Token.TERNARY_QMARK);
							ps.token.pos.set(ps.pos,ps.line,errorFile);
							ps.commandStack.push(ps.token);
							ps.state = IN_UNKNOWN;
							continue;
						case ':': // check for colon
							// create new token to store position info
							ps.token = new Token(Token.COLON);
							ps.token.pos.set(ps.pos,ps.line,errorFile);
							ps.commandStack.push(ps.token);
							ps.state = IN_UNKNOWN;
							continue;
						case ',': // check for function separators (",")
							ps.token = new Token(Token.FUNCTION_SEPARATOR);
							ps.token.pos.set(ps.pos,ps.line,errorFile);
							ps.commandStack.push(ps.token);
							ps.state = IN_UNKNOWN;
							if (ps.fLevel > 0 || ufInHeader) {
								// create new token to store pos info
								if (!ufInHeader)
									paramNum++;
								continue;
							}
							//errorPos.set(ps.pos,ps.line,errorFile);
							//throw new ParseException("Komma operator outside function context.");
							break;
						case '{': // open brace
							ps.braceLevel++;
							// create new token to store pos info
							ps.token = new Token(Token.BRACE_OPEN);
							ps.token.pos.set(ps.pos,ps.line,errorFile);
							ps.commandStack.push(ps.token);
							ps.state = IN_UNKNOWN;
							continue;
						case '}': // closing brace
							ps.braceLevel--;
							// create new token to store pos info
							ps.token = new Token(Token.BRACE_CLOSE);
							ps.token.pos.set(ps.pos,ps.line,errorFile);
							ps.commandStack.push(ps.token);
							ps.state = IN_UNKNOWN;
							continue;
						case '[': // open bracket
							ps.bracketLevel++;
							// create new token to store pos info
							ps.token = new Token(Token.BRACKET_OPEN);
							ps.token.pos.set(ps.pos,ps.line,errorFile);
							ps.commandStack.push(ps.token);
							ps.state = IN_UNKNOWN;
							continue;
						case ']': // closing bracket
							ps.bracketLevel--;
							// create new token to store pos info
							ps.token = new Token(Token.BRACKET_CLOSE);
							ps.token.pos.set(ps.pos,ps.line,errorFile);
							ps.commandStack.push(ps.token);
							ps.state = IN_UNKNOWN;
							continue;
						case '(' : // check for open parenthesis
							if (ufInHeader) {
								errorPos.set(ps.pos,ps.line,errorFile);
								throw new ParseException("Unexpected parenthesis inside user function header "+tokenStr);
							}
							switch (ps.token.type) {
								case Token.USER_FUNCTION_NAME:
									ufInHeader = true; // we're inside the user function header definition
									break;
								case Token.FUNCTION:
								case Token.USER_FUNCTION:
									//fpLevel.push(new Integer(ps.pLevel));
									ps.fLevel++;
									fParamNum.push(new Integer(paramNum));
									paramNum=0;
									break;
								case Token.OPERATOR:
								case Token.SEPARATOR:
								case Token.PARENTHESIS_OPEN:
								case Token.START:
								case Token.BRACKET_OPEN:
								case Token.BRACE_OPEN:
								case Token.BRACE_CLOSE:
								case Token.FUNCTION_SEPARATOR:
									// special checking for "casting" ops like (int)
									if (s.length() - ps.idx > 2) {// at least three characters "(x)"
										if (!isNumeric(s.charAt(ps.idx + 1))) {
											for (int i = ps.idx + 2; i < s.length(); i++) {
												c = s.charAt(i);
												if (Operator.operatorChars.indexOf(c) > -1)
													break;
												if (c == ')') {
													// normal case outside loop for speedup
													tokenStr = s.substring(ps.idx, i + 1);
													Operator ops[] = operators.get(tokenStr);
													if (ops != null) {
														tok = new Token(Token.OPERATOR,tokenStr);
														tok.pos.set(ps.pos-tokenStr.length(),ps.line,errorFile);
														ps.commandStack.push(tok);
														ps.token = tok;
														ps.pos += i-ps.idx;
														ps.idx = i;
														ps.token.pLevel = ps.pLevel;
														ps.state = IN_UNKNOWN;
														continue parseLoop;
													}
												}
											}
										}
									}
									break;
								case Token.KEYWORD_IF:
								case Token.KEYWORD_FOR:
								case Token.KEYWORD_FOREACH:
								case Token.KEYWORD_WHILE:
								case Token.TERNARY_QMARK:
								case Token.COLON:
								case Token.VARIABLE:
									// just to avoid the default case
									break;
									default:
										// no value possible before opening parenthesis
										errorPos.set(ps.pos,ps.line,errorFile);
										throw new ParseException("Unexpected parenthesis after token "+tokenStr);
							}
							ps.pLevel++;
							// create new token to store pos info
							ps.token = new Token(Token.PARENTHESIS_OPEN);
							ps.token.pos.set(ps.pos,ps.line,errorFile);
							ps.commandStack.push(ps.token);
							ps.state = IN_UNKNOWN;
							continue;
						case ')':
							ps.pLevel--;
							if (ps.pLevel < 0) {
								errorPos.set(ps.pos,ps.line,errorFile);
								throw new ParseException("Parenthesis closed too often.");
							}
							// inside user function header definition ?
							ufInHeader = false;
							// create new token to store pos info
							ps.token = new Token(Token.PARENTHESIS_CLOSE);
							ps.token.pos.set(ps.pos,ps.line,errorFile);
							ps.commandStack.push(ps.token);
							ps.state = IN_UNKNOWN;
							continue;
					}
					// check for operators
				} else if (Operator.operatorChars.indexOf(c) > -1) {
					// check comments
					if (c=='/' && ps.idx < s.length()-1 ) {
						int newstate;
						switch (s.charAt(ps.idx+1)) {
							case '/':
								// line comment
								newstate = IN_LINE_COMMENT;
								break;
							case '*':
								//block comment
								newstate = IN_BLOCK_COMMENT;
								break;
							default:
								newstate=ps.state;
						}
						if (newstate == IN_LINE_COMMENT || newstate == IN_BLOCK_COMMENT) {
							ps.state = newstate;
							ps.idx+=2;
							continue;
						}
					}
					// operator
					if (ps.state != IN_OPERATOR) {
						// begin of new operator
						ps.startIdx = ps.idx;
						errorPos.set(ps.pos,ps.line,errorFile);
						ps.state = IN_OPERATOR;
					}
				} else {
					// token
					if (ps.state != IN_TOKEN) {
						// start of new token
						ps.startIdx = ps.idx;
						errorPos.set(ps.pos,ps.line,errorFile);
						ps.state = IN_TOKEN;
					}
				}
			}
		if (ps.pLevel > 0) {
			errorPos.set(ps.pos,ps.line,errorFile);
			throw new ParseException("Parenthesis still open.");
		}
		if (ps.state == IN_STRING) {
			errorPos.set(ps.pos,ps.line,errorFile);
			throw new ParseException("String literal not closed");
		}

		// push ending tokens as last tokens
		Token endToken = new Token(Token.END);
		ps.commandStack.push(endToken);
		ps.commandStack.push(endToken);
		ps.commandStack.push(endToken);
		return ps.commandStack;
		} catch (ParseException ex) {
			throw ex;
		} catch (NullPointerException ex) {
			throw new ParseException("Null pointer exception: " + ex.toString());
		}
	}

	/**
	 * Check if a string is an operator or consists of more than one operator. If concatenated, the
	 * largest possible operator(s) from the left is/are extracted and pushed to the commandStack
	 * separately e.g. "+++" -> "++" "+"
	 * @param commandStack Stack to push Token to
	 * @param s String to check for operators
	 * @param pLevel parenthesis level
	 * @return Operator Token
	 * @throws ParseException
	 */
	private Token pushOperator(ALStack<Token> commandStack, String s, int pLevel) throws ParseException {
		Operator ops[];
		Token token;
		// normal case outside loop for speedup
		ops = operators.get(s);
		if (ops != null) {
			token = new Token(Token.OPERATOR,s);
			if (ops.length==1)
				token.content = ops[0]; // resolved
				token.pos.copyFrom(errorPos);
				commandStack.push(token);
				token.pLevel = pLevel;
				return token;
		}
		// now this is either a concatenation of operators or a syntax error
		// search for longest operator on the left (so the end index is decreased)
		String h;
		for (int i=s.length()-1; i>=0; i--) {
			h = s.substring(0,i);
			ops = operators.get(h);
			if (ops != null) {
				token = new Token(Token.OPERATOR, h);
				if (ops.length==1)
					token.content = ops[0]; // resolved
					token.pos.copyFrom(errorPos);
					commandStack.push(token);
					token.pLevel = pLevel;
					errorPos.pos += h.length();
					if (i == s.length())
						return token; // the last possible operator was resolved
					s = s.substring(i,s.length());
					i = s.length()+1; // manipulate loop index for re-entry
			}
		}
		// if the loop is left, whatever is left in s couldn't be resolved
		throw new ParseException("Unknown operator \""+s+"\"");
	}

	/**
	 * Push number or variable name on the commandStack
	 * @param commandStack Stack to push tokens on
	 * @param s String to check for number/variable
	 * @return Created Token
	 * @throws ParseException
	 */
	private Token pushToken(ALStack<Token> commandStack, String s) throws ParseException {
		Token token=null;

		if (!isNumeric(s.charAt(0))) {
			if (s.equalsIgnoreCase("true") || s.equalsIgnoreCase("false")) {
				token = new Token(Token.VALUE, new Value(Boolean.valueOf(s)));
			} else {
				// must be a variable, since it doesn't start with a valid numeric character
				token = new Token(Token.VARIABLE, s);
			}
			token.pos.copyFrom(errorPos);
			commandStack.push(token);
			return token;
		}
		// let's see if this is a hex or binary number (0x..., 0b...)
		try {
			token = new Token(Token.VALUE, new Value(new JNumber(s)));
			token.pos.copyFrom(errorPos);
			commandStack.push(token);
			return token;
		} catch (NumberFormatException ex) {
			throw new ParseException("Invalid number format: "+s);
		}
	}

	/**
	 *  If there are multiple possibilities for an operator (e.g. binary or unary right),
	 *  decide from the context which operator is used or if there's a syntax error.
	 *  This helps evaluating since operator association and priority needs to be known
	 *  @param commandStack Stack to resolve
	 *  @throws ParseException
	 */
	private void resolveOps(ALStack<Token> commandStack) throws ParseException {
		Token token,left,right;
		Operator ops[];
		Operator op;
		// special handling for very first token
		left = new Token(Token.START);
		right = commandStack.get(0);
		// go through all Tokens
		for (int i=0; i<commandStack.size()-3; i++) {
			token = right;
			right = commandStack.get(i+1);
			errorPos.copyFrom(token.pos);
			if (token.type == Token.OPERATOR && token.content == null) {
				ops = operators.get(token.name);
				for (int j=0; j<ops.length; j++) {
					op = ops[j];
					switch (op.type) {
						case Operator.BINARY_LEFT:
						case Operator.BINARY_RIGHT:
							switch (left.type) {
								case Token.OPERATOR: // token to the left could be unary left op
								if ( ((Operator)left.content).type != Operator.UNARY_LEFT )
									break;
								//else fall through
								case Token.VALUE:
								case Token.VARIABLE:
								case Token.PARENTHESIS_CLOSE:
								case Token.BRACE_CLOSE:
								case Token.BRACKET_CLOSE:
									switch (right.type) {
										case Token.VALUE:
										case Token.VARIABLE:
										case Token.PARENTHESIS_OPEN:
										case Token.BRACE_OPEN:
										case Token.BRACKET_OPEN:
										case Token.OPERATOR:
										case Token.FUNCTION:
										case Token.USER_FUNCTION:
											/* @remark
											 * indeed this could also be a unary left operator if the
											 * following operator is unary right.
											 * I trust here on the fact that nobody would create two ops
											 * of the same name which could be unary left and binary
											 */
											 token.content = op; // resolved
											 break;
									}
							}
							break;
						case Operator.UNARY_LEFT:
							switch (left.type) {
								case Token.VALUE:
								case Token.VARIABLE:
								case Token.PARENTHESIS_CLOSE:
								case Token.BRACE_CLOSE:
								case Token.BRACKET_CLOSE:
									switch (right.type) {
										case Token.END:
										case Token.SEPARATOR:
										case Token.PARENTHESIS_CLOSE:
										case Token.BRACE_CLOSE:
										case Token.BRACKET_CLOSE:
										case Token.OPERATOR:
											/* @remark
											 * indeed this could also be a unary left operator if the
											 * following operator is binary or unary left.
											 * I trust here on the fact that nobody would create two ops
											 * of the same name which could be unary left and binary
											 */
											 token.content = op; // resolved
											 break;
									}
							}
							break;
						case Operator.UNARY_RIGHT:
							switch (right.type) {
								case Token.OPERATOR:
									// op to the right can only be unary right but right now we can't check yet
									// fall through
								case Token.VALUE:
								case Token.VARIABLE:
								case Token.PARENTHESIS_OPEN:
								case Token.BRACKET_OPEN:
								case Token.BRACE_OPEN:
								case Token.FUNCTION:
								case Token.USER_FUNCTION:
									switch (left.type) {
										case Token.OPERATOR: // operator to the left mustn't be unary left
										if ( ((Operator)left.content).type == Operator.UNARY_LEFT )
											break;
										// else fall through
										case Token.START:
										case Token.SEPARATOR:
										case Token.FUNCTION_SEPARATOR:
										case Token.PARENTHESIS_OPEN:
										case Token.BRACKET_OPEN:
										case Token.BRACE_OPEN:
											token.content = op; // resolved!
											break;
									}
							}
					}
					if (token.content != null)
						break;
				}
				if (token.content == null)
					throw new ParseException("Operator syntax error: "+token.name);
			}
			left = token;
		}
	}

	/**
	 * Add parse listener
	 * @param listener JParseListener
	 */
	public void addParseListener(JParseListener listener) {
		parseListener = listener;
	}

	/**
	 * Output string by creating a JParseEvent and calling the configured parseListener
	 * @param s String to output
	 */
	public void output(String s) {
		parseListener.parseEvent(new JParseEvent(s));
		/*
		try {
			Thread.sleep(0,10);
		} catch (InterruptedException ex) {}
		 */
	}


	/* TESTING GROUND */

	/**
	 * Pop from commandStack to RPN stack
	 * Sub function of createRPNStack()
	 * The stack is used to handle priorities, parenthesis nesting level and association
	 * @param evStack evaluation stack to pop from
	 * @param rpnStack RPNstack to push to
	 * @param pLevel Current parenthesis level. Important to stop evaluating the stack if the Operator
	 *        nesting level differs from the current nesting level to handle parenthesis nesting.
	 * @param precedence commandStack evaluatuin stops if the current priority is higher (precedence lower) than
	 *        that of an operator on the stack.
	 * @param association The association of the current operator or LEFT is the stack is evaluated at a separator,
	 *        closing bracket or end of expression.
	 * @throws ParseException
	 */
	private void popStack(ALStack<Token> evStack, ALStack<Token> rpnStack, int pLevel, int precedence, int association) throws ParseException {
		Token token;
		Operator op;

		do {
			// just peek, don't pop yet
			token = evStack.peek();
			if ( token.pLevel != pLevel || token.fLevel > 0 )
				break;
			if (token.type != Token.OPERATOR) {
				errorPos.copyFrom(token.pos);
				throw new ParseException("Token on stack is no operator: "+token.toString());
			}
			op = (Operator)token.content;
			// don't pop if the next operator on the stack doesn't have a higher (numerically smaller) priority
			// than the given precedence or if the parenthesis level of the next operator on the stack is different
			// from the given precedence
			if ( op.precedence > precedence || (association==Operator.RIGHT && (op.association == Operator.RIGHT)))
				break;
			evStack.pop();
			rpnStack.push(token);
		} while (!evStack.isEmpty());
	}

	private static final int F_SEPARATOR = 0;
	private static final int F_PARAMETER = 1;
	private static final int F_HEADER_OK = 2;


	/**
	 * Create function call
	 * Called from createRPNStack()
	 * @param cmdStack Command Stack in which the user function is defined
	 * @param rpnStack RPN Stack to push indexed access on
	 * @param position Current position of inside the command stack (pointing at "function")
	 * @param pLevel Current parenthesis nesting level
	 * @return position of command stack after the evaluation
	 * @throws ParseException
	 */
	private int createFunctionCall(ALStack<Token> cmdStack, ALStack<Token> rpnStack, int position, int pLevel) throws ParseException {
		int i;
		Token token;
		Token fNameToken; // just for error output
		String fName;
		// first token must be function token (not completely resolved yet)
		fNameToken = token = cmdStack.get(position);
		fName = token.name;
		Function fons[] = (Function []) token.content; // either reference to builtin function or null (user function)

		// next token must be an opening parenthesis "("
		token = cmdStack.get(position+1);
		if (token.type != Token.PARENTHESIS_OPEN) {
			errorPos.copyFrom(token.pos);
			throw new ParseException("Unexpected token after function name "+fName);
		}
		// search for closing parenthesis ")" and store variable names
		int pLevelStart = ++pLevel;
		int bLevel = 0; // brace level
		int bLevelStart = 0;
		int params=0;
		ALStack<Token> expRPNStack;
		position += 2;
		search_end:
			for (i = position; i<cmdStack.size()-2; i++) {
				token = cmdStack.get(i);
				switch (token.type) {
					case Token.BRACE_OPEN:
						bLevel++;
						break;
					case Token.BRACE_CLOSE:
						bLevel--;
						break;
					case Token.PARENTHESIS_OPEN:
						pLevel++;
						break;
					case Token.PARENTHESIS_CLOSE:
						if (pLevel == pLevelStart && bLevel==bLevelStart) {
							if ( cmdStack.get(i-1).type != Token.PARENTHESIS_OPEN) {
								expRPNStack = createRPNStack(cmdStack, position, i-1, pLevel);
								rpnStack.addAll(expRPNStack);
								params++;
							}
							break search_end;
						}
						pLevel--;
						break;
					case Token.FUNCTION_SEPARATOR:
						if (pLevel == pLevelStart && bLevel==bLevelStart) {
							expRPNStack = createRPNStack(cmdStack, position, i-1, pLevel);
							rpnStack.addAll(expRPNStack);
							position = i+1;
							params++;
						}
						break;
				}
			}
		if (pLevel > pLevelStart)
			throw new ParseException("Parenthesis not closed before end of expression.");
		if (pLevel < pLevelStart)
			throw new ParseException("Parenthesis closed too often before end of expression.");
		if (bLevel > 0)
			throw new ParseException("Brace not closed before end of expression.");
		if (bLevel < 0)
			throw new ParseException("Brace clsoed too often before end of expression.");

		// builtin function ?
				token = fNameToken; // reuse original token

				if (fons != null) {
					Function fon = null;
					for (int j=0; j<fons.length; j++)
						if (fons[j].paramNum == params) {
							fon = fons[j];
							token.content = fon;
							break;
						}
					if (fon == null) {
						errorPos.copyFrom(token.pos);
						throw new ParseException("Wrong number of parameters for function "+token.name);
					}
				} else {
					// user function
					// just store number of parameters
					token.content = new Integer(params);
				}
				// push function token on rpn stack
				rpnStack.push(token);
				position = i; // should point at closing brace
				return position;
	}

	/**
	 * Create user function
	 * Called from createRPNStack()
	 * @param cmdStack Command Stack in which the user function is defined
	 * @param position Current position of inside the command stack (pointing at "function")
	 * @param pLevel Current parenthesis nesting level
	 * @return position of command stack after the evaluation
	 * @throws ParseException
	 */
	private int createUserFunction(ALStack<Token> cmdStack, int position, int pLevel) throws ParseException {
		int i;
		int state;
		int blvl;
		Token token;
		Token fNameToken; // just for error output
		String ufName;
		ArrayList<String> ufParam = new ArrayList<String>();
		String ufParamStr[];
		UserFunction uf;
		// next token must be function name, misintepreted as variable
		fNameToken = token = cmdStack.get(position+1);
		if (token.type != Token.USER_FUNCTION_NAME) {
			errorPos.copyFrom(token.pos);
			throw new ParseException("Unexpected token after keyword 'function' ");
		}
		ufName = token.name;
		// next token must be an opening parenthesis "("
		token = cmdStack.get(position+2);
		i = position+3;
		if (token.type != Token.PARENTHESIS_OPEN) {
			errorPos.copyFrom(token.pos);
			throw new ParseException("Unexpected token after user function name "+ufName);
		}
		// search for closing parenthesis ")" and store variable names
		// special case: empty parameter list
		if (cmdStack.get(position+3).type != Token.PARENTHESIS_CLOSE) {
			state = F_PARAMETER; // now we expect a token
			checkHeader:
				for (; i<cmdStack.size()-2; i++) {
					token = cmdStack.get(i);
					if (state == F_PARAMETER) {
						if (token.type != Token.VARIABLE) {
							errorPos.copyFrom(token.pos);
							throw new ParseException("Unexpected token in header of user function "+ufName);
						}
						ufParam.add(token.name);
						state = F_SEPARATOR; // now we expect a separator
					} else {
						switch (token.type) {
							case Token.FUNCTION_SEPARATOR:
								state = F_PARAMETER;
								break;
							case Token.PARENTHESIS_CLOSE:
								state = F_HEADER_OK;
								break checkHeader;
							default:
								errorPos.copyFrom(token.pos);
								throw new ParseException("Unexpected token in header of user function "+ufName);
						}
					}
				}
			if (state != F_HEADER_OK) {
				errorPos.copyFrom(token.pos);
				throw new ParseException("Syntax error in header of user function "+ufName);
			}
		}
		// check for opening brace
		position = i+1; // should point at opening brace
		token = cmdStack.get(position);
		if (token.type != Token.BRACE_OPEN) {
			errorPos.copyFrom(token.pos);
			throw new ParseException("Missing opening brace after header of user function "+ufName);
		}
		// search closing brace
		blvl = 1; position++;
		for (i = position; i<cmdStack.size()-2; i++) {
			token = cmdStack.get(i);
			if (token.type == Token.BRACE_OPEN)
				blvl++;
			else if (token.type == Token.BRACE_CLOSE) {
				if (--blvl==0)
					break;
			}
		}
		if (blvl != 0) {
			errorPos.copyFrom(fNameToken.pos);
			throw new ParseException("Missing closing brace at the end of user function "+ufName);
		}

		// create RPN stack
		// Note: since inside the body another function could be defined,
		// name space handling is added here
		userFunctions.enterNameSpace(ufName);
		ALStack<Token> ufRPNStack = createRPNStack(cmdStack,position,i-1,pLevel);
		userFunctions.leaveNameSpace();
		position = i; // should point at closing brace

		// create user function
		ufParamStr = new String[ufParam.size()];
		ufParamStr = ufParam.toArray(ufParamStr);
		uf = new UserFunction(ufParamStr, ufRPNStack);
		userFunctions.put(ufName, uf);
		// might throw parseexception
		errorPos.copyFrom(fNameToken.pos);
		return position;
	}


	/**
	 * Create list token on the rpn stack
	 * Called from createRPNStack()
	 * lists contain values separates by "," and enclosed by curly braces. Can also contain lists
	 * e.g. { value_1, value_2, { value_a, value_b} }
	 * @param cmdStack Command Stack in which the conditional is defined
	 * @param rpnStack RPN Stack to push indexed access on
	 * @param position Current position of inside the command stack (pointing at "{")
	 * @param pLevel Current parenthesis nesting level
	 * @return position of command stack after the evaluation
	 * @throws ParseException
	 */
	private int createList(ALStack<Token> cmdStack, ALStack<Token> rpnStack, int position, int pLevel) throws ParseException {
		int element=0;
		int i;
		int bLevel=1;
		Token token = cmdStack.get(position);
		errorPos.copyFrom(token.pos); // points at "{"

		createList:
			for (i = position+1; i<cmdStack.size()-2; i++) {
				token = cmdStack.get(i);
				switch (token.type) {
					case Token.END:
						break createList; // just for robustness
					case Token.FUNCTION_SEPARATOR:
						// next element
						break;
					case Token.BRACE_OPEN:
						// recurse (list in list)
						element++;
						i  = createList(cmdStack, rpnStack, i, pLevel);
						break;
					case Token.BRACE_CLOSE:
						bLevel--;
						// list finished
						break createList;
					case Token.VALUE:
					case Token.VARIABLE:
						// single value -> no need to evaluate
						Token next = cmdStack.get(i+1);
						if (next.type == Token.FUNCTION_SEPARATOR || next.type == Token.BRACE_CLOSE) {
							element++;
							rpnStack.push(new Token(token));
							break;
						}
						// fall through: evaluate
					default:
						// some other token -> evaluate
						// find end of expression (either "," or "}", where pLevel = pLevelStart
						int pLevelStart = pLevel;
						int bLevelStart = bLevel;
						int brLevel = 0; // bracket level
						int j;
						search_end:
							for (j = i; j<cmdStack.size()-2; j++) {
								token = cmdStack.get(j);
								switch (token.type) {
									case Token.PARENTHESIS_OPEN:
										pLevel++;
										break;
									case Token.PARENTHESIS_CLOSE:
										pLevel--;
										break;
									case Token.BRACKET_OPEN:
										brLevel++;
										break;
									case Token.BRACKET_CLOSE:
										brLevel--;
										break;
									case Token.BRACE_OPEN:
										bLevel++;
										break;
									case Token.BRACE_CLOSE:
										bLevel--;
										if (bLevel == bLevelStart-1)
											break search_end;
										break;
									case Token.FUNCTION_SEPARATOR:
										if (pLevel == pLevelStart && bLevel == bLevelStart)
											break search_end;
										break;
								}
							}
						if (brLevel > 0)
							throw new ParseException("Bracket not closed before end of expression.");
						if (pLevel > pLevelStart)
							throw new ParseException("Parenthesis not closed before end of expression.");
						if (pLevel < pLevelStart)
							throw new ParseException("Parenthesis closed too often before end of expression.");
						ALStack<Token> expRPNStack = createRPNStack(cmdStack, i, j-1, pLevel);
						element++;
						rpnStack.addAll(expRPNStack);
						i = j;
						if (bLevel==0)
							break createList;
				}
			}
		if (bLevel != 0)
			throw new ParseException("Brace not closed before end of expression.");
		token = new Token(Token.LIST);
		token.content = new Integer(element);
		rpnStack.add(token);
		return i; // should point at closing brace "}"
	}


	/**
	 * Create indexed access from brackets
	 * Called from createRPNStack()
	 * e.g. variable[index] is transformed into "variable index INDEXED_ACCESS"
	 * @param cmdStack Command Stack in which the conditional is defined
	 * @param rpnStack RPN Stack to push indexed access on
	 * @param position Current position of inside the command stack (pointing at '[')
	 * @param pLevel Current parenthesis nesting level
	 * @return position of command stack after the evaluation
	 * @throws ParseException
	 */
	private int createIndexedAccess(ALStack<Token> cmdStack, ALStack<Token> rpnStack, int position, int pLevel) throws ParseException {
		Token token;
		Token valueToken=null;
		int i;

		token = cmdStack.get(position);
		errorPos.copyFrom(token.pos); // points at "["

		do {
			int bkLevel=1;
			int values=0;
			// find closing bracket
			searchBracket:
				for (i = position+1; i<cmdStack.size()-2; i++) {
					token = cmdStack.get(i);
					switch (token.type) {
						case Token.BRACKET_OPEN:
							bkLevel++;
							break;
						case Token.BRACKET_CLOSE:
							bkLevel--;
							if (bkLevel == 0)
								break searchBracket;
							break;
						case Token.VALUE:
							values++;
							valueToken = token;
							break;
						default:
							values = 2; // just mark as "more than one value"
					}
				}
			// check for open brackets
			if (bkLevel != 0)
				throw new ParseException("Missing closing bracket");
			// check for empty brackets
			if (i - position < 2)
				throw new ParseException("Empty brackets");
			// check for simple case
			if (values==1) {
				Value val = (Value)valueToken.content;
				if (val.type != Value.NUMBER)
					throw new ParseException("Invalid type for index");
				token = rpnStack.peek(); // this is the variable token
				StringBuffer sb = new StringBuffer(token.name);
				JNumber num = (JNumber)val.value;
				if (!num.isInteger() || num.signum() < 0)
					throw new ParseException("Only integers >=0 allowed for index");
				sb.append("@");
				sb.append(num.toString());
				// replace variable name
				token.name = sb.toString();
			} else {
				// complex case: create INDEXED_ACCESS on stack
				// the variable to be indexed is already on the stack, now push the index expression
				ALStack<Token> expRPNStack = createRPNStack(cmdStack, position+1, i-1, pLevel);
				rpnStack.addAll(expRPNStack);
				token = new Token(Token.INDEXED_ACCESS);
				token.pos.copyFrom(errorPos);
				rpnStack.push(token);
			}
			position = i+1; // points at character after "]"
		} while ( cmdStack.get(position).type == Token.BRACKET_OPEN );
		return i;
	}

	/**
	 * Create if/else conditional on the rpn stack
	 * Called from createRPNStack()
	 * if ( <exp1> ) { <exp2> } else { <exp3> }
	 * pushed on stack as
	 * <exp1> JMP_FALSE {size exp2 + 1} <exp2> JMP {size exp3} <exp3>
	 * @param cmdStack Command Stack in which the conditional is defined
	 * @param rpnStack RPN STack to push for loop to
	 * @param position Current position of inside the command stack (pointing at "if")
	 * @param pLevel Current parenthesis nesting level
	 * @return position of command stack after the evaluation
	 * @throws ParseException
	 */
	private int createConditional(ALStack<Token> cmdStack, ALStack<Token> rpnStack, int position, int pLevel) throws ParseException {
		int i;
		int lvl;
		Token token;
		ALStack<Token> exp3RPNStack = null;
		boolean elseBranch=false;

		token = cmdStack.get(position);
		errorPos.copyFrom(token.pos); // points at if
		// next token must be an opening parenthesis
		token = cmdStack.get(position+1);
		if (token.type != Token.PARENTHESIS_OPEN)
			throw new ParseException("Missing opening parenthesis after keyword 'if'");
		errorPos.copyFrom(token.pos);
		pLevel++;
		// search for closing parenthesis ")"
		lvl = 1; position += 2;
		for (i = position; i<cmdStack.size()-2; i++) {
			token = cmdStack.get(i);
			if (token.type == Token.PARENTHESIS_OPEN)
				lvl++;
			else if (token.type == Token.PARENTHESIS_CLOSE) {
				if (--lvl==0)
					break;
			}
		}
		if (lvl != 0)
			throw new ParseException("Missing closing parenthesis after keyword 'if'");
		// create rpnStack from the condition inside the parenthesises
		ALStack<Token> exp1RPNStack = createRPNStack(cmdStack,position,i-1, pLevel);
		pLevel--;
		// check for opening brace
		position = i+1; // should point at opening brace
		token = cmdStack.get(position);
		if (token.type != Token.BRACE_OPEN)
			throw new ParseException("Missing opening brace after keyword 'if'");
		errorPos.copyFrom(token.pos);
		// search closing brace
		lvl = 1; position++;
		for (i = position; i<cmdStack.size()-2; i++) {
			token = cmdStack.get(i);
			if (token.type == Token.BRACE_OPEN)
				lvl++;
			else if (token.type == Token.BRACE_CLOSE) {
				if (--lvl==0)
					break;
			}
		}
		if (lvl != 0)
			throw new ParseException("Missing closing brace after keyword 'if'");
		errorPos.copyFrom(token.pos);
		// create rpnStack from the if body inside the braces
		ALStack<Token> exp2RPNStack = createRPNStack(cmdStack,position,i-1,pLevel);
		position = i; // should point at closing brace
		// check for 'else' branch
		token = cmdStack.get(position+1);
		if (token.type == Token.KEYWORD_ELSE) {
			elseBranch = true;
			position+=2; // points at opening brace
			errorPos.copyFrom(token.pos);
			// check for opening brace
			token = cmdStack.get(position);
			if (token.type != Token.BRACE_OPEN)
				throw new ParseException("Missing opening brace after keyword 'else'");
			// search closing brace and create user function command stack
			lvl = 1; position++;
			for (i = position; i<cmdStack.size()-2; i++) {
				token = cmdStack.get(i);
				if (token.type == Token.BRACE_OPEN)
					lvl++;
				else if (token.type == Token.BRACE_CLOSE) {
					if (--lvl==0)
						break;
				}
			}
			if (lvl != 0)
				throw new ParseException("Missing closing brace after keyword 'else'");
			errorPos.copyFrom(token.pos);
			// create rpnStack from the else body inside the braces
			exp3RPNStack = createRPNStack(cmdStack,position,i-1,pLevel);
			position = i; // should point at closing brace
		}
		// shove the conditional onto the RPN stack
		rpnStack.add(new Token(Token.STORE_VSTACK_SIZE));
		rpnStack.addAll(exp1RPNStack);
		token = new Token(Token.JMP_FALSE);
		token.content = new Integer(exp2RPNStack.size()+ ((elseBranch)?1:0)); // store offset to first token after body
		rpnStack.add(token);
		// shove 'if' body on the RPN stack
		rpnStack.addAll(exp2RPNStack);
		if (elseBranch) {
			token = new Token(Token.JMP);
			token.content = new Integer(exp3RPNStack.size()); // store offset to first token after else body
			rpnStack.add(token);
			// shove 'else' body on the RPN stack
			rpnStack.addAll(exp3RPNStack);
		}
		rpnStack.add(new Token(Token.PUSH_DUMMY_IF_VSTACK_EQUAL));
		return position;
	}


	/**
	 * Create ternary (?/:) conditional on the rpn stack
	 * Called from createRPNStack()
	 * ( <exp1> ) ? ( <exp2> ) : ( <exp3> )
	 * pushed on stack as
	 * <exp1> JMP_FALSE {size exp2 + 1} <exp2> JMP {size exp3} <exp3>
	 * Note that <expr1> is already on the RPN stack when this function is called.
	 * @param cmdStack Command Stack in which the conditional is defined
	 * @param rpnStack RPN Stack to push for loop to
	 * @param position Current position of inside the command stack (pointing at "if")
	 * @param pLevel Current parenthesis nesting level
	 * @return position of command stack after the evaluation
	 * @throws ParseException
	 */
	private int createTernary(ALStack<Token> cmdStack, ALStack<Token> rpnStack, int position, int pLevel) throws ParseException {
		int i;
		int lvl;
		Token token;

		token = cmdStack.get(position);
		errorPos.copyFrom(token.pos); // points at "?"
		// next token must be an opening parenthesis, a value, a variable or a right side unary operator
		token = cmdStack.get(position+1);
		lvl = 0;
		switch (token.type) {
			case Token.PARENTHESIS_OPEN:
				lvl++;
				// fall through
			case Token.VALUE:
			case Token.VARIABLE:
				break; // ok
			case Token.OPERATOR:
				if ( ((Operator)token.content).type == Operator.UNARY_RIGHT)
					break;
			default:
				throw new ParseException("Unexpected token after ternary operator '?'");
		}
		errorPos.copyFrom(token.pos);
		// search for ":"
		position += 1; // points to start of exp2
		for (i = position+1; i<cmdStack.size()-2; i++) {
			token = cmdStack.get(i);
			if (token.type == Token.PARENTHESIS_OPEN)
				lvl++;
			else if (token.type == Token.PARENTHESIS_CLOSE)
				lvl--;
			else if (token.type == Token.COLON)
				break;
		}
		if (lvl != 0)
			throw new ParseException("Missing parenthesis before ternary operator ':'");
		if (token.type != Token.COLON)
			throw new ParseException("Missing ':' after ternary operator '?'");
		// create rpnStack for <exp2>
		ALStack<Token> exp2RPNStack = createRPNStack(cmdStack,position,i-1, pLevel);
		// check for end of <exp3>
		// if the token right of ":" was a value or variable, this is exp3
		// if the token is an open parenthesis, search for the closing parenthesis
		// if the token is a unary right side operator the tokens can be the same as for ":"
		// check for opening brace
		lvl = 0;
		i++;
		position = i; // first token of <exp3>
		token = cmdStack.get(position);
		loop: for (;;) {
			switch (token.type) {
				case Token.PARENTHESIS_OPEN:
					lvl++;
					loop2: for (i++; i<cmdStack.size()-2; i++) {
						token = cmdStack.get(i);
						if (token.type == Token.PARENTHESIS_OPEN)
							lvl++;
						else if (token.type == Token.PARENTHESIS_CLOSE) {
							if (--lvl==0)
								break loop2;
						}
					}
					if (lvl != 0)
						throw new ParseException("Missing parenthesis after ternary operator ':'");
					// fall through
				case Token.VALUE:
				case Token.VARIABLE:
					break loop; // ok
				case Token.OPERATOR:
					if ( ((Operator)token.content).type == Operator.UNARY_RIGHT) {
						i++;   // look at right side of unary operator
						token = cmdStack.get(i);
						break; // continue with the for loop
					}
				default:
					throw new ParseException("Unexpected token after ternary sub-operator ':'");
			}
			errorPos.copyFrom(token.pos);
		}
		// create rpnStack from the if body inside the braces
		// token should point at begin of <exp3>, i should point at end of <exp3>
		ALStack<Token> exp3RPNStack = createRPNStack(cmdStack,position,i,pLevel);
		position = i; // should point as last token of <exp3>
		// shove the conditional onto the RPN stack (note: expr1 is already on the stack)
		// JMP_FALSE {size exp2 + 1} <exp2> JMP {size exp3} <exp3>
		token = new Token(Token.JMP_FALSE);
		token.content = new Integer(exp2RPNStack.size()+ 1);
		rpnStack.add(token);
		rpnStack.addAll(exp2RPNStack);
		token = new Token(Token.JMP);
		token.content = new Integer(exp3RPNStack.size()); // store offset to first token after else body
		rpnStack.add(token);
		rpnStack.addAll(exp3RPNStack);
		return position;
	}

	/**
	 * Create for loop on the rpn stack
	 * Called from createRPNStack()
	 * "for ( <exp1>; <exp2>; <exp3>) { <exp4> }"
	 * pushed on stack as
	 * <exp1> <exp2> JMP_FALSE {size exp<3> + size exp<4> + 1}
	 * <exp4> <exp3> JMP {- size exp<4> - size exp<3> - size exp<2> -1}
	 * @param cmdStack Command Stack in which the conditional is defined
	 * @param rpnStack RPN STack to push for loop to
	 * @param position Current position of inside the command stack (pointing at "for")
	 * @param pLevel Current parenthesis nesting level
	 * @return position of command stack after the evaluation
	 * @throws ParseException
	 */
	private int createForLoop(ALStack<Token> cmdStack, ALStack<Token> rpnStack, int position, int pLevel) throws ParseException {
		int i;
		int lvl;
		Token token = cmdStack.get(position);
		Token pop1 = new Token(Token.POP_VALUE);
		pop1.content = new Integer(1);
		errorPos.copyFrom(token.pos); // points at "for"
		// next token must be an opening parenthesis
		token = cmdStack.get(position+1);
		if (token.type != Token.PARENTHESIS_OPEN)
			throw new ParseException("Missing opening parenthesis after keyword 'for'");
		pLevel++;
		errorPos.copyFrom(token.pos);
		// search for first separator ";" and create command stack for init expression
		position += 2;
		for (i = position; i<cmdStack.size()-2; i++) {
			token = cmdStack.get(i);
			if (token.type == Token.SEPARATOR)
				break;
		}
		if (token.type != Token.SEPARATOR)
			throw new ParseException("Missing 1st separator after keyword 'for'");
		// create rpnStack from the initialization condition
		ALStack<Token> exp1RPNStack = createRPNStack(cmdStack, position, i-1, pLevel);
		rpnStack.addAll(exp1RPNStack);
		if (exp1RPNStack.size()>0)
			rpnStack.add(pop1);
		// check for next separator
		position = i+1; // should point at first token after 1st separator
		for (i = position; i<cmdStack.size()-2; i++) {
			token = cmdStack.get(i);
			if (token.type == Token.SEPARATOR)
				break;
		}
		if (token.type != Token.SEPARATOR)
			throw new ParseException("Missing 2nd separator after keyword 'for'");
		// create rpnStack from the loop break condition
		ALStack<Token> exp2RPNStack = createRPNStack(cmdStack, position, i-1, pLevel);
		rpnStack.addAll(exp2RPNStack);
		// search for closing parenthesis ")"
		position = i+1; // should point at first token after 2nd separator
		lvl = 1;
		for (i = position; i<cmdStack.size()-2; i++) {
			token = cmdStack.get(i);
			if (token.type == Token.PARENTHESIS_OPEN)
				lvl++;
			else if (token.type == Token.PARENTHESIS_CLOSE) {
				if (--lvl==0)
					break;
			}
		}
		if (lvl != 0)
			throw new ParseException("Missing closing parenthesis after keyword 'for'");
		// create rpnStack from the initialization condition
		ALStack<Token> exp3RPNStack = createRPNStack(cmdStack, position, i-1, pLevel);
		pLevel--;
		position = i+1; // should point at opening brace
		// check for opening brace
		token = cmdStack.get(position);
		if (token.type != Token.BRACE_OPEN)
			throw new ParseException("Missing opening brace after keyword 'for'");
		errorPos.copyFrom(token.pos);
		// search closing brace
		position++;
		lvl = 1;
		for (i = position; i<cmdStack.size()-2; i++) {
			token = cmdStack.get(i);
			if (token.type == Token.BRACE_OPEN)
				lvl++;
			else if (token.type == Token.BRACE_CLOSE) {
				if (--lvl==0)
					break;
			}
		}
		if (lvl != 0)
			throw new ParseException("Missing closing brace after keyword 'for'");
		errorPos.copyFrom(token.pos);
		// create rpnStack from the if body inside the braces
		ALStack<Token> exp4RPNStack = createRPNStack(cmdStack, position, i-1, pLevel);
		position = i; // should point at closing brace
		// shove the conditional onto the RPN stack
		// conditional jump <exp2> JMP_FALSE
		token = new Token(Token.JMP_FALSE);
		rpnStack.add(token);
		rpnStack.add(new Token (Token.STORE_VSTACK_SIZE));
		rpnStack.addAll(exp4RPNStack);
		int size4 = exp4RPNStack.size()+2;
		rpnStack.add(new Token (Token.POP_IF_VSTACK_UNEQUAL));
		rpnStack.addAll(exp3RPNStack);
		int size3 = exp3RPNStack.size();
		if (size3>0) {
			rpnStack.add(pop1);
			size3++;
		}
		// fix jmp offset
		token.content = new Integer(size3 + size4+ 1); // store offset to first token after body
		// unconditional JMP to start of loop
		token = new Token(Token.JMP);
		token.content = new Integer(-exp2RPNStack.size() - size3 - size4 - 2);
		rpnStack.add(token);
		return position;
	}


	/**
	 * Create foreach loop on the rpn stack
	 * Called from createRPNStack()
	 * "foreach ( <var>; <list> ) { <body> }"
	 * pushed on stack as
	 * <list> FOREACH_INIT
	 * <var> FOREACH_NEXT {size <body> + 1}
	 * <body> JMP {- size <body> - 2}
	 * @param cmdStack Command Stack in which the conditional is defined
	 * @param rpnStack RPN STack to push for loop to
	 * @param position Current position of inside the command stack (pointing at "for")
	 * @param pLevel Current parenthesis nesting level
	 * @return position of command stack after the evaluation
	 * @throws ParseException
	 */
	private int createForeachLoop(ALStack<Token> cmdStack, ALStack<Token> rpnStack, int position, int pLevel) throws ParseException {
		int i;
		int lvl;
		Token token = cmdStack.get(position);
		//Token list = null;
		Token var = null;
		Token pop1 = new Token(Token.POP_VALUE);
		pop1.content = new Integer(1);
		errorPos.copyFrom(token.pos); // points at "foreach"
		// next token must be an opening parenthesis
		token = cmdStack.get(++position);
		if (token.type != Token.PARENTHESIS_OPEN)
			throw new ParseException("Missing opening parenthesis after keyword 'foreach'");
		pLevel++;
		errorPos.copyFrom(token.pos);
		// next token must be a variable
		position++;
		var = cmdStack.get(position);
		if (var.type != Token.VARIABLE)
			throw new ParseException("Foreach: missing loop variable opening parenthesis");
		// next token must be a separator
		position++;
		token = cmdStack.get(position);
		if (token.type != Token.SEPARATOR)
			throw new ParseException("foreach: missing separator after loop variable");
		// search for closing parenthesis ")"
		position++; // should point at first token after 2nd separator
		lvl = 1;
		for (i = position; i<cmdStack.size()-2; i++) {
			token = cmdStack.get(i);
			if (token.type == Token.PARENTHESIS_OPEN)
				lvl++;
			else if (token.type == Token.PARENTHESIS_CLOSE) {
				if (--lvl==0)
					break;
			}
		}
		if (lvl != 0)
			throw new ParseException("Missing closing parenthesis after keyword 'foreach'");
		// create rpnStack from the list expression
		ALStack<Token> expRPNStack = createRPNStack(cmdStack, position, i-1, pLevel);
		rpnStack.addAll(expRPNStack);
		pLevel--;
		position = i+1; // should point at opening brace
		// check for opening brace
		token = cmdStack.get(position);
		if (token.type != Token.BRACE_OPEN)
			throw new ParseException("Missing opening brace after keyword 'foreach'");
		errorPos.copyFrom(token.pos);
		// search closing brace
		position++;
		lvl = 1;
		for (i = position; i<cmdStack.size()-2; i++) {
			token = cmdStack.get(i);
			if (token.type == Token.BRACE_OPEN)
				lvl++;
			else if (token.type == Token.BRACE_CLOSE) {
				if (--lvl==0)
					break;
			}
		}
		if (lvl != 0)
			throw new ParseException("Missing closing brace after keyword 'foreach'");
		errorPos.copyFrom(token.pos);
		// create rpnStack from the if body inside the braces
		expRPNStack = createRPNStack(cmdStack, position, i-1, pLevel);
		int size = expRPNStack.size();
		position = i; // should point at closing brace
		// shove the loop on the stack
		rpnStack.add(new Token(Token.FOREACH_INIT));
		rpnStack.add(var);
		token = new Token(Token.FOREACH_NEXT);
		token.content = new Integer(size + 3);
		rpnStack.add(token);
		rpnStack.add(new Token (Token.STORE_VSTACK_SIZE));
		rpnStack.addAll(expRPNStack);
		rpnStack.add(new Token (Token.POP_IF_VSTACK_UNEQUAL));
		// unconditional JMP to start of loop
		token = new Token(Token.JMP);
		token.content = new Integer(-size -4);
		rpnStack.add(token);
		return position;
	}


	/**
	 * Create do while loop on the rpn stack
	 * Called from createRPNStack()
	 * "do { <exp1> } while ( <exp2> )"
	 * pushed on stack as
	 * <exp1> <exp2> JMP_TRUE {-size exp<1> - size exp<1> - 1}
	 * @param cmdStack Command Stack in which the conditional is defined
	 * @param rpnStack RPN STack to push for loop to
	 * @param position Current position of inside the command stack (pointing at "for")
	 * @param pLevel Current parenthesis nesting level
	 * @return position of command stack after the evaluation
	 * @throws ParseException
	 */
	private int createDoLoop(ALStack<Token> cmdStack, ALStack<Token> rpnStack, int position, int pLevel) throws ParseException {
		int i;
		int lvl;
		Token token = cmdStack.get(position);
		Token pop1 = new Token(Token.POP_VALUE);
		pop1.content = new Integer(1);
		errorPos.copyFrom(token.pos); // point at "do"
		// next token must be an opening brace
		token = cmdStack.get(++position);
		if (token.type != Token.BRACE_OPEN)
			throw new ParseException("Missing opening brace after keyword 'do'");
		errorPos.copyFrom(token.pos);
		// search closing brace
		position++;
		lvl = 1;
		for (i = position; i<cmdStack.size()-2; i++) {
			token = cmdStack.get(i);
			if (token.type == Token.BRACE_OPEN)
				lvl++;
			else if (token.type == Token.BRACE_CLOSE) {
				if (--lvl==0)
					break;
			}
		}
		if (lvl != 0)
			throw new ParseException("Missing closing brace after keyword 'do'");
		errorPos.copyFrom(token.pos);
		// create rpnStack from the if body inside the braces
		ALStack<Token> exp1RPNStack = createRPNStack(cmdStack, position, i-1, pLevel);
		rpnStack.add(new Token (Token.STORE_VSTACK_SIZE));
		rpnStack.addAll(exp1RPNStack);
		rpnStack.add(new Token (Token.POP_IF_VSTACK_UNEQUAL));
		int size1 = exp1RPNStack.size()+2;
		position = i; // should point at closing brace
		// next token must be keyword "while"
		token = cmdStack.get(++position);
		if (token.type != Token.KEYWORD_WHILE)
			throw new ParseException("Missing keyword 'while'");
		// next token must be an opening parenthesis
		errorPos.copyFrom(token.pos);
		token = cmdStack.get(++position);
		if (token.type != Token.PARENTHESIS_OPEN)
			throw new ParseException("Missing opening parenthesis after keyword 'while'");
		errorPos.copyFrom(token.pos);
		// search for closing parenthesis ")"
		pLevel++;
		position++; // should point at first token inside parenthesises
		lvl = 1;
		for (i = position; i<cmdStack.size()-2; i++) {
			token = cmdStack.get(i);
			if (token.type == Token.PARENTHESIS_OPEN)
				lvl++;
			else if (token.type == Token.PARENTHESIS_CLOSE) {
				if (--lvl==0)
					break;
			}
		}
		if (lvl != 0)
			throw new ParseException("Missing closing parenthesis after keyword 'while'");
		// create rpnStack from the loop condition
		ALStack<Token> exp2RPNStack = createRPNStack(cmdStack, position, i-1, pLevel);
		rpnStack.addAll(exp2RPNStack);
		pLevel--;
		position = i; // points at closing parenthesis
		// shove the conditional onto the RPN stack
		// conditional jump <exp2> JMP_TRUE
		token = new Token(Token.JMP_TRUE);
		token.content = new Integer(-size1 - exp2RPNStack.size() - 1);
		rpnStack.add(token);
		return position;
	}

	/**
	 * Create while loop on the rpn stack
	 * Called from createRPNStack()
	 * "while ( <exp1> ) { <exp2> }"
	 * pushed on stack as
	 * <exp1> JMP_FALSE {size <exp2> + 1} <exp2> JMP {-size <exp2> -2}
	 * @param cmdStack Command Stack in which the conditional is defined
	 * @param rpnStack RPN STack to push for loop to
	 * @param position Current position of inside the command stack (pointing at "for")
	 * @param pLevel Current parenthesis nesting level
	 * @return position of command stack after the evaluation
	 * @throws ParseException
	 */
	private int createWhileLoop(ALStack<Token> cmdStack, ALStack<Token> rpnStack, int position, int pLevel) throws ParseException {
		int i;
		int lvl;
		Token token = cmdStack.get(position);
		Token pop1 = new Token(Token.POP_VALUE);
		pop1.content = new Integer(1);
		errorPos.copyFrom(token.pos); // point at "while"
		// next token must be an opening parenthesis
		token = cmdStack.get(position+1);
		if (token.type != Token.PARENTHESIS_OPEN)
			throw new ParseException("Missing opening parenthesis after keyword 'while'");
		pLevel++;
		errorPos.copyFrom(token.pos);
		// search for closing parenthesis ")"
		position += 2;
		lvl = 1;
		for (i = position; i<cmdStack.size()-2; i++) {
			token = cmdStack.get(i);
			if (token.type == Token.PARENTHESIS_OPEN)
				lvl++;
			else if (token.type == Token.PARENTHESIS_CLOSE) {
				if (--lvl==0)
					break;
			}
		}
		if (lvl != 0)
			throw new ParseException("Missing closing parenthesis after keyword 'while'");
		// create rpnStack from the initialization condition
		ALStack<Token> exp1RPNStack = createRPNStack(cmdStack, position, i-1, pLevel);
		pLevel--;
		position = i+1; // should point at opening brace
		// check for opening brace
		token = cmdStack.get(position);
		if (token.type != Token.BRACE_OPEN)
			throw new ParseException("Missing opening brace after keyword 'while'");
		errorPos.copyFrom(token.pos);
		// search closing brace
		position++;
		lvl = 1;
		for (i = position; i<cmdStack.size()-2; i++) {
			token = cmdStack.get(i);
			if (token.type == Token.BRACE_OPEN)
				lvl++;
			else if (token.type == Token.BRACE_CLOSE) {
				if (--lvl==0)
					break;
			}
		}
		if (lvl != 0)
			throw new ParseException("Missing closing brace after keyword 'while'");
		errorPos.copyFrom(token.pos);
		// create rpnStack from the if body inside the braces
		ALStack<Token> exp2RPNStack = createRPNStack(cmdStack, position, i-1, pLevel);
		position = i; // should point at closing brace
		// shove the conditional onto the RPN stack

		rpnStack.addAll(exp1RPNStack);
		int size2 = exp2RPNStack.size()+2;
		// conditional jump <exp2> JMP_FALSE
		token = new Token(Token.JMP_FALSE);
		token.content = new Integer(size2+ 1); // store offset to first token after body
		rpnStack.add(token);
		rpnStack.add(new Token (Token.STORE_VSTACK_SIZE));
		rpnStack.addAll(exp2RPNStack);
		rpnStack.add(new Token (Token.POP_IF_VSTACK_UNEQUAL));
		// unconditional JMP to start of loop
		token = new Token(Token.JMP);
		token.content = new Integer(-exp1RPNStack.size() - size2 - 2);
		rpnStack.add(token);
		return position;
	}


	/**
	 * Converts the commandStack into an RPN stack
	 * Binary operations are pushed as "right left operator"
	 * Unary operations are pushed as "operand operator"
	 * Functions are pushed as "paramN ... param1 function"
	 * @param  cmdStack commandStack
	 * @param  startPos index to start parsing from
	 * @param  endPos index of last token to parse
	 * @param  pLevel Current parenthesis nesting level
	 * @return pointer to RPN stack
	 * @throws ParseException
	 */

	private ALStack<Token> createRPNStack(ALStack<Token> cmdStack, int startPos, int endPos, int pLevel) throws ParseException {
		Operator op,opr;
		Token token,left,right,nextright;
		ALStack<Token> evStack = new ALStack<Token>();
		ALStack<Token> rpnStack = new ALStack<Token>();
		Token shortcutStart = new Token(Token.SHORTCUT_START);
		right = cmdStack.get(startPos);
		token = new Token(Token.START);
		boolean shortcut = false;
		boolean pushShortcut = false;
		boolean rightSideIndexed = false;
		int pLevelShortcut = 0;
		int precedenceShortcut = 0;
		int lastRPNsize = 0;
		Token pop1 = new Token(Token.POP_VALUE);
		pop1.content = new Integer(1);

		parseloop:
			for (int i=startPos; i<=endPos; i++) {
				left = token;
				token = right;
				right = cmdStack.get(i+1);
				errorPos.copyFrom(token.pos);

				// get token type and decide what to do next
				switch (token.type) {
					case Token.KEYWORD_FUNCTION:
						// examine if syntax is correct, remember variables, then createRPNs
						i = createUserFunction(cmdStack,i,pLevel);
						token = cmdStack.get(i);
						right = cmdStack.get(i+1);
						break;
					case Token.KEYWORD_FOR:
						i = createForLoop(cmdStack, rpnStack, i,pLevel);
						// if the next token is ";" it must be hindered to pop the value stack
						lastRPNsize = rpnStack.size();
						token = cmdStack.get(i);
						right = cmdStack.get(i+1);
						break;
					case Token.KEYWORD_DO:
						i = createDoLoop(cmdStack, rpnStack, i,pLevel);
						// if the next token is ";" it must be hindered to pop the value stack
						lastRPNsize = rpnStack.size();
						token = cmdStack.get(i);
						right = cmdStack.get(i+1);
						break;
					case Token.KEYWORD_WHILE:
						i = createWhileLoop(cmdStack, rpnStack, i,pLevel);
						// if the next token is ";" it must be hindered to pop the value stack
						lastRPNsize = rpnStack.size();
						token = cmdStack.get(i);
						right = cmdStack.get(i+1);
						break;
					case Token.KEYWORD_FOREACH:
						i = createForeachLoop(cmdStack, rpnStack, i,pLevel);
						// if the next token is ";" it must be hindered to pop the value stack
						lastRPNsize = rpnStack.size();
						token = cmdStack.get(i);
						right = cmdStack.get(i+1);
						break;
					case Token.KEYWORD_IF:
						i = createConditional(cmdStack, rpnStack, i,pLevel);
						// problem: after the if or else body, the next command usually doesn't consume the value on the
						// value stack (only operators do). Then it has to be consumed to keep the value stack clean.
						// Furthermore there is at least one case (nested if clauses which shall return a value)
						// in which the value stack mustn't be popped.
						// so in all cases apart from next token being ";" or "}" (to allow nested if clauses with return
						// value) insert a virtual ";"
						token = cmdStack.get(i);
						right = cmdStack.get(i+1);
						if (right.type == Token.SEPARATOR || right.type == Token.BRACE_CLOSE || right.type == Token.PARENTHESIS_CLOSE
								||  right.type == Token.OPERATOR || right.type == Token.END || i>=endPos)
							break;
						// insert "virtual" separator to consume value stack
						if (!evStack.isEmpty())
							popStack(evStack,rpnStack, pLevel, 255, Operator.LEFT);
						if (shortcut)
							shortcut = false;
						rpnStack.add(pop1);
						lastRPNsize = rpnStack.size();
						break;
					case Token.KEYWORD_ELSE:
						throw new ParseException("Unexpected else");
					case Token.TERNARY_QMARK:
						if (left.type != Token.PARENTHESIS_CLOSE)
							throw new ParseException("Unexpected ternary operator '?'");
						i = createTernary(cmdStack, rpnStack, i,pLevel);
						token = cmdStack.get(i);
						right = cmdStack.get(i+1);
						break;
					case Token.BRACKET_OPEN:
						if (left.type == Token.VARIABLE) {
							i = createIndexedAccess(cmdStack, rpnStack, i,pLevel);
							token = cmdStack.get(i);   // points at "]"
							right = cmdStack.get(i+1); // move on after "]"
							break;
						}
						throw new ParseException("Unexpected '['");
					case Token.VARIABLE:
					case Token.VALUE:
						rpnStack.push(new Token(token));
						break;
					case Token.SEPARATOR:
					case Token.END:
						if (!evStack.isEmpty())
							popStack(evStack,rpnStack, pLevel, 255, Operator.LEFT);
						if (shortcut)
							shortcut = false;
						if (token.type == Token.END)
							break parseloop;
						if (rpnStack.size() > lastRPNsize) {
							rpnStack.add(pop1);
							lastRPNsize = rpnStack.size();
						}
						break;
					case Token.BRACE_OPEN:
						// create list
						i = createList(cmdStack, rpnStack, i, pLevel);
						token = cmdStack.get(i);
						right = cmdStack.get(i+1);
						break;
					case Token.BRACE_CLOSE:
						throw new ParseException("Unexpected closing brace!");
					case Token.PARENTHESIS_OPEN:
						pLevel++;
						break;
					case Token.PARENTHESIS_CLOSE:
						if (!evStack.isEmpty())
							popStack(evStack, rpnStack, pLevel, 255, Operator.LEFT);
						pLevel--;
						if (left.type == Token.PARENTHESIS_OPEN)
							throw new ParseException("Empty parenthesis");
						if (shortcut && pLevel < pLevelShortcut) // end of parenthesis nesting -> end shortcut
							shortcut = false;
						continue;               // don't change left
					case Token.FUNCTION_SEPARATOR:
						throw new ParseException("Unexpected ','");
					case Token.FUNCTION:
					case Token.USER_FUNCTION:
						i = createFunctionCall(cmdStack,rpnStack,i,pLevel);
						token = cmdStack.get(i);
						right = cmdStack.get(i+1);
						break;
					case Token.OPERATOR:
						nextright = cmdStack.get(i+2);  // to allow peeking on the token after next token
						rightSideIndexed = false;
						// special case: allow peeking at token after indexed access
						int j = i+3;
						while (nextright.type == Token.BRACKET_OPEN) {
							int bkLevel = 1;
							searchBracket:
								for (; j<cmdStack.size()-2; j++) {
									nextright = cmdStack.get(j);
									switch (nextright.type) {
										case Token.BRACKET_OPEN:
											bkLevel++;
											break;
										case Token.BRACKET_CLOSE:
											bkLevel--;
											if (bkLevel == 0)
												break searchBracket;
											break;
									}
								}
							nextright = cmdStack.get(++j); // now j points a character after "]"
							j++; // now j points at character after nextright
							rightSideIndexed = true;
						}
						// get operator
						op = (Operator)token.content;
						if (!evStack.isEmpty())
							popStack(evStack, rpnStack, pLevel, op.precedence, op.association);
						if (shortcut && pLevel == pLevelShortcut && op.precedence >= precedenceShortcut)
							shortcut = false;
						switch (op.type) {
							case Operator.BINARY_LEFT:
							case Operator.BINARY_RIGHT:
								switch (left.type) {
									case Token.START:
									case Token.SEPARATOR:
									case Token.FUNCTION_SEPARATOR:
									case Token.PARENTHESIS_OPEN:
									case Token.BRACE_OPEN:
										throw new ParseException("Unexpected Operator");
								}
								// test for possibility of shortcut
								if (op.canShortCut()) {
									shortcut = true;              // ignore right side until end condition
									pushShortcut = true;
									precedenceShortcut = op.precedence; // needed for end condition
									pLevelShortcut = pLevel;              // needed for end condition
									//fLevelShortcut = fLevel;              // needed for end condition
								}
								// check right side
								switch (right.type) {
									case Token.VALUE:
									case Token.VARIABLE:
										// handle priority and association
										if (nextright.type == Token.OPERATOR) {
											opr = (Operator)nextright.content;
											if (opr.precedence < op.precedence ||
													(opr.precedence == op.precedence && op.association == Operator.RIGHT)) {
												if (pushShortcut) {
													pushShortcut = false;
													rpnStack.push(shortcutStart);
												}
												evStack.push(new Token(token));
												break;
											}
										}
										if (pushShortcut) {
											pushShortcut = false;
											rpnStack.push(shortcutStart);
										}
										rpnStack.push(new Token(right));
										if (rightSideIndexed && right.type == Token.VARIABLE) {
											i = createIndexedAccess(cmdStack, rpnStack, i+2,pLevel);
											// leave token as it was
											nextright = cmdStack.get(i+1);
											--i; // compensate for following i++
										}
										rpnStack.push(new Token(token));
										right = nextright;  // next token will be nextright
										i++;                // skip right side, since already used
										continue;           // don't overwrite left
									case Token.OPERATOR:
										// operator on the right side of a binary operator can only be unary right
										// in this case, the unary operator has to be evaluated first
										opr = (Operator)right.content;
										if (opr.type != Operator.UNARY_RIGHT || opr.precedence > op.precedence)
											throw new ParseException("Invalid unary operator at right side of binary operator "+token.name);
										// else fallthrough
									case Token.PARENTHESIS_OPEN:
									case Token.BRACE_OPEN:
									case Token.KEYWORD_IF:
									case Token.KEYWORD_FOR:
									case Token.KEYWORD_FOREACH:
									case Token.KEYWORD_DO:
									case Token.KEYWORD_WHILE:
									case Token.FUNCTION:
									case Token.USER_FUNCTION:
										// push current operand and operator on eval stack
										if (pushShortcut) {
											pushShortcut = false;
											rpnStack.push(shortcutStart);
										}
										evStack.push(new Token(token));
										break;
									default:
										throw new ParseException("Unexpected argument for right side of binary operator: "+token.name);
								}
								break;
							case Operator.UNARY_LEFT:
								// there should be no possibility for priority problems, also the left side should be resolved
								rpnStack.push(new Token(token));
								break;
								//break; unreachable
							case Operator.UNARY_RIGHT:
								switch (right.type) {
									case Token.VALUE:
									case Token.VARIABLE:
										rpnStack.push(new Token(right));
										if (rightSideIndexed && right.type == Token.VARIABLE) {
											i = createIndexedAccess(cmdStack, rpnStack, i+2,pLevel);
											// leave token as it was
											nextright = cmdStack.get(i+1);
											--i; // compensate for following i++
										}
										rpnStack.push(new Token(token));
										right = nextright;  // next token will be nextright
										i++;                // skip right side, since already used
										continue;           // don't change left
									case Token.OPERATOR:
										opr = (Operator)right.content;
										if (opr.type != Operator.UNARY_RIGHT) // only unary right could be to the right
											throw new ParseException("Unexpected argument for right side of unary right operator: "+token.name);
										// fall through
									case Token.FUNCTION:
									case Token.USER_FUNCTION:
									case Token.PARENTHESIS_OPEN:
										// push current operand on stack
										evStack.push(new Token(token));
										break;
									default:
										throw new ParseException("Unexpected argument for right side of unary right operator: "+token.name);
								}
								break;
						}

						break;
				}
			}
		if (!evStack.isEmpty())
			popStack(evStack,rpnStack, pLevel, 255, Operator.LEFT);
		return rpnStack;
	}


	/**
	 * Evaluates RPN stack
	 * @param rpnStack RPN stack to evaluate
	 * @return Value of the expression on the RPN tack
	 * @throws ParseException
	 */
	Value evalRPNStack(ALStack<Token> rpnStack) throws ParseException {
		Token operand[] = new Token[2];
		Value result = null;
		Operator op;
		Function fon;
		UserFunction uFon;
		UserFunction uFons[];
		Token token=null;
		ALStack<Token> valueStack = new ALStack<Token>();
		ALStack<Iterator<Value>> iteratorStack = new ALStack<Iterator<Value>>();
		int fparam;
		int index;
		Token param[];
		ALStack<Integer> vStackSizes = new ALStack<Integer>();
		int vStackSize;

		Token dummy = new Token(Token.INVALID);

		if (rpnStack.isEmpty())
			return new Value();
		//throw new ParseException("Empty or invalid expression");

		// debug
		//System.out.println("");
		//System.out.println("RPNStack:");
		//printStack(rpnStack);
		//System.out.println("");

		// now eval the stack
		try {
			for (index=0; index < rpnStack.size(); index++) {
				if (breakExec) {
					breakExec = false;
					throw new ParseException("User interrupt.");
				}
				token = rpnStack.get(index);
				errorPos.copyFrom(token.pos);
				switch (token.type) {
					case Token.LIST: {
						// create list token on value stack { a,b,c }
						fparam = ((Integer)token.content).intValue();
						if (valueStack.size() < fparam)
							throw new ParseException("Invalid list definition");
						// resort valuestack into list
						ValueList jl = new ValueList(fparam);
						for (int i=fparam-1; i>=0; i--) {
							token = valueStack.pop();
							switch (token.type) {
								case Token.VALUE:
									jl.add(0,(Value)token.content);
									break;
								case Token.VARIABLE:
									jl.add(0,variables.getValue(token.name));
									break;
								default:
									throw new ParseException("Invalid list element.");
							}
						}
						// push list back on stack
						token = new Token(Token.VALUE);
						token.content = new Value(jl); // shallow copy as this is a new list
						valueStack.push(token);
						break; }
					case Token.INDEXED_ACCESS: {
						// evaluate indexed access var[index]
						if (valueStack.size() < 2)
							throw new ParseException("Invalid value for binary operator");
						operand[1] = valueStack.pop();
						operand[0] = valueStack.pop();
						if (operand[0].type != Token.VARIABLE)
							throw new ParseException("Not a variable.");
						switch (operand[1].type) {
							case Token.VALUE:
								result = (Value)operand[1].content;
								break;
							case Token.VARIABLE:
								result = variables.getValue(operand[1].name);
								break;
							default:
								throw new ParseException("Invalid Index.");
						}
						if (result.type != Value.NUMBER)
							throw new ParseException("Invalid type for index");
						StringBuffer sb = new StringBuffer(operand[0].name);
						JNumber num = (JNumber)result.value;
						if (!num.isInteger() || num.signum() < 0)
							throw new ParseException("Only integers >=0 allowed for index");
						sb.append("@");
						sb.append(num.toString());
						// create new token
						token = new Token(Token.VARIABLE);
						token.name = sb.toString();
						valueStack.push(token);
						break; }
					case Token.STORE_VSTACK_SIZE: {
						vStackSizes.push(new Integer(valueStack.size()));
						break; }
					case Token.POP_IF_VSTACK_UNEQUAL: {
						vStackSize = vStackSizes.pop().intValue();
						if (vStackSize < valueStack.size())
							valueStack.pop();
						break; }
					case Token.PUSH_DUMMY_IF_VSTACK_EQUAL: {
						vStackSize = vStackSizes.pop().intValue();
						if (vStackSize == valueStack.size())
							valueStack.push(dummy);
						break; }
					case Token.PUSH_DUMMY: {
						valueStack.push(dummy);
						break; }
					case Token.POP_VALUE: {
						int pop = ((Integer)token.content).intValue();
						while(pop-- > 0)
							valueStack.pop();
						break; }
					case Token.JMP: {
						// unconditional jump: just increase index
						index += ((Integer)token.content).intValue();
					break; }
					case Token.JMP_TRUE:
					case Token.JMP_FALSE: {
						// get condition from value stack
						if (valueStack.size() < 1)
							throw new ParseException("Invalid condition");
						operand[0] = valueStack.pop();
						switch (operand[0].type ) {
							case Token.VALUE:
								result = ((Value)(operand[0]).content);
								if (result.type != Value.BOOL)
									throw new ParseException("Boolean value expected");
								break;
							case Token.VARIABLE:
								try {
									result = variables.getValue(operand[0].name);
								} catch (ParseException ex) {
									throw new ParseException(ex.getMessage());
								}
						}
						if (((Boolean)result.value).booleanValue() ^ token.type == Token.JMP_FALSE)
							index += ((Integer)token.content).intValue();
						break; }
					case Token.FOREACH_INIT: {
						if (valueStack.size() < 1)
							throw new ParseException("No list on value stack");
						operand[0] = valueStack.pop();
						switch (operand[0].type) {
							case Token.VARIABLE:
								try {
									result = variables.getValue(operand[0].name);
								} catch (ParseException ex) {
									throw new ParseException(ex.getMessage());
								}
								if (result.type != Value.LIST)
									throw new ParseException("List expected.");
								break;
							case Token.VALUE:
								result = ((Value)(operand[0]).content);
								if (result.type != Value.LIST)
									throw new ParseException("List expected.");
								break;
						}
						ValueList jl = (ValueList)result.value;
						// get iterator and push it on stack
						Iterator<Value> it = jl.iterator();
						iteratorStack.push(it);
						break; }
					case Token.FOREACH_NEXT: {
						operand[0] = valueStack.peek(); // don't pop as we will need it again
						if (operand[0].type != Token.VARIABLE)
							throw new ParseException("Variable expected.");
						// get iterator and push it on stack
						Iterator<Value> it = iteratorStack.peek();
						if (!it.hasNext()) {
							iteratorStack.pop(); // last loop -> pop iterator
							valueStack.pop();    // last loop -> pop loop variable
							index += ((Integer)token.content).intValue(); // jump at the end of loop
							result = new Value(); // undefined
						} else {
							result = new Value(it.next()); // first element
							variables.putValue(operand[0].name,result);
						}
						//valueStack.push(new Token(Token.VALUE,result));
						break; }
					case Token.VARIABLE:
					case Token.VALUE: {
						valueStack.push(new Token(token));
						break; }
					case Token.OPERATOR: {
						op = (Operator)token.content;
						switch (op.type) {
							case Operator.BINARY_LEFT:
							case Operator.BINARY_RIGHT:
								if (valueStack.size() < 2)
									throw new ParseException("Invalid value for binary operator");
								operand[1] = valueStack.pop();
								operand[0] = valueStack.pop();
								if (operand[0].type == Token.INVALID || operand[1].type == Token.INVALID)
									throw new ParseException("Invalid value for binary operator");
								result = op.result(operand);
								valueStack.push(new Token(Token.VALUE,result));
								break;
							case Operator.UNARY_LEFT:
							case Operator.UNARY_RIGHT:
								if (valueStack.size() < 1)
									throw new ParseException("Invalid value for unary operator");
								operand[0] = valueStack.pop();
								operand[1] = null;
								if (operand[0].type == Token.INVALID)
									throw new ParseException("Invalid value for binary operator");
								result = op.result(operand);
								valueStack.push(new Token(Token.VALUE,result));
								break;
						}
						break; }
					case Token.FUNCTION: {
						fon = (Function)token.content;
						fparam = fon.paramNum;
						param = new Token[fparam];
						if (valueStack.size() < fparam)
							throw new ParseException("Invalid parameter");
						for (int i=fparam-1; i>=0; i--)
							param[i] = valueStack.pop();
						result = fon.result(param);
						// hack: keep NUMSTRING for hex/bin
						Token tokn = new Token(Token.VALUE);
						// don't use copy constructor to keep numstring
						Value v = new Value();
						v.copyFrom(result);
						tokn.content = v;
						valueStack.push(tokn);
						break; }
					case Token.USER_FUNCTION: {
						fparam = ((Integer)token.content).intValue(); // fetch number of parameters
						uFons = userFunctions.get(token.name);
						// search fitting function
						uFon = null;
						if (uFons != null) {
							for (int j=0; j<uFons.length; j++)
								if (uFons[j].param.length == fparam) {
									uFon = uFons[j];
									break;
								}
						} else throw new ParseException("Unknown function "+token.name);
						if (uFon == null)
							throw new ParseException("Wrong number of parameters for function "+token.name);

						if (valueStack.size() < fparam)
							throw new ParseException("Invalid parameter");
						Value[] paramu = new Value[fparam];
						for (int i=fparam-1; i>=0; i--)
							paramu[i] = getValue(valueStack.pop(), Token.VALUE | Token.VARIABLE, i);
						userFunctions.enterNameSpace(token.name);
						result = uFon.result(paramu, this);
						userFunctions.leaveNameSpace();
						// hack: keep NUMSTRING for hex/bin
						Token toknu = new Token(Token.VALUE);
						// don't use copy constructor to keep numstring
						Value vu = new Value();
						vu.copyFrom(result);
						toknu.content = vu;
						valueStack.push(toknu);
						break; }
					case Token.SHORTCUT_START: {
						// search for fitting shortcut operator +2 cause at least one "left" token on stack
						int scDepth=0;
						Token tok;
						Token left = valueStack.peek();
						for(int i=index+2; i<rpnStack.size(); i++) {
							tok = rpnStack.get(i);
							if (tok.type == Token.SHORTCUT_START)
								scDepth++;
							if (tok.type == Token.OPERATOR) {
								op = (Operator)tok.content;
								if (op.canShortCut()) {
									if (scDepth!=0) {
										// not the right SC operator
										scDepth--;
										continue;
									}
									if (op.isShortCut(left)) {
										result = op.resultShortCut();
										valueStack.pop(); // erase left
										valueStack.push(new Token(Token.VALUE,result));
										index = i; // continue with token after op
									}
									break; // from loop
								}
							}
						}
						break; }
					default:
						errorPos.copyFrom(token.pos);
						throw new ParseException("Invalid element on RPN stack.");
				}
			}
		} catch (EmptyStackException ex) {
			throw new ParseException("Evaluator Stack underflow");
		} catch (NullPointerException ex) {
			throw new ParseException("Evaluator nullpointer access");
		}
		// debug
		//System.out.println("");
		//System.out.println("ValueStack:");
		//printStack(valueStack);

		if (valueStack.size() == 0)
			return new Value(); // undefined value !
		if (valueStack.size() > 1)
			throw new ParseException("Valuestack not empty!");
		token = valueStack.pop();
		if (token.type == Token.INVALID)
			return new Value(); // undefined value !
		if (token.type == Token.VALUE)
			return (Value)token.content;
		// variable
		return variables.getValue(token.name); // might result in ParseExcpetion if not found
	}

	void printStack(ALStack<Token> stack) {
		Token token;
		for (int i=0; i<stack.size(); i++) {
			token = stack.get(i);
			// output for debugging
			switch (token.type) {
				case Token.VARIABLE:
					System.out.print("{"+token.name+"}"); break;
				case Token.VALUE:
					System.out.print("{"+token.toString()+"}"); break;
				case Token.OPERATOR:
					System.out.print("{"+token.name);
					switch (((Operator)token.content).type ) {
						case Operator.BINARY_LEFT: System.out.print("bl}"); break;
						case Operator.BINARY_RIGHT: System.out.print("br}"); break;
						case Operator.UNARY_LEFT: System.out.print("ul}"); break;
						case Operator.UNARY_RIGHT: System.out.print("ur}"); break;
					}
					break;
				default:
					System.out.print(token.toString());
			}
		}
	}
}


