package Parser;

import java.math.BigDecimal;
import java.math.BigInteger;

/*
 * Copyright 2009 Volker Oth / Arndt Bruenner
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @author Volker Oth / Arndt Bruenner
 *
 * Math utility class for BigDecimal numbers.<br>
 * Based on the work of Arndt Bruenner.<br>
 * @see <a href="http://www.arndt-bruenner.de/mathe/mathekurse.htm">Arndt Bruenner's site</a>
 *
 */
public final class BigDecimalMath {

	/**
	 * Returns value of PI with a given scale.
	 * @param scale Scale for PI
	 * @return PI
	 */
	public static BigDecimal PI(int scale) {
		if(scale <= 0)
			return PI_;
		if(scale <= PI_.scale()) {
			BigDecimal bigdecimal = PI_;
			return bigdecimal.setScale(scale, roundingMode);
		}
		int j = (int)Math.floor(scale / 256 + 1) * 256;
		BigDecimal bigdecimal1 = atanInv(new BigDecimal("5"), j + 10);
		if(interrupt)
			return PI_;
		BigDecimal bigdecimal2 = atanInv(new BigDecimal("239.0"), j + 10);
		if(interrupt)
			return PI_;
		PI_ = bigdecimal1.multiply(FOUR).subtract(bigdecimal2).multiply(FOUR);
		return PI_.setScale(scale, roundingMode);

	}

	/**
	 * Returns value of Euler number E with a given scale
	 * @param scale Scale for E
	 * @return Euler number E
	 */
	public static BigDecimal E(int scale) {
		if(scale <= 0)
			return E_;
		if(scale <= E_.scale())
			return E_.setScale(scale, roundingMode);
		BigDecimal bigdecimal = exp(ONE, (int)Math.floor(scale / 256 + 1) * 256);
		return (E_ = bigdecimal).setScale(scale, roundingMode);
	}

	/**
	 * inverse arcus tangens
	 * @param bigdecimal value to calculate atanInv from
	 * @param scale Scale for result
	 * @return inverse arcus tangens
	 */
	static BigDecimal atanInv(BigDecimal bigdecimal, int scale) {
		BigDecimal bigdecimal2 = bigdecimal.multiply(bigdecimal);
		BigDecimal bigdecimal3 = ONE.divide(bigdecimal, scale, roundingMode);
		BigDecimal bigdecimal4 = bigdecimal3;
		int j = 1;
		BigDecimal bigdecimal1;
		do {
			bigdecimal3 = bigdecimal3.divide(bigdecimal2, scale, roundingMode);
			int k = 2 * j + 1;
			bigdecimal1 = bigdecimal3.divide(BigDecimal.valueOf(k), scale, roundingMode);
			if(j % 2 != 0)
				bigdecimal4 = bigdecimal4.subtract(bigdecimal1);
			else
				bigdecimal4 = bigdecimal4.add(bigdecimal1);
			j++;
		} while(!interrupt && bigdecimal1.compareTo(ZERO) != 0);
		return bigdecimal4;
	}

	/**
	 * Arcustangens
	 * @param bigdecimal Value to calculate atan from
	 * @param scale Scale for result
	 * @return Arcustangens
	 */
	public static BigDecimal atan(BigDecimal bigdecimal, int scale) {
		if(bigdecimal.compareTo(ONE) == 0)
			return PI(bigdecimal.scale()).divide(FOUR, scale, roundingMode);
		if(bigdecimal.compareTo(ONE.negate()) == 0)
			return PI(bigdecimal.scale()).divide(FOUR, scale, roundingMode).negate();
		scale += 10;
		BigDecimal bigdecimal3 = bigdecimal;
		BigDecimal bigdecimal5 = ONE.divide(bigdecimal, scale, roundingMode);
		BigDecimal bigdecimal6 = bigdecimal5.multiply(bigdecimal5).setScale(scale, roundingMode);
		BigDecimal bigdecimal7 = bigdecimal3;
		int j = 1;
		BigDecimal bigdecimal1;
		if(bigdecimal.abs().compareTo(ONE) == -1) {
			do {
				bigdecimal3 = bigdecimal3.divide(bigdecimal6, scale, roundingMode);
				int l = 2 * j + 1;
				bigdecimal1 = bigdecimal3.divide(BigDecimal.valueOf(l), scale, roundingMode);
				if(j % 2 != 0)
					bigdecimal7 = bigdecimal7.subtract(bigdecimal1);
				else
					bigdecimal7 = bigdecimal7.add(bigdecimal1);
				j++;
			} while(!interrupt && bigdecimal1.compareTo(ZERO) != 0);
		} else {
			bigdecimal7 = PI(scale).divide(TWO, scale, roundingMode).subtract(bigdecimal5);
			int k = 1;
			BigDecimal bigdecimal4 = bigdecimal5;
			BigDecimal bigdecimal2;
			do {
				bigdecimal4 = bigdecimal4.multiply(bigdecimal6).setScale(scale, roundingMode);
				int i1 = 2 * k + 1;
				bigdecimal2 = bigdecimal4.divide(BigDecimal.valueOf(i1), scale, roundingMode);
				if(k % 2 == 0)
					bigdecimal7 = bigdecimal7.subtract(bigdecimal2);
				else
					bigdecimal7 = bigdecimal7.add(bigdecimal2);
				k++;
			} while(!interrupt && bigdecimal2.compareTo(ZERO) != 0);
		}
		return bigdecimal7.setScale(scale - 10, roundingMode);
	}

	/**
	 * Arcussinus
	 * @param bigdecimal Value to calculate asin from
	 * @param scale Scale for result
	 * @return Arcussinus
	 */
	public static BigDecimal asin(BigDecimal bigdecimal, int scale) {
		if(bigdecimal.abs().compareTo(ONE) == 1)
			return ONE.divide(ZERO, scale, roundingMode);
		if(bigdecimal.compareTo(ONE) == 0)
			return PI(scale).divide(TWO, scale, roundingMode);
		if(bigdecimal.negate().compareTo(ONE) == 0)
			return PI(scale).divide(TWO, scale, roundingMode).add(PI(scale));
		BigDecimal bigdecimal1 = new BigDecimal("0.86602540378443864676");
		if(bigdecimal.abs().compareTo(bigdecimal1) == 1) {
			BigDecimal bigdecimal2 = sqrt(ONE.subtract(bigdecimal.multiply(bigdecimal).setScale(scale + 10, roundingMode)), scale + 5).multiply(TWO).multiply(bigdecimal).setScale(scale + 5, roundingMode);
			bigdecimal2 = asin(bigdecimal2, scale);
			if(bigdecimal.signum() == -1)
				return PI(scale).add(bigdecimal2).negate().divide(TWO, scale, roundingMode);
			return PI(scale).subtract(bigdecimal2).divide(TWO, scale, roundingMode);
		}
		BigDecimal bigdecimal3 = bigdecimal;
		//BigDecimal bigdecimal4 = bigdecimal;
		BigDecimal bigdecimal6 = ONE;
		BigDecimal bigdecimal7 = bigdecimal;
		BigDecimal bigdecimal8 = bigdecimal.multiply(bigdecimal).setScale(scale + 10, roundingMode);
		long l = 0L;
		do {
			l += 2L;
			bigdecimal7 = bigdecimal7.multiply(BigDecimal.valueOf(l - 1L)).multiply(bigdecimal8).setScale(scale + 10, roundingMode);
			bigdecimal6 = bigdecimal6.multiply(BigDecimal.valueOf(l));
			BigDecimal bigdecimal5 = bigdecimal7.divide(bigdecimal6.multiply(BigDecimal.valueOf(l + 1L)), scale + 10, roundingMode);
			if(bigdecimal5.compareTo(ZERO) == 0)
				break;
			bigdecimal3 = bigdecimal3.add(bigdecimal5);
		} while(!interrupt);
		return bigdecimal3.setScale(scale, roundingMode);
	}

	/**
	 * Arcuscosinus
	 * @param bigdecimal Value to calculate acos from
	 * @param scale Scale for result
	 * @return Arcuscosinus
	 */
	public static BigDecimal acos(BigDecimal bigdecimal, int scale) {
		return PI(scale + 3).divide(TWO, scale, roundingMode).subtract(asin(bigdecimal, scale));
	}

	/**
	 * Square root
	 * @param bigdecimal Value to calculate sqrt from
	 * @param scale Scale for result
	 * @return Square root
	 */
	public static BigDecimal sqrt(BigDecimal bigdecimal, int scale) {
		if(bigdecimal.compareTo(ZERO) == -1)
			return ONE.divide(ZERO, scale, roundingMode);
		if(bigdecimal.compareTo(ZERO) == 0)
			return ZERO.setScale(scale, roundingMode);
		BigDecimal bigdecimal1;
		try {
			bigdecimal1 = new BigDecimal(Math.sqrt(bigdecimal.doubleValue()));
		}
		catch(Exception exception) {
			bigdecimal1 = bigdecimal.divide(TWO, scale, roundingMode);
		}
		if(bigdecimal1.compareTo(ZERO) == 0)
			bigdecimal1 = ONE;
		int j = scale + 2;
		//BigDecimal bigdecimal2 = new BigDecimal(-1D);
		do {
			BigDecimal bigdecimal3 = bigdecimal1.add(bigdecimal.divide(bigdecimal1, j + 5, roundingMode)).divide(TWO, j, roundingMode);
			if(bigdecimal1.compareTo(bigdecimal3) == 0)
				break;
			bigdecimal1 = bigdecimal3;
		} while(!interrupt);
		return bigdecimal1.setScale(scale, roundingMode);
	}

	/**
	 * Root
	 * @param bigdecimal Value to calculate root from
	 * @param bigdecimal1 Nth root
	 * @param scale Scale for result
	 * @return Square root
	 */
	static BigDecimal root(BigDecimal bigdecimal, BigDecimal bigdecimal1, int scale) {
		if(bigdecimal.compareTo(ZERO) == 0)
			return ZERO.setScale(scale, roundingMode);
		if(bigdecimal.compareTo(ZERO) == -1 && bigdecimal1.compareTo(bigdecimal1.setScale(0, roundingMode)) != 0)
			return ONE.divide(ZERO, scale, roundingMode);
		BigDecimal bigdecimal2;
		try {
			bigdecimal2 = new BigDecimal(Math.pow(bigdecimal.doubleValue(), 1.0D / bigdecimal1.doubleValue()));
		}
		catch(Exception exception) {
			bigdecimal2 = bigdecimal.divide(bigdecimal1, scale, roundingMode);
		}
		if(bigdecimal2.compareTo(ZERO) == 0)
			bigdecimal2 = ONE;
		int j = scale + 2;
		//BigDecimal bigdecimal3 = new BigDecimal(-1D);
		BigDecimal bigdecimal5 = bigdecimal1.subtract(ONE);
		do {
			BigDecimal bigdecimal4 = bigdecimal2.multiply(bigdecimal5).add(bigdecimal.divide(pow(bigdecimal2, bigdecimal5, j), j + 5, roundingMode)).divide(bigdecimal1, j, roundingMode);
			if(bigdecimal2.compareTo(bigdecimal4) == 0)
				break;
			bigdecimal2 = bigdecimal4;
		} while(!interrupt);
		return bigdecimal2.setScale(scale, roundingMode);
	}

	/**
	 * Normalize trigonometric functions to values <= 2*PI
	 * @param bigdecimal Value to normalize
	 * @param scale Scale for result
	 * @return Normalized value
	 */
	static BigDecimal trigNorm(BigDecimal bigdecimal, int scale) {
		BigDecimal bigdecimal1 = bigdecimal;
		BigDecimal bigdecimal2 = PI(scale * 2);
		BigDecimal bigdecimal3 = bigdecimal2.add(bigdecimal2);
		BigDecimal bigdecimal4 = bigdecimal1.divide(bigdecimal3, 0, 1);
		for(bigdecimal1 = bigdecimal1.subtract(bigdecimal3.multiply(bigdecimal4)); bigdecimal1.compareTo(ZERO) == -1; bigdecimal1 = bigdecimal1.add(bigdecimal3));
		for(; bigdecimal1.compareTo(bigdecimal3) == 1; bigdecimal1 = bigdecimal1.subtract(bigdecimal3));
		return bigdecimal1;
	}

	/**
	 * Sinus
	 * @param bigdecimal Value to calculate sin from
	 * @param scale Scale for result
	 * @return Sinus
	 */
	public static BigDecimal sin(BigDecimal bigdecimal, int scale) {
		BigDecimal bigdecimal1 = trigNorm(bigdecimal,scale);
		int j = scale + 20;
		BigDecimal bigdecimal2 = PI_.divide(TWO, j, roundingMode);
		int k = 0;
		if(bigdecimal1.compareTo(PI_) == 1) {
			bigdecimal1 = bigdecimal1.subtract(PI_);
			k = 2;
		}
		if(bigdecimal1.compareTo(bigdecimal2) == 1) {
			bigdecimal1 = PI_.subtract(bigdecimal1);
			k++;
		}
		BigDecimal bigdecimal3 = new BigDecimal("1");
		BigDecimal bigdecimal4 = bigdecimal1;
		BigDecimal bigdecimal5 = bigdecimal1;
		BigDecimal bigdecimal6 = ZERO.setScale(j, roundingMode);
		BigDecimal bigdecimal7 = ONE;
		BigDecimal bigdecimal8 = bigdecimal1.multiply(bigdecimal1);
		//BigDecimal bigdecimal9 = THREE;
		bigdecimal5 = bigdecimal5.setScale(j, roundingMode);
		boolean flag = true;
		do {
			bigdecimal4 = bigdecimal4.multiply(bigdecimal8).setScale(j, roundingMode);
			bigdecimal7 = bigdecimal7.add(ONE);
			bigdecimal3 = bigdecimal3.multiply(bigdecimal7);
			bigdecimal7 = bigdecimal7.add(ONE);
			bigdecimal3 = bigdecimal3.multiply(bigdecimal7);
			BigDecimal bigdecimal10 = bigdecimal4.divide(bigdecimal3, scale, roundingMode);
			if(bigdecimal10.setScale(j, roundingMode).compareTo(bigdecimal6) == 0)
				break;
			flag = !flag;
			if(flag)
				bigdecimal5 = bigdecimal5.add(bigdecimal10);
			else
				bigdecimal5 = bigdecimal5.subtract(bigdecimal10);
		} while(!interrupt);
		if(k > 1)
			bigdecimal5 = bigdecimal5.negate();
		return bigdecimal5.setScale(scale, roundingMode);
	}

	/**
	 * Cosinus
	 * @param bigdecimal Value to calculate cos from
	 * @param scale Scale for result
	 * @return Cosinus
	 */
	public static BigDecimal cos(BigDecimal bigdecimal, int scale) {
		BigDecimal bigdecimal1 = PI(scale + 2).divide(TWO, scale + 2, roundingMode);
		return sin(bigdecimal1.subtract(bigdecimal), scale);
	}

	/**
	 * Tangens
	 * @param bigdecimal Value to calculate tan from
	 * @param scale Scale for result
	 * @return Cosinus
	 */
	public static BigDecimal tan(BigDecimal bigdecimal, int scale) {
		return sin(bigdecimal, scale).divide(cos(bigdecimal, scale), scale, roundingMode);
	}

	/*
	public static BigDecimal tan_(BigDecimal bigdecimal) {
		BigDecimal bigdecimal1;
		for(bigdecimal1 = PI_.divide(TWO, bigdecimal.scale(), roundingMode); bigdecimal.compareTo(bigdecimal1.negate()) == -1; bigdecimal = bigdecimal.add(PI_));
		for(; bigdecimal.compareTo(bigdecimal1) == 1; bigdecimal = bigdecimal.subtract(PI_));
		BigDecimal bigdecimal2 = ONE;
		BigDecimal bigdecimal3 = ONE;
		BigDecimal bigdecimal4 = bigdecimal;
		BigDecimal bigdecimal5 = THREE;
		BigDecimal bigdecimal7 = bigdecimal.multiply(bigdecimal);
		BigDecimal bigdecimal8 = bigdecimal;
		int i = 0;
		int j = bigdecimal.scale() + 5;
		do {
			i++;
			bigdecimal3 = bigdecimal3.multiply(BigDecimal.valueOf((2 * i - 1) * (2 * i)));
			bigdecimal2 = bigdecimal2.multiply(FOUR);
			bigdecimal8 = bigdecimal8.multiply(bigdecimal7).setScale(j, roundingMode);
			BigDecimal bigdecimal10 = Bernoulli(2 * i, j, roundingMode); //Bernoulli.b(2 * i, j, roundingMode);
			BigDecimal bigdecimal9 = bigdecimal2.multiply(bigdecimal2.subtract(ONE)).multiply(bigdecimal8).multiply(bigdecimal10);
			BigDecimal bigdecimal6 = bigdecimal9.divide(bigdecimal3, j, roundingMode);
			if(bigdecimal6.compareTo(ZERO) == 0)
				break;
			bigdecimal4 = bigdecimal4.add(bigdecimal6);
			System.out.println(bigdecimal4);
		} while(i <= 200);
		return bigdecimal4.setScale(j, roundingMode);
	}
	 */

	/**
	 * Sinus hyperbolicus
	 * @param bigdecimal Value to calculate sinh from
	 * @param scale Scale for result
	 * @return Sinus hyperbolicus
	 */
	public static BigDecimal sinh(BigDecimal bigdecimal, int scale) {
		BigDecimal bigdecimal1 = exp(bigdecimal, scale + 2);
		BigDecimal bigdecimal2 = ONE.divide(bigdecimal1, scale + 2, roundingMode);
		return bigdecimal1.subtract(bigdecimal2).divide(TWO, scale, roundingMode);
	}

	/**
	 * Cosinus hyperbolicus
	 * @param bigdecimal Value to calculate cosh from
	 * @param scale Scale for result
	 * @return Cosinus hyperbolicus
	 */
	public static BigDecimal cosh(BigDecimal bigdecimal, int scale) {
		BigDecimal bigdecimal1 = exp(bigdecimal, scale + 2);
		BigDecimal bigdecimal2 = ONE.divide(bigdecimal1, scale + 2, roundingMode);
		return bigdecimal1.add(bigdecimal2).divide(TWO, scale, roundingMode);
	}

	/**
	 * Tangens hyperbolicus
	 * @param bigdecimal Value to calculate tanh from
	 * @param scale Scale for result
	 * @return Tangens hyperbolicus
	 */
	public static BigDecimal tanh(BigDecimal bigdecimal, int scale) {
		BigDecimal bigdecimal1 = exp(bigdecimal, scale + 2);
		BigDecimal bigdecimal2 = ONE.divide(bigdecimal1, scale + 2, roundingMode);
		return bigdecimal1.subtract(bigdecimal2).divide(bigdecimal1.add(bigdecimal2), scale, roundingMode);
	}

	/**
	 * Arccussinus hyperbolicus
	 * @param bigdecimal Value to calculate asinh from
	 * @param scale Scale for result
	 * @return Arcussinus hyperbolicus
	 */
	public static BigDecimal asinh(BigDecimal bigdecimal, int scale) {
		return log(bigdecimal.add(sqrt(bigdecimal.multiply(bigdecimal).setScale(scale + 5, roundingMode).add(ONE), scale)), scale);
	}

	/**
	 * Arcuscosinus hyperbolicus
	 * @param bigdecimal Value to calculate acosh from
	 * @param scale Scale for result
	 * @return Arcuscosinus hyperbolicus
	 */
	public static BigDecimal acosh(BigDecimal bigdecimal, int scale) {
		return log(bigdecimal.add(sqrt(bigdecimal.multiply(bigdecimal).setScale(scale + 5, roundingMode).subtract(ONE), scale)), scale);
	}

	/**
	 * Arcustangens hyperbolicus
	 * @param bigdecimal Value to calculate atanh from
	 * @param scale Scale for result
	 * @return Arcustangens hyperbolicus
	 */
	public static BigDecimal atanh(BigDecimal bigdecimal, int scale) {
		return log(ONE.add(bigdecimal).divide(ONE.subtract(bigdecimal), scale + 5, roundingMode),scale).divide(TWO, scale, roundingMode);
	}

	/**
	 * Calculate Euler function e^x
	 * @param bigdecimal Value to calculate exp from
	 * @param scale Scale for result
	 * @return Euler function e^x
	 */
	public static BigDecimal exp(BigDecimal bigdecimal, int scale) {
		int j = scale + 2;
		BigDecimal bigdecimal1 = ONE;
		BigDecimal bigdecimal2 = ONE;
		BigDecimal bigdecimal3 = ONE;
		BigDecimal bigdecimal4 = ZERO.setScale(j, roundingMode);
		BigDecimal bigdecimal5 = ZERO;
		//BigDecimal bigdecimal6 = THREE;
		bigdecimal3 = ONE.setScale(j, roundingMode);
		do {
			bigdecimal2 = bigdecimal2.multiply(bigdecimal).setScale(j, roundingMode);
			bigdecimal5 = bigdecimal5.add(ONE);
			bigdecimal1 = bigdecimal1.multiply(bigdecimal5);
			BigDecimal bigdecimal7 = bigdecimal2.divide(bigdecimal1, j, roundingMode);
			if(bigdecimal7.setScale(j, roundingMode).compareTo(bigdecimal4) == 0)
				break;
			bigdecimal3 = bigdecimal3.add(bigdecimal7);
		} while(!interrupt);
		return bigdecimal3.setScale(scale, roundingMode);
	}

	/**
	 * Calculate Natural logarithm log(x) [or ln(x)]
	 * @param bigdecimal Value to calculate log for
	 * @param scale Scale for result
	 * @return Natural logarithm log(x)
	 */
	public static BigDecimal log(BigDecimal bigdecimal, int scale) {
		int j = scale;
		if(bigdecimal.compareTo(ZERO) < 1)
			return bigdecimal.negate().divide(ZERO, scale, roundingMode);
		if(bigdecimal.compareTo(ONE) == 0)
			return ZERO;
		if(bigdecimal.compareTo(TWO) == 0)
			return log2(scale);
		BigDecimal bigdecimal1 = ZERO;
		BigDecimal bigdecimal2 = new BigDecimal("1.5");
		boolean flag = false;
		if(bigdecimal.compareTo(ONE) == -1) {
			bigdecimal = ONE.divide(bigdecimal, scale + 20, roundingMode);
			flag = true;
		}
		if(bigdecimal.compareTo(TWO) == 0)
			return log2(scale).negate();
		while(bigdecimal.compareTo(bigdecimal2) == 1) {
			j++;
			bigdecimal = bigdecimal.divide(TWO, j, roundingMode);
			bigdecimal1 = bigdecimal1.add(ONE);
			if(bigdecimal.compareTo(TWO) == 0)
				return log2(j).multiply(bigdecimal1.add(ONE));
		}
		bigdecimal = bigdecimal.subtract(ONE).divide(bigdecimal.add(ONE), j + 20, roundingMode);
		BigDecimal bigdecimal3 = bigdecimal.multiply(bigdecimal);
		j += 20;
		BigDecimal bigdecimal4 = bigdecimal;
		BigDecimal bigdecimal5 = ONE;
		BigDecimal bigdecimal6 = bigdecimal;
		//BigDecimal bigdecimal7 = THREE;
		do {
			bigdecimal4 = bigdecimal4.multiply(bigdecimal3).setScale(j, roundingMode);
			bigdecimal5 = bigdecimal5.add(TWO);
			BigDecimal bigdecimal8 = bigdecimal4.divide(bigdecimal5, j + 20, roundingMode);
			if(bigdecimal8.compareTo(ZERO) == 0)
				break;
			bigdecimal6 = bigdecimal6.add(bigdecimal8);
		} while(!interrupt);
		bigdecimal6 = bigdecimal6.add(bigdecimal6);
		if(bigdecimal1.compareTo(ZERO) == 1)
			bigdecimal6 = bigdecimal6.add(bigdecimal1.multiply(log2(j)));
		if(flag)
			bigdecimal6 = bigdecimal6.negate();
		return bigdecimal6.setScale(scale, roundingMode);
	}

	/*
	public static BigDecimal log_(BigDecimal bigdecimal) {
		if(bigdecimal.compareTo(TWO) == 0)
			return log2(bigdecimal.scale());
		if(bigdecimal.compareTo(ZERO) < 1)
			return bigdecimal.negate().divide(ZERO, bigdecimal.scale(), roundingMode);
		BigDecimal bigdecimal1 = ZERO;
		for(BigDecimal bigdecimal2 = new BigDecimal("1.5"); bigdecimal.compareTo(bigdecimal2) > -1;) {
			bigdecimal = bigdecimal.divide(TWO, bigdecimal.scale() + 1, roundingMode);
			bigdecimal1 = bigdecimal1.add(ONE);
		}

		BigDecimal bigdecimal3 = bigdecimal.subtract(ONE);
		boolean flag = true;
		int i = bigdecimal.scale() + 20;
		BigDecimal bigdecimal4 = bigdecimal3;
		BigDecimal bigdecimal5 = ONE;
		BigDecimal bigdecimal6 = bigdecimal3;
		BigDecimal bigdecimal7 = THREE;
		do {
			bigdecimal4 = bigdecimal4.multiply(bigdecimal3).setScale(bigdecimal.scale() * 4, roundingMode);
			bigdecimal5 = bigdecimal5.add(ONE);
			BigDecimal bigdecimal8 = bigdecimal4.divide(bigdecimal5, i, roundingMode);
			if(bigdecimal8.compareTo(ZERO) == 0)
				break;
			flag = !flag;
			if(flag)
				bigdecimal6 = bigdecimal6.add(bigdecimal8);
			else
				bigdecimal6 = bigdecimal6.subtract(bigdecimal8);
		} while(true);
		if(bigdecimal1.compareTo(ZERO) == 1)
			bigdecimal6 = bigdecimal6.add(bigdecimal1.multiply(log2(i)));
		return bigdecimal6.setScale(bigdecimal.scale(), roundingMode);
	}
	 */

	/**
	 * Calculate natural logarithm of two [log(2) or ln(2)]
	 * @param scale Scale for result
	 * @return Natural logarithm of 2
	 */
	static BigDecimal log2(int scale) {
		if(log2_.scale() >= scale)
			return log2_.setScale(scale, roundingMode);
		//BigDecimal bigdecimal = ONE;
		BigDecimal bigdecimal1 = ONE;
		BigDecimal bigdecimal2 = ONE;
		//BigDecimal bigdecimal3 = THREE;
		BigDecimal bigdecimal5 = ONE;
		do {
			bigdecimal5 = bigdecimal5.add(TWO);
			bigdecimal1 = bigdecimal1.multiply(NINE);
			BigDecimal bigdecimal4 = ONE.divide(bigdecimal1.multiply(bigdecimal5), scale + 20, roundingMode);
			if(bigdecimal4.compareTo(ZERO) == 0)
				break;
			bigdecimal2 = bigdecimal2.add(bigdecimal4);
		} while(!interrupt);
		log2_ = bigdecimal2.add(bigdecimal2).divide(THREE, scale, roundingMode);
		return log2_.setScale(scale, roundingMode);
	}

	/**
	 * Calculate natural logarithm of 10 [log(10) or ln(10)]
	 * @param scale Scale for result
	 * @return Natural logarithm of 2
	 */
	static BigDecimal log10(int scale) {
		if(log10_.scale() >= scale)
			return log10_.setScale(scale, roundingMode);
		//BigDecimal bigdecimal = ONE;
		BigDecimal bigdecimal1 = ONE;
		BigDecimal bigdecimal2 = ONE;
		//BigDecimal bigdecimal3 = THREE;
		BigDecimal bigdecimal5 = ONE;
		do {
			bigdecimal5 = bigdecimal5.add(TWO);
			bigdecimal1 = bigdecimal1.multiply(NINE);
			BigDecimal bigdecimal4 = ONE.divide(bigdecimal1.multiply(bigdecimal5), scale + 20, roundingMode);
			if(bigdecimal4.compareTo(ZERO) == 0)
				break;
			bigdecimal2 = bigdecimal2.add(bigdecimal4);
		} while(!interrupt);
		log10_ = bigdecimal2.add(bigdecimal2).divide(THREE, scale, roundingMode);
		return log2_.setScale(scale, roundingMode);
	}


	/**
	 * Calculate X to the power of Y: x^y
	 * @param bigdecimal Base (x)
	 * @param bigdecimal1 Exponent (y)
	 * @param scale
	 * @return X to the power of Y: x^y
	 */
	static BigDecimal pow(BigDecimal bigdecimal, BigDecimal bigdecimal1, int scale) {
		int j = scale + 5;
		if(bigdecimal1.setScale(0, roundingMode).compareTo(bigdecimal1) == 0) {
			boolean flag = bigdecimal1.signum() == -1;
			BigInteger biginteger = bigdecimal1.abs().toBigInteger();
			BigDecimal bigdecimal2 = ONE;
			for(int k = biginteger.bitLength(); k > 0; k--) {
				if(biginteger.testBit(k))
					bigdecimal2 = bigdecimal2.multiply(bigdecimal).setScale(j, roundingMode);
				bigdecimal2 = bigdecimal2.multiply(bigdecimal2).setScale(j, roundingMode);
				if(interrupt)
					break;
			}
			if(biginteger.testBit(0))
				bigdecimal2 = bigdecimal2.multiply(bigdecimal).setScale(j, roundingMode);
			if(flag)
				bigdecimal2 = ONE.divide(bigdecimal2, j, roundingMode);
			return bigdecimal2.setScale(scale, roundingMode);
		}
		if(bigdecimal1.compareTo(ZERO) == 0)
			return ONE;
		if(bigdecimal1.compareTo(ONE) == 0)
			return bigdecimal;
		if(bigdecimal1.compareTo(ONE.negate()) == 0)
			return ONE.divide(bigdecimal, scale, roundingMode);
		if(bigdecimal1.compareTo(TWO) == 0)
			return bigdecimal.multiply(bigdecimal).setScale(scale, roundingMode);
		if(bigdecimal1.compareTo(ONE) == 0)
			return bigdecimal;
		return exp(bigdecimal1.multiply(log(bigdecimal, j + 10)), j + 10).setScale(scale, roundingMode);
	}

	/*
	static private BigDecimal Bernoulli( int n, int scale, int rm) {
		if ( (n&1)==1 )
			return ZERO; // odd -> zero
		if (n==0)
			return ONE;
		if (n==1)
			return HALF.negate();
		Factorial f = new Factorial();
		BigInteger i = f.factorial(n);
		return ONE.divide(new BigDecimal(i),scale,rm);
	}
	 */

	final static BigDecimal ONE = BigDecimal.valueOf(1);
	final static BigDecimal TWO = BigDecimal.valueOf(2);
	final static BigDecimal HALF = new BigDecimal("0.5");
	final static BigDecimal THREE = BigDecimal.valueOf(3);
	final static BigDecimal FOUR = BigDecimal.valueOf(4);
	final static BigDecimal FIVE = BigDecimal.valueOf(5);
	final static BigDecimal EIGHT = BigDecimal.valueOf(8);
	final static BigDecimal NINE = BigDecimal.valueOf(9);
	final static BigDecimal TWELFE = BigDecimal.valueOf(12);
	final static BigDecimal SIXTEEN = BigDecimal.valueOf(16);
	final static BigDecimal ZERO = BigDecimal.valueOf(0);
	static BigDecimal PI_    = new BigDecimal("3.141592653589793238462643383279502884197169399375105820974944592307816406286208998628034825342117067982148086513282306647093844609550582231725359408128481117450284102701938521105559644622948954930381964428810975665933446128475648233786783165271201909145648566923460348610454326648213393607260249141273724587006606315588174881520920962829254091715364367892590360011330530548820466521384146951941511609433057270365759591953092186117381932611793105118548074462379962749567351885752724891227938183011949129833673362440656643");
	static BigDecimal E_     = new BigDecimal("2.718281828459045235360287471352662497757247093699959574966967627724076630353547594571382178525166427427466391932003059921817413596629043572900334295260595630738132328");
	static BigDecimal log2_  = new BigDecimal("0.6931471805599453094172321214581765680755001343602552541206800094933936219696947156058633269964186875");
	static BigDecimal log10_ = new BigDecimal("2.3025850929940456840179914546843642076011014886287729760333279009675726096773524802359972050895982983");

	/**
	 * Rounding mode applied in here
	 */
	public static int roundingMode = BigDecimal.ROUND_HALF_EVEN;

	/**
	 * Interrupt calculation (set to true to interrupt)
	 */
	public static boolean interrupt = false;


}