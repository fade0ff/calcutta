package GUI;

import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.HeadlessException;
import java.awt.Point;
import java.awt.image.BufferedImage;

import javax.swing.DefaultListCellRenderer;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JColorChooser;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;

/*
 * Copyright 2009 Volker Oth
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * More or less generic color chooser dialog.
 *
 * @author Volker Oth
 */
public class ColorDialog extends JDialog {
	final static long serialVersionUID = 0x000000001;

	private javax.swing.JPanel jContentPane = null;

	private JComboBox jFontBox = null;
	private JLabel lblColor = null;
	private JScrollPane jScrollPane = null;
	private JLabel lblFontSize = null;
	private JList jList = null;
	private JButton btnOk = null;
	private JButton btnCancel = null;
	private JButton btnDefault = null;
	/* custom created attributes */
	private ImageIcon cIcon[];      /* image icon to preview color */
	private Color  cColor[];        /* selected color */
	//	private Color  cColorBackup[];  /* backup for original colors */
	private Color  cColorDefault[]; /* default colors */
	private String cName[];         /* name of color element */
	private int fontSize;
	private String  fontSizes[] = { "8", "10", "12", "14", "18", "24"};

	private JButton btnColor = null;
	/**
	 * Paint JButton's Icon in a given color
	 * @param b JButton to paint
	 * @param c Color to paint
	 */
	private void paintIcon(ImageIcon i, Color c) {
		Graphics g = i.getImage().getGraphics();
		g.setColor(c);
		g.setPaintMode();
		g.fillRect(0,0,i.getIconWidth(),i.getIconHeight());
	}

	/**
	 * custom initialization function (not created by VisualEditor)
	 */
	private void MyInit() {
		// fill font size combo box
		for (int i=0; i<fontSizes.length; i++)
			jFontBox.addItem(fontSizes[i]);
	}

	/**
	 * Set dialog parameters
	 * @param String cName color names
	 * @param cCurrent current colors
	 * @param cDefault default colors
	 * @param fSize  Font size
	 */
	public void setParameters(String cName_[], Color cCurrent[], Color cDefault[], int fSize) {
		cName = cName_;
		// set fontsize
		int index = 0;
		fontSize = fSize;
		for (int i=0; i<fontSizes.length; i++) {
			if (Integer.parseInt(fontSizes[i]) == fontSize) {
				index = i;
				break;
			}
		}
		jFontBox.setSelectedIndex(index);
		// set backup and default colors
		//	    cColorBackup = cCurrent;
		cColorDefault = cDefault;
		cColor = new Color[cName.length];
		cIcon = new ImageIcon[cName.length];
		// init color list box
		jList = new JList(cName);
		jList.setSelectedIndex(0);
		jList.setSelectionMode( ListSelectionModel.SINGLE_SELECTION);
		jList.setCellRenderer(new myListCellRenderer());
		for (int i=0; i < cName.length; i++) {
			cIcon[i] = new ImageIcon(new BufferedImage(12,12,BufferedImage.TYPE_INT_RGB ));
			cColor[i] = new Color(cCurrent[i].getRGB());
			paintIcon(cIcon[i],cColor[i]);
		}
		jScrollPane.setViewportView(jList);
	}

	public Color[] getColors() {
		return cColor;
	}

	public int getFontSize() {
		return fontSize;
	}

	/**
	 * Constructor for modal dialog in parent frame
	 * @param frame parent frame
	 * @param modal create modal dialog?
	 */
	public ColorDialog(JFrame frame, boolean modal) {
		super(frame, modal);
		initialize();
		//
		Point p = frame.getLocation();
		this.setLocation(p.x+frame.getWidth()/2-getWidth()/2, p.y+frame.getHeight()/2-getHeight()/2);
		MyInit();
		//
	}

	/**
	 * @throws HeadlessException
	 */
	public ColorDialog() throws HeadlessException {
		super();
		initialize();
		MyInit();

	}

	/**
	 * This method initializes this
	 *
	 * @return void
	 */
	private void initialize() {
		this.setContentPane(getJContentPane());  // Generated
		this.setSize(372, 187);
		this.setTitle("Choose Colors and Font");
		this.addWindowListener(new java.awt.event.WindowAdapter() {
			@Override
			public void windowClosing(java.awt.event.WindowEvent e) {
				cColor = cColorDefault; // same as cancel
			}
		});
	}

	/**
	 * This method initializes jContentPane
	 *
	 * @return javax.swing.JPanel
	 */
	private javax.swing.JPanel getJContentPane() {
		if(jContentPane == null) {
			lblFontSize = new JLabel();
			lblColor = new JLabel();
			jContentPane = new javax.swing.JPanel();
			jContentPane.setLayout(null);
			lblColor.setText("Choose Color");  // Generated
			lblColor.setBounds(15, 9, 73, 16);  // Generated
			lblFontSize.setBounds(15, 120, 50, 20);  // Generated
			lblFontSize.setText("Font Size");  // Generated
			jContentPane.add(lblColor, null);  // Generated
			jContentPane.add(getBtnOk(), null);  // Generated
			jContentPane.add(getBtnCancel(), null);  // Generated
			jContentPane.add(getBtnDefault(), null);  // Generated
			jContentPane.add(getJFontBox(), null);  // Generated
			jContentPane.add(getJScrollPane(), null);  // Generated
			jContentPane.add(lblFontSize, null);  // Generated
			jContentPane.add(getBtnColor(), null);  // Generated
		}
		return jContentPane;
	}

	/**
	 * This method initializes jFontBox
	 *
	 * @return javax.swing.JComboBox
	 */
	private JComboBox getJFontBox() {
		if (jFontBox == null) {
			jFontBox = new JComboBox();
			jFontBox.setBounds(80, 120, 85, 20);  // Generated
		}
		return jFontBox;
	}
	/**
	 * This method initializes jScrollPane
	 *
	 * @return javax.swing.JScrollPane
	 */
	private JScrollPane getJScrollPane() {
		if (jScrollPane == null) {
			jScrollPane = new JScrollPane();
			jScrollPane.setBounds(15, 30, 150, 80);  // Generated
			//jScrollPane.setViewportView(getJList());  // Generated
		}
		return jScrollPane;
	}
	/**
	 * This method initializes jList
	 *
	 * @return javax.swing.JList
	 */
	/*
	private ValueList getJList() {
		if (jList == null) {
			jList = new ValueList();
		}
		return jList;
	}*/
	/**
	 * This method initializes jButton
	 *
	 * @return javax.swing.JButton
	 */
	private JButton getBtnOk() {
		if (btnOk == null) {
			btnOk = new JButton();
			btnOk.setBounds(175, 120, 170, 20);  // Generated
			btnOk.setText("OK");  // Generated
			btnOk.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {
					fontSize = Integer.parseInt(fontSizes[jFontBox.getSelectedIndex()]);
					dispose();
				}
			});
		}
		return btnOk;
	}
	/**
	 * This method initializes jButton
	 *
	 * @return javax.swing.JButton
	 */
	private JButton getBtnCancel() {
		if (btnCancel == null) {
			btnCancel = new JButton();
			btnCancel.setBounds(175, 90, 170, 20);  // Generated
			btnCancel.setText("Cancel");  // Generated
			btnCancel.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {
					cColor = cColorDefault;
					dispose();
				}
			});
		}
		return btnCancel;
	}
	/**
	 * This method initializes jButton
	 *
	 * @return javax.swing.JButton
	 */
	private JButton getBtnDefault() {
		if (btnDefault == null) {
			btnDefault = new JButton();
			btnDefault.setBounds(175, 60, 170, 20);  // Generated
			btnDefault.setText("Restore default Colors");  // Generated
			btnDefault.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {
					for (int i=0; i<cName.length; i++) {
						cColor[i] = new Color(cColorDefault[i].getRGB());
						paintIcon(cIcon[i],  cColor[i]);
					}
					jList.repaint();
				}
			});
		}
		return btnDefault;
	}

	private class myListCellRenderer extends DefaultListCellRenderer {
		final static long serialVersionUID = 0x000000001;
		@Override
		public Component getListCellRendererComponent( JList list, Object value, int index, boolean isSelected,  boolean cellHasFocus) {
			Component retValue = super.getListCellRendererComponent( list, value, index, isSelected, cellHasFocus);
			setText(cName[index]);
			setIcon(cIcon[index]);
			return retValue;
		}
	}
	/**
	 * This method initializes jButton
	 *
	 * @return javax.swing.JButton
	 */
	private JButton getBtnColor() {
		if (btnColor == null) {
			btnColor = new JButton();
			btnColor.setBounds(175, 30, 170, 20);  // Generated
			btnColor.setText("Change Color");  // Generated
			btnColor.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {
					int idx = jList.getSelectedIndex();
					Color c = JColorChooser.showDialog( null, "Chose Input Color "+cName[idx], cColor[idx] );
					if (c != null)
						cColor[idx] = c;
					paintIcon(cIcon[idx],  cColor[idx]);
					jList.repaint();
				}
			});
		}
		return btnColor;
	}
}  //  @jve:decl-index=0:visual-constraint="311,10"
