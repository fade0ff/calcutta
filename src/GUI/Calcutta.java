package GUI;

import java.awt.Color;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GraphicsEnvironment;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.StringSelection;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLDecoder;

import javax.jnlp.ServiceManager;
import javax.jnlp.UnavailableServiceException;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.text.BadLocationException;
import javax.swing.text.DefaultStyledDocument;
import javax.swing.text.JTextComponent;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;

import Parser.ErrorPos;
import Parser.JParse;
import Parser.JParseEvent;
import Parser.JParseListener;
import Parser.Operator;
import Parser.ParseException;
import Parser.Value;
import Tools.Props;

/*
 * Copyright 2009 Volker Oth
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * GUI class for parser. All the parser functionality is implemented in {@link JParse}.
 *
 * @author Volker Oth
 */
public class Calcutta extends JFrame {

	private javax.swing.JPanel jContentPane = null;
	private JTextField jInputField = null;
	private JScrollPane jScrollPane = null;
	private JTextPane jOutputPane = null;
	private JMenuBar jJMenuBar = null;
	private JMenu jMenuFile = null;
	private JMenuItem jMenuItemExit = null;
	private JMenu jMenuOptions = null;
	private JMenuItem jMenuItemColors = null;
	private JMenu jMenuView = null;
	private JMenuItem jMenuItemClear = null;
	private JMenuItem jMenuItemScale = null;
	private JMenuItem jMenuItemSkriptPath = null;
	private JMenuItem jMenuItemBreak = null;
	private JMenu jMenuHelp = null;
	private JMenuItem jMenuItemHelp = null;
	private JPopupMenu jPopupMenu = null;
	private JMenuItem jPopupMenuItemCopy = null;
	private JMenuItem jPopupMenuItemPaste = null;
	private JMenuItem jPopupMenuItemBreak = null;


	/* custom created attributes */
	final static long serialVersionUID = 0x000000001;
	private final static String versionStr = "Calcutta V0.89c";
	private final static String iniStr = "Calcutta.ini";
	//	private static String jreStr;
	private static String iniFileStr; // including path
	private String scriptPath;
	private DefaultStyledDocument jOutputDoc;
	private Props props;
	private SimpleAttributeSet outputAtr, errorAtr, outputOpAtr, outputStringAtr;
	private SimpleAttributeSet outputNum1Atr, outputNum2Atr, outputBracketAtr, outputCommentAtr;
	private static int fontSize;
	private int maxDocSize = 1000000; // to work around bad TextPane performance
	private Calcutta jCalc;
	private JParse jParse;
	private CalcThread calcThread;
	private String commandHistory[] = new String[20];
	private int commandHistoryStoreIndex=0;
	private int commandHistoryReadIndex=0;
	private boolean addLinefeed = false;

	private Component popupSource;

	private int internalScale=64;

	final static String bracketChars = "[](){}";
	final static String operatorChars = Operator.operatorChars+";,.:?";
	final static String numericBreakCharacters = JParse.delimiterChars+Operator.operatorChars+"p";

	private final String cName[] = { "Output", "Num1", "Num2", "Comment", "Operator", "Bracket", "String", "Error" };
	private Color cColor[];        /* selected color */
	private final Color cColorDefault[] /* default colors */ = {
			Color.BLUE, Color.BLACK, new Color(100,100,255), Color.GRAY,
			new Color(255,100,0), new Color(100,0,50), new Color(50,150,0), Color.RED
	};

	private static boolean isWebstartApp = true;

	/**
	 * Return main window font size
	 * @return font size in points
	 */
	public static int getFontSize() {
		return fontSize;
	}

	public static String addSeparator(String fName) {
		int pos = fName.lastIndexOf(File.separator);
		if (pos != fName.length()-1)
			pos = fName.lastIndexOf("/");
		if (pos != fName.length()-1)
			return fName + "/";
		else return fName;
	}

	public static String exchangeSeparators(String fName) {
		int pos;
		StringBuffer sb = new StringBuffer(fName);
		while ( (pos = sb.indexOf("\\")) != -1 )
			sb.setCharAt(pos,'/');
		return sb.toString();
	}

	// If a string is on the system clipboard, this method returns it;
	// otherwise it returns null.
	private static String getClipboard() {
		Transferable t = Toolkit.getDefaultToolkit().getSystemClipboard().getContents(null);

		try {
			if (t != null && t.isDataFlavorSupported(DataFlavor.stringFlavor)) {
				String text = (String)t.getTransferData(DataFlavor.stringFlavor);
				return text;
			}
		} catch (UnsupportedFlavorException e) {
		} catch (IOException e) {
		}
		return null;
	}

	// This method writes a string to the system clipboard.
	// otherwise it returns null.
	private static void setClipboard(String str) {
		StringSelection ss = new StringSelection(str);
		Toolkit.getDefaultToolkit().getSystemClipboard().setContents(ss, null);
	}

	/**
	 * Calculation thread implemented as internal class
	 */
	class CalcThread extends Thread {
		String command;

		/**
		 * Pass command string to calculation thread. Must be called before starting the thread
		 * @param cmd Command string to parse
		 */
		public void setCommand(String cmd) {
			command = cmd;
		}

		/**
		 * Thread run method
		 * @see java.lang.Runnable#run()
		 */
		@Override
		public void run() {
			try {
				Value result = jParse.eval(command);
				if (addLinefeed) {
					printOut("\n");
					addLinefeed = false;
				}
				printOutColored("        out = "+result.toString()+"\n\n");
				setBusy(false);
			} catch (ParseException ex) {
				if (addLinefeed) {
					printOut("\n");
					addLinefeed = false;
				}
				ErrorPos epos = jParse.getErrorPos();
				if (epos.getLine() > 1) {
					String file = epos.getFile().substring(5);
					printErr("\n");
					if (epos.getFile().length()>0)
						printErr("File: "+file+", ");
					printErr("Line: "+epos.getLine()+", Pos: "+epos.getPos()+"\n");
				}
				try {
					command = jParse.getErrorString();
				} catch (ParseException e) {
					command = e.getMessage();
				};
				printErr("\n"+command+"\n");
				StringBuffer sb = new StringBuffer();
				for (int i=0; i<epos.getPos();i++)
					sb.append('-');
				sb.append("^\n");
				printErr(sb.toString());

				printErr(ex.getMessage()+"\n\n");
				setBusy(false);

			}
		}
	}

	/**
	 * Enable/disable busy cursor and command input
	 * @param busy true: enter busy state, false: leave busy state
	 */
	private void setBusy(boolean busy) {
		if (busy) {
			jInputField.setEditable(false);
			jMenuItemBreak.setEnabled(true);
			jPopupMenuItemBreak.setEnabled(true);
			jOutputPane.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
			jInputField.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));

		} else {
			jInputField.setEditable(true);
			jMenuItemBreak.setEnabled(false);
			jPopupMenuItemBreak.setEnabled(false);
			jOutputPane.setCursor(Cursor.getDefaultCursor());
			jInputField.setCursor(Cursor.getDefaultCursor());
			jInputField.requestFocus();
		}
	}

	class PopupListener extends MouseAdapter {
		@Override
		public void mousePressed(MouseEvent e) {
			maybeShowPopup(e);
		}

		@Override
		public void mouseReleased(MouseEvent e) {
			maybeShowPopup(e);
		}

		private void maybeShowPopup(MouseEvent e) {
			if (e.isPopupTrigger()) {
				popupSource = e.getComponent();
				boolean canPaste = ((JTextComponent)popupSource).isEditable() && (getClipboard() != null);
				boolean canCopy = ((JTextComponent)popupSource).getSelectedText() != null;
				jPopupMenuItemPaste.setEnabled(canPaste);
				jPopupMenuItemCopy.setEnabled(canCopy);
				jPopupMenu.show(popupSource,e.getX(), e.getY());
			}
		}
	}

	/**
	 * Checks is a character is a valid (starting) character for a number
	 * @param c the char to be checked
	 * @return true (is numeric) or false (is not numeric)
	 */
	private boolean isNumeric(char c) {
		return ("0123456789.".indexOf(c) != -1);
	}


	/**
	 * Color text in output pane depending on the contents of the inserted string
	 * @param s String that was inserted into the output pane
	 * @param offset Start offset where string was inserted
	 */
	private void colorOutput(String s, int offset) {
		int startIdx = 0;
		boolean lastBreak = true; // last character was a break character
		char c;
		for (int idx=0; idx<s.length();idx++) {
			c = s.charAt(idx);
			startIdx = idx;
			// check for line comments
			if (c=='/' && idx<s.length()-1 && s.charAt(idx+1)=='/') {
				startIdx = idx;
				idx++;
				while (++idx<s.length() && (c=s.charAt(idx))!='\n');
				jOutputDoc.setCharacterAttributes(offset+startIdx,idx-startIdx,outputCommentAtr,true);
			}
			// check for operators
			startIdx = idx;
			while (operatorChars.indexOf(c)!=-1 && idx<s.length())
				c = s.charAt(++idx);
			if (idx != startIdx) {
				jOutputDoc.setCharacterAttributes(offset+startIdx,idx-startIdx,outputOpAtr,true);
				lastBreak = true;
			}
			// check for brackets (no own state, since there's usually only one bracket in a row)
			if (bracketChars.indexOf(c)!=-1) {
				jOutputDoc.setCharacterAttributes(offset+idx,1,outputBracketAtr,true);
				lastBreak = true;
				continue;
			}
			// check for strings
			if ( c=='"') {
				startIdx = idx;
				while (++idx<s.length() && (c=s.charAt(idx))!='"');
				jOutputDoc.setCharacterAttributes(offset+startIdx,idx-startIdx+1,outputStringAtr,true);
				lastBreak = true; // discussable as this would be a syntax error anyway
			}
			// check for numbers
			if (lastBreak && isNumeric(c)) {
				int len, step, point=-1;
				boolean color1 = true;
				boolean dec = true;
				startIdx = idx;
				// search for break character
				while (++idx<s.length() && numericBreakCharacters.indexOf(c=s.charAt(idx))==-1) {
					if (c=='.')
						point = idx-startIdx; // position of point inside number
				}
				if (idx!=s.length()) idx--; // idx on last numeric character
				len = idx-startIdx+1;
				step = 3;
				// check kind of number
				if (len > 1) {
					if (s.charAt(startIdx)=='0') {
						// special cases: octal, binary, hexadecimal
						switch (s.charAt(startIdx+1)) {
							case 'x': dec = false; step = 2; startIdx+=2; len-=2; break; // octal
							case 'b': dec = false; step = 4; startIdx+=2; len-=2; break; // octal
							case '.': dec = false; step = 3; break; // decimal
							default: dec = false; step = 2; break;  // octal
						}
					}
				}
				// special case: search for 'e' and 'p'
				if (dec)
					for (int pos=startIdx; pos<startIdx+len; pos++) {
						c=s.charAt(pos);
						if (c == 'e' || c == 'E') {
							idx = pos;
							len = idx-startIdx;
							break;
						}
					}
				if (point != -1) {
					int step2 = step;
					// special case: decimal point
					// first color characters after the point from left to right
					for (int pos=point+1; pos<len; pos+=step2) {
						if (pos+step2 >= len)
							step2 = len - pos; // don't color outside number
						jOutputDoc.setCharacterAttributes(offset+startIdx+pos,step2,
								color1 ? outputNum1Atr : outputNum2Atr,true);
						color1 = !color1; // toggle
					}
					// then color characters before the point from right to left
					len = point;
					color1 = true;
				}
				for (int pos=len; pos>0; pos-=step) {
					if (pos-step < 0) // don't color outside number
						step = pos;
					jOutputDoc.setCharacterAttributes(offset+startIdx+pos-step,step,
							color1 ? outputNum1Atr : outputNum2Atr,true);
					color1 = !color1; // toggle
				}
				continue;
			}
			// this should be only reached for variables or spaces
			if (c==' ')
				lastBreak = true;
			else
				lastBreak = false;

		}
	}

	/**
	 * Print text to output pane with syntax coloring
	 * @param s text to print
	 */
	public void printOutColored(String s) {
		try {
			int length = jOutputDoc.getLength();
			if (length>maxDocSize) {
				jOutputDoc.remove(0,length-maxDocSize);
				length = jOutputDoc.getLength();
			}
			jOutputDoc.insertString(length,s,outputAtr);
			// now start the coloring
			colorOutput(s, length);
			jOutputPane.setCaretPosition(jOutputDoc.getLength());
		}
		catch (BadLocationException ble) {
			System.out.println("Couldn't write to Output Pane");
		}
	}


	/**
	 * Print text to output pane
	 * @param s text to print
	 */
	public void printOut(String s) {
		try {
			int length = jOutputDoc.getLength();
			if (length>maxDocSize) {
				jOutputDoc.remove(0,length-maxDocSize);
				length = jOutputDoc.getLength();
			}
			jOutputDoc.insertString(length,s,outputAtr);
			jOutputPane.setCaretPosition(jOutputDoc.getLength());
		}
		catch (BadLocationException ble) {
			System.out.println("Couldn't write to Output Pane");
		}
	}

	/**
	 * Print text to error pane
	 * @param s text to print
	 */
	public void printErr(String s) {
		try {
			int length = jOutputDoc.getLength();
			if (length>maxDocSize) {
				jOutputDoc.remove(0,length-maxDocSize);
				length = jOutputDoc.getLength();
			}
			jOutputDoc.insertString(length,s,errorAtr);
			jOutputPane.setCaretPosition(jOutputDoc.getLength());
		}
		catch (BadLocationException ble) {
			System.out.println("Couldn't write to Output Pane");
		}
	}
	/**
	 * Common exit method to use in exit events
	 */
	private void exit() {
		// store width and height
		Dimension d = this.getSize();
		props.set("frameWidth", d.width);
		props.set("frameHeight", d.height);
		// store frame pos
		Point p = this.getLocation();
		props.set("framePosX", p.x);
		props.set("framePosY", p.y);
		//
		props.save(iniFileStr);
		System.exit(0);
	}

	/**
	 * Initialize instance, load properties etc.
	 */
	private void init() {
		// get ini path
		if (isWebstartApp) {
			iniFileStr = exchangeSeparators(System.getProperty("user.home"));
			iniFileStr = addSeparator(iniFileStr);
		} else {
			String s = this.getClass().getName().replace('.','/') + ".class";
			URL url = this.getClass().getClassLoader().getResource(s);
			int pos;
			try {
				iniFileStr = URLDecoder.decode(url.getPath(),"UTF-8");				
			} catch (UnsupportedEncodingException ex) {};
			// special handling for JAR
			if (( (pos=iniFileStr.toLowerCase().indexOf("file:")) != -1))
				iniFileStr = iniFileStr.substring(pos+5);		
			if ( (pos=iniFileStr.toLowerCase().indexOf(s.toLowerCase())) != -1)
				iniFileStr = iniFileStr.substring(0,pos);
			
			// e.g. at his point it's "/E:/src/Java/Calcutta/Calcutta.jar!/"
			if ( (pos = iniFileStr.lastIndexOf(".jar")) != -1)            // find last occurrence of ".jar"
					if ( (pos = iniFileStr.lastIndexOf('/', pos)) != -1)  // find last separator
							iniFileStr = iniFileStr.substring(0,pos+1);			
		}
		iniFileStr += iniStr;
		System.out.println(iniFileStr); // debug
		// load properties
		props = new Props();
		props.setHeader(versionStr);
		props.load(iniFileStr);
		// read props
		internalScale = props.get("internalScale", 64);
		scriptPath = props.get("scriptPath",".");
		fontSize = props.get("fontSize", 12);
		cColor = new Color[8];
		cColor[Colors.OUTPUT] = new Color(props.get("outputColor", cColorDefault[Colors.OUTPUT].getRGB()));
		cColor[Colors.ERROR]  = new Color(props.get("errorColor", cColorDefault[Colors.ERROR].getRGB()));
		cColor[Colors.OPERATOR] = new Color(props.get("outputOperatorColor", cColorDefault[Colors.OPERATOR].getRGB()));
		cColor[Colors.STRING] = new Color(props.get("outputStringColor", cColorDefault[Colors.STRING].getRGB()));
		cColor[Colors.NUM1] = new Color(props.get("outputNumberColor1", cColorDefault[Colors.NUM1].getRGB()));
		cColor[Colors.NUM2] = new Color(props.get("outputNumberColor2", cColorDefault[Colors.NUM2].getRGB()));
		cColor[Colors.BRACKET] = new Color(props.get("outputBracketColor", cColorDefault[Colors.BRACKET].getRGB()));
		cColor[Colors.COMMENT] = new Color(props.get("outputCommentColor", cColorDefault[Colors.COMMENT].getRGB()));
		// read frame props
		int width, height, posX, posY;
		width = props.get("frameWidth", 300);
		height = props.get("frameHeight", 200);
		this.setSize(width,height);
		Point p = GraphicsEnvironment.getLocalGraphicsEnvironment().getCenterPoint();
		p.x -= 300/2;
		p.y -= 200/2;
		posX   = props.get("framePosX", p.x > 0 ? p.x : 0);
		posY   = props.get("framePosY", p.y > 0 ? p.y : 0);
		this.setLocation(posX, posY);
		this.validate(); // force redraw
		this.setVisible(true);

		jCalc = this;
		//jOutputDoc = jOutputPane.getStyledDocument();
		jOutputDoc = new DefaultStyledDocument();
		jOutputPane.setDocument(jOutputDoc);
		Font f = new Font("Monospaced", Font.PLAIN, fontSize );
		jOutputPane.setFont(f);
		jInputField.setFont(f);
		height = jInputField.getGraphics().getFontMetrics().getHeight();
		jInputField.setBounds(jInputField.getBounds().x,jContentPane.getHeight()-height,jInputField.getWidth(),height);
		jContentPane.doLayout();

		// popupmenu
		getJPopupMenu();
		MouseListener popupListener = new PopupListener();
		jOutputPane.addMouseListener(popupListener);
		jInputField.addMouseListener(popupListener);

		// error
		errorAtr = new SimpleAttributeSet();
		StyleConstants.setForeground(errorAtr, cColor[Colors.ERROR]);
		// normal output
		outputAtr = new SimpleAttributeSet();
		StyleConstants.setForeground(outputAtr, cColor[Colors.OUTPUT]);
		// operator output
		outputOpAtr = new SimpleAttributeSet();
		StyleConstants.setForeground(outputOpAtr, cColor[Colors.OPERATOR]);
		// string output
		outputStringAtr = new SimpleAttributeSet();
		StyleConstants.setForeground(outputStringAtr, cColor[Colors.STRING]);
		// number 1 output
		outputNum1Atr = new SimpleAttributeSet();
		StyleConstants.setForeground(outputNum1Atr, cColor[Colors.NUM1]);
		// number 2 output
		outputNum2Atr = new SimpleAttributeSet();
		StyleConstants.setForeground(outputNum2Atr, cColor[Colors.NUM2]);
		// bracket output
		outputBracketAtr = new SimpleAttributeSet();
		StyleConstants.setForeground(outputBracketAtr, cColor[Colors.BRACKET]);
		// comment output
		outputCommentAtr = new SimpleAttributeSet();
		StyleConstants.setForeground(outputCommentAtr, cColor[Colors.STRING]);

		// create Parser
		try {
			jParse = new JParse();
			jParse.addDefaultOperators();
			jParse.addDefaultFunctions();
			jParse.addDefaultConstants();
		} catch (ParseException ex) {
			printErr("Init failed: "+ex.getMessage()+"\n");
		}

		jParse.setScale(internalScale);
		jParse.setScriptPath(scriptPath);

		// print status info
		printOut(versionStr+"\n");
		//printOut("Running in JRE "+jreStr+"\n");
		//printOut("Loading settings from "+iniFileStr+"\n\n");

		// add parse listener
		jParse.addParseListener(new JParseListener() {
			public void parseEvent(JParseEvent ev) {
				printOut(ev.getMessage());
				addLinefeed = true;
				// synchronize with AWT thread
				try {
					SwingUtilities.invokeAndWait(new Runnable() { public void run() {} });
				} catch (Exception e) {
					e.printStackTrace();
				}
			}

		});
		// request Focus
		jInputField.requestFocus();
	}

	/**
	 * This method initializes jInputField
	 *
	 * @return javax.swing.JTextField
	 */
	private JTextField getJInputField() {
		if (jInputField == null) {
			jInputField = new JTextField();
			jInputField.addKeyListener(new java.awt.event.KeyAdapter() {
				@Override
				public void keyPressed(java.awt.event.KeyEvent e) {
					switch (e.getKeyCode()) {
						case KeyEvent.VK_UP:
							commandHistoryReadIndex--;
							if (commandHistoryReadIndex < 0)
								commandHistoryReadIndex=commandHistory.length-1;
							jInputField.setText(commandHistory[commandHistoryReadIndex]);
							break;
						case KeyEvent.VK_DOWN:
							commandHistoryReadIndex++;
							if (commandHistoryReadIndex >= commandHistory.length)
								commandHistoryReadIndex=0;
							jInputField.setText(commandHistory[commandHistoryReadIndex]);
							break;
					}
				}
			});
			jInputField.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {
					String cmd = e.getActionCommand();
					if (cmd.length()>0) {
						commandHistory[commandHistoryStoreIndex++] = cmd;
						if (commandHistoryStoreIndex>=commandHistory.length)
							commandHistoryStoreIndex=0;
						commandHistoryReadIndex = commandHistoryStoreIndex;
						jInputField.setText("");
						printOutColored(cmd+"\n");
						setBusy(true);
						calcThread = new CalcThread();
						calcThread.setCommand(cmd);
						calcThread.start();

						/*
						try {
							result = jParse.parse(cmd);
							printOut("        out = "+result.toString()+"\n\n");
						} catch (ParseException ex) {
							StringBuffer sb = new StringBuffer();
							for (int i=0; i<jParse.getErrorPos();i++)
								sb.append('-');
							sb.append("^\n");
							printErr(sb.toString());
							printErr(ex.getMessage()+"\n\n");
						}
						 */
					}
				}
			});
		}
		return jInputField;
	}
	/**
	 * This method initializes jScrollPane
	 *
	 * @return javax.swing.JScrollPane
	 */
	private JScrollPane getJScrollPane() {
		if (jScrollPane == null) {
			jScrollPane = new JScrollPane();
			jScrollPane.setViewportView(getJOutputPane());
		}
		return jScrollPane;
	}
	/**
	 * This method initializes jOutputPane
	 *
	 * @return javax.swing.JTextPane
	 */
	private JTextPane getJOutputPane() {
		if (jOutputPane == null) {
			jOutputPane = new JTextPane();
			jOutputPane.setEditable(false);
		}
		return jOutputPane;
	}

	/**
	 * This method initializes jJMenuBar
	 *
	 * @return javax.swing.JMenuBar
	 */
	private JMenuBar getJJMenuBar() {
		if (jJMenuBar == null) {
			jJMenuBar = new JMenuBar();
			jJMenuBar.add(getJMenuFile());
			jJMenuBar.add(getJMenuOptions());  // Generated
			jJMenuBar.add(getJMenuView());  // Generated
			jJMenuBar.add(getJMenuHelp());  // Generated
		}
		return jJMenuBar;
	}
	/**
	 * This method initializes jMenu
	 *
	 * @return javax.swing.JMenu
	 */
	private JMenu getJMenuFile() {
		if (jMenuFile == null) {
			jMenuFile = new JMenu();
			jMenuFile.setText("File");
			jMenuFile.add(getJMenuItemSkriptPath());  // Generated
			jMenuFile.add(getJMenuItemBreak());  // Generated
			jMenuFile.add(getJMenuItemExit());
		}
		return jMenuFile;
	}
	/**
	 * This method initializes jMenuItemExit
	 *
	 * @return javax.swing.JMenuItem
	 */
	private JMenuItem getJMenuItemExit() {
		if (jMenuItemExit == null) {
			jMenuItemExit = new JMenuItem();
			jMenuItemExit.setText("Exit");
			jMenuItemExit.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {
					exit();
				}
			});
		}
		return jMenuItemExit;
	}
	/**
	 * This method initializes jMenu
	 *
	 * @return javax.swing.JMenu
	 */
	private JMenu getJMenuOptions() {
		if (jMenuOptions == null) {
			jMenuOptions = new JMenu();
			jMenuOptions.setText("Options");  // Generated
			jMenuOptions.add(getJMenuItemColors());  // Generated
			jMenuOptions.add(getJMenuItemScale());  // Generated
		}
		return jMenuOptions;
	}
	/**
	 * This method initializes jMenuItem
	 *
	 * @return javax.swing.JMenuItem
	 */
	private JMenuItem getJMenuItemColors() {
		if (jMenuItemColors == null) {
			jMenuItemColors = new JMenuItem();
			jMenuItemColors.setText("Set Font & Colors");  // Generated
			jMenuItemColors.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {
					ColorDialog cDiag = new ColorDialog(jCalc, true);
					cDiag.setParameters(cName, cColor, cColorDefault, fontSize);
					cDiag.setVisible(true);
					fontSize = cDiag.getFontSize();
					cColor = cDiag.getColors();
					// set new setting
					Font f = new Font("Monospaced", Font.PLAIN, fontSize );
					jOutputPane.setFont(f);
					jInputField.setFont(f);
					int height = jInputField.getGraphics().getFontMetrics().getHeight();
					jInputField.setBounds(jInputField.getBounds().x,jContentPane.getHeight()-height,jInputField.getWidth(),height);
					jContentPane.doLayout();
					StyleConstants.setForeground(errorAtr, cColor[Colors.ERROR]);
					StyleConstants.setForeground(outputAtr, cColor[Colors.OUTPUT]);
					StyleConstants.setForeground(outputOpAtr, cColor[Colors.OPERATOR]);
					StyleConstants.setForeground(outputStringAtr, cColor[Colors.STRING]);
					StyleConstants.setForeground(outputNum1Atr, cColor[Colors.NUM1]);
					StyleConstants.setForeground(outputNum2Atr, cColor[Colors.NUM2]);
					StyleConstants.setForeground(outputBracketAtr, cColor[Colors.BRACKET]);
					StyleConstants.setForeground(outputCommentAtr, cColor[Colors.STRING]);
					// store new settings
					props.set("fontSize", fontSize);
					props.set("outputColor", cColor[Colors.OUTPUT].getRGB());
					props.set("errorColor", cColor[Colors.ERROR].getRGB());
					props.set("outputOperatorColor", cColor[Colors.OPERATOR].getRGB());
					props.set("outputStringColor", cColor[Colors.STRING].getRGB());
					props.set("outputNumberColor1", cColor[Colors.NUM1].getRGB());
					props.set("outputNumberColor2", cColor[Colors.NUM2].getRGB());
					props.set("outputBracketColor", cColor[Colors.BRACKET].getRGB());
					props.set("outputCommentColor", cColor[Colors.COMMENT].getRGB());

				}
			});
		}
		return jMenuItemColors;
	}
	/**
	 * This method initializes jMenu
	 *
	 * @return javax.swing.JMenu
	 */
	private JMenu getJMenuView() {
		if (jMenuView == null) {
			jMenuView = new JMenu();
			jMenuView.setText("View");  // Generated
			jMenuView.add(getJMenuItemClear());  // Generated
		}
		return jMenuView;
	}
	/**
	 * This method initializes jMenuItem
	 *
	 * @return javax.swing.JMenuItem
	 */
	private JMenuItem getJMenuItemClear() {
		if (jMenuItemClear == null) {
			jMenuItemClear = new JMenuItem();
			jMenuItemClear.setText("Clear View");  // Generated
			jMenuItemClear.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {
					jOutputPane.setText("");
				}
			});
		}
		return jMenuItemClear;
	}
	/**
	 * This method initializes jMenuItem
	 *
	 * @return javax.swing.JMenuItem
	 */
	private JMenuItem getJMenuItemScale() {
		if (jMenuItemScale == null) {
			jMenuItemScale = new JMenuItem();
			jMenuItemScale.setText("Set Scale");  // Generated
			jMenuItemScale.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {
					ScaleDialog sDiag = new ScaleDialog(jCalc,true);
					sDiag.setScale(internalScale);
					sDiag.setVisible(true);
					internalScale = sDiag.getScale();
					props.set("internalScale", internalScale);
					jParse.setScale(internalScale);
				}
			});
		}
		return jMenuItemScale;
	}

	/**
	 * This method initializes jMenuItem
	 *
	 * @return javax.swing.JMenuItem
	 */
	private JMenuItem getJMenuItemSkriptPath() {
		if (jMenuItemSkriptPath == null) {
			jMenuItemSkriptPath = new JMenuItem();
			jMenuItemSkriptPath.setText("Set Script Path");  // Generated
			jMenuItemSkriptPath.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {
					JFileChooser jf = new JFileChooser(scriptPath);
					jf.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY );
					int returnVal = jf.showOpenDialog(jCalc);
					if(returnVal == JFileChooser.APPROVE_OPTION) {
						scriptPath = jf.getSelectedFile().getAbsolutePath();
					}
					jParse.setScriptPath(scriptPath);
					props.set("scriptPath",scriptPath);
				}
			});
		}
		return jMenuItemSkriptPath;
	}

	/**
	 * This method initializes jMenuItem
	 *
	 * @return javax.swing.JMenuItem
	 */
	private JMenuItem getJMenuItemBreak() {
		if (jMenuItemBreak == null) {
			jMenuItemBreak = new JMenuItem();
			jMenuItemBreak.setText("Break Calculation");  // Generated
			jMenuItemBreak.setEnabled(false);
			jMenuItemBreak.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {
					jParse.breakExec();
				}
			});
		}
		return jMenuItemBreak;
	}
	/**
	 * This method initializes jMenu
	 *
	 * @return javax.swing.JMenu
	 */
	private JMenu getJMenuHelp() {
		if (jMenuHelp == null) {
			jMenuHelp = new JMenu();
			jMenuHelp.setText("Help");  // Generated
			jMenuHelp.add(getJMenuItemHelp());  // Generated
		}
		return jMenuHelp;
	}
	/**
	 * This method initializes jMenuItem
	 *
	 * @return javax.swing.JMenuItem
	 */
	private JMenuItem getJMenuItemHelp() {
		if (jMenuItemHelp == null) {
			jMenuItemHelp = new JMenuItem();
			jMenuItemHelp.setText("Help");  // Generated
			jMenuItemHelp.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {
					Help help = new Help();
					help.setLocation(getX()+30,getY()+30);
					help.setSize(800,600);
					help.setVisible(true);
				}
			});
		}
		return jMenuItemHelp;
	}
	/**
	 * This method initializes jPopupMenu
	 *
	 * @return javax.swing.JPopupMenu
	 */
	private JPopupMenu getJPopupMenu() {
		if (jPopupMenu == null) {
			jPopupMenu = new JPopupMenu();
			jPopupMenu.add(getJPopupMenuItemBreak());  // Generated
			jPopupMenu.add(getJPopupMenuItemCopy());  // Generated
			jPopupMenu.add(getJPopupMenuItemPaste());  // Generated
			//jPopupMenu.setVisible(false);  // Generated
		}
		return jPopupMenu;
	}
	/**
	 * This method initializes jMenuItem
	 *
	 * @return javax.swing.JMenuItem
	 */
	private JMenuItem getJPopupMenuItemCopy() {
		if (jPopupMenuItemCopy == null) {
			jPopupMenuItemCopy = new JMenuItem();
			jPopupMenuItemCopy.setText("Copy");  // Generated
			jPopupMenuItemCopy.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {
					String s = ((JTextComponent)popupSource).getSelectedText();
					if (s!=null)
						setClipboard(s);
				}
			});
		}
		return jPopupMenuItemCopy;
	}
	/**
	 * This method initializes jMenuItem1
	 *
	 * @return javax.swing.JMenuItem
	 */
	private JMenuItem getJPopupMenuItemPaste() {
		if (jPopupMenuItemPaste == null) {
			jPopupMenuItemPaste = new JMenuItem();
			jPopupMenuItemPaste.setText("Paste");  // Generated
			jPopupMenuItemPaste.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {
					String s = getClipboard();
					if (s!=null)
						((JTextComponent)popupSource).paste();
				}
			});

		}
		return jPopupMenuItemPaste;
	}
	/**
	 * This method initializes jMenuItem2
	 *
	 * @return javax.swing.JMenuItem
	 */
	private JMenuItem getJPopupMenuItemBreak() {
		if (jPopupMenuItemBreak == null) {
			jPopupMenuItemBreak = new JMenuItem();
			jPopupMenuItemBreak.setText("Break");  // Generated
			jPopupMenuItemBreak.setEnabled(false);  // Generated
			jPopupMenuItemBreak.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {
					jParse.breakExec();
				}
			});
		}
		return jPopupMenuItemBreak;
	}

	static int getInt(String s) {
		try {
			return Integer.parseInt(s);
		} catch (NumberFormatException ex) {
			return 0;
		}
	}

	/**
	 * Main function, check JVM version, set Look and Feel
	 * @param args
	 */
	public static void main(String[] args) {
		/*
		 * Set "Look and Feel" to system default
		 */
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (Exception e) { /* don't care */}
		/*
		 * Apple menu bar for MacOS
		 */
		System.setProperty("com.apple.macos.useScreenMenuBar", "true");

		/*
		 * Check JVM version
		 */
		String jreStr = System.getProperty("java.version");
		String vs[] = jreStr.split("[._]");
		double vnum;
		if (vs.length >= 3) {
			vnum = (getInt(vs[0]))
			+ (getInt(vs[1])) * 0.1
			+ (getInt(vs[2])) * 0.01;
			if (vnum < 1.42) {
				JOptionPane.showMessageDialog(null,"Run this with JVM >= 1.4.2","Error",JOptionPane.ERROR_MESSAGE);
				System.exit(1);
			}
		}

		// detect webstart
		try {
			ServiceManager.lookup("javax.jnlp.BasicService");
		} catch (UnavailableServiceException ex) {
			isWebstartApp = false;
		};

		Toolkit.getDefaultToolkit().setDynamicLayout(true);

		// create Instance of this static class and call init function
		new Calcutta().init();
	}

	/**
	 * This is the default constructor
	 */
	public Calcutta() {
		super();
		initialize();
	}

	public static String getProgVerName() {
		return versionStr;
	}

	/**
	 * This method initializes this
	 */
	private void initialize() {

		ClassLoader loader = Calcutta.class.getClassLoader();
		Image img = Toolkit.getDefaultToolkit().getImage(loader.getResource("icon_32.png"));
		setIconImage(img);

		this.setJMenuBar(getJJMenuBar());
		this.setResizable(true);
		this.setSize(300,200);
		this.setContentPane(getJContentPane());
		this.setTitle("Calcutta");
		this.setVisible(false);
		this.addWindowListener(new java.awt.event.WindowAdapter() {
			@Override
			public void windowClosing(java.awt.event.WindowEvent e) {
				exit();
			}
			@Override
			public void windowClosed(java.awt.event.WindowEvent e) {
				exit();
			}
		});
	}

	/**
	 * Show a dialog with details about an exception
	 * @param ex Throwable/Exception to display
	 */
	public static void showException(final Throwable ex) {
		String m;
		m = "<html>";
		m += ex.getClass().getName()+"<p>";
		if (ex.getMessage() != null)
			m += ex.getMessage() +"<p>";
		StackTraceElement ste[] = ex.getStackTrace();
		for (int i=0; i<ste.length; i++) {
			m += ste[i].toString()+"<p>";
			// m += ste[i].getClassName()+"/"+ste[i].getMethodName()+"/"
			//   + ste[i].getLineNumber()+"<p>";
		}
		m += "</html>";
		ex.printStackTrace();
		JOptionPane.showMessageDialog( null, m, "Error", JOptionPane.ERROR_MESSAGE );
	}

	/**
	 * This method initializes jContentPane
	 *
	 * @return javax.swing.JPanel
	 */
	private javax.swing.JPanel getJContentPane() {
		if(jContentPane == null) {
			GridBagConstraints gridBagConstraints3 = new GridBagConstraints();
			GridBagConstraints gridBagConstraints2 = new GridBagConstraints();
			jContentPane = new javax.swing.JPanel();
			jContentPane.setLayout(new GridBagLayout());
			gridBagConstraints2.weightx = 1.0;
			gridBagConstraints2.fill = java.awt.GridBagConstraints.HORIZONTAL;
			gridBagConstraints2.gridx = 0;
			gridBagConstraints2.gridy = 3;
			gridBagConstraints3.gridx = 0;
			gridBagConstraints3.gridy = 0;
			gridBagConstraints3.weightx = 1.0;
			gridBagConstraints3.weighty = 1.0;
			gridBagConstraints3.fill = java.awt.GridBagConstraints.BOTH;
			jContentPane.add(getJInputField(), gridBagConstraints2);
			jContentPane.add(getJScrollPane(), gridBagConstraints3);
		}
		return jContentPane;
	}

	private class Colors {
		final static int OUTPUT   = 0;
		final static int NUM1     = 1;
		final static int NUM2     = 2;
		final static int COMMENT  = 3;
		final static int OPERATOR = 4;
		final static int BRACKET  = 5;
		final static int STRING   = 6;
		final static int ERROR    = 7;
	}
}  //  @jve:decl-index=0:visual-constraint="29,21"


