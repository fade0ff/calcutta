package GUI;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Point;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

/*
 * Copyright 2009 Volker Oth
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


/**
 * Dialog for editing scale value.
 *
 * @author Volker Oth
 */
public class ScaleDialog extends JDialog {
	final static long serialVersionUID = 0x000000001;

	private javax.swing.JPanel jContentPane = null;
	private JButton btnOk = null;
	private JButton btnCancel = null;
	private JTextField jTextField = null;
	private JLabel lblScale = null;
	private int scale = 64;

	public void setScale(int scale) {
		jTextField.setText(String.valueOf(scale));
	}

	public int getScale() {
		return scale;
	}

	private void getScale_() {
		String s = jTextField.getText();
		int sc;
		if (s.length() == 0)
			sc = 64;
		else {
			sc = Integer.parseInt(s);
			if (sc <10 || sc > 1000) {
				sc = 64;
				setScale(sc);
			}
		}
		scale = sc;
	}

	/**
	 * Constructor for modal dialog in parent frame
	 * @param frame parent frame
	 * @param modal create modal dialog?
	 */
	public ScaleDialog(JFrame frame, boolean modal) {
		super(frame, modal);
		initialize();
		//
		Point p = frame.getLocation();
		this.setLocation(p.x+frame.getWidth()/2-getWidth()/2, p.y+frame.getHeight()/2-getHeight()/2);
		//
	}

	/**
	 * This method initializes this
	 *
	 * @return void
	 */
	private void initialize() {
		this.setSize(198, 117);
		this.setContentPane(getJContentPane());
	}
	/**
	 * This method initializes jContentPane
	 *
	 * @return javax.swing.JPanel
	 */
	private javax.swing.JPanel getJContentPane() {
		if(jContentPane == null) {
			lblScale = new JLabel();
			GridBagConstraints gridBagConstraints3 = new GridBagConstraints();
			GridBagConstraints gridBagConstraints2 = new GridBagConstraints();
			GridBagConstraints gridBagConstraints1 = new GridBagConstraints();
			GridBagConstraints gridBagConstraints4 = new GridBagConstraints();
			jContentPane = new javax.swing.JPanel();
			jContentPane.setLayout(new GridBagLayout());
			gridBagConstraints1.gridx = 2;  // Generated
			gridBagConstraints1.gridy = 2;  // Generated
			gridBagConstraints2.gridx = 1;  // Generated
			gridBagConstraints2.gridy = 2;  // Generated
			gridBagConstraints3.gridx = 2;  // Generated
			gridBagConstraints3.gridy = 1;  // Generated
			gridBagConstraints3.weightx = 1.0;  // Generated
			gridBagConstraints3.fill = java.awt.GridBagConstraints.HORIZONTAL;  // Generated
			gridBagConstraints4.gridx = 1;  // Generated
			gridBagConstraints4.gridy = 1;  // Generated
			lblScale.setText("Intenal Scale");  // Generated
			gridBagConstraints4.insets = new java.awt.Insets(4,8,4,4);  // Generated
			gridBagConstraints2.insets = new java.awt.Insets(4,8,4,4);  // Generated
			gridBagConstraints2.fill = java.awt.GridBagConstraints.HORIZONTAL;  // Generated
			gridBagConstraints3.insets = new java.awt.Insets(4,4,4,8);  // Generated
			gridBagConstraints1.fill = java.awt.GridBagConstraints.HORIZONTAL;  // Generated
			gridBagConstraints1.insets = new java.awt.Insets(4,4,4,8);  // Generated
			jContentPane.add(getBtnOk(), gridBagConstraints1);  // Generated
			jContentPane.add(getBtnCancel(), gridBagConstraints2);  // Generated
			jContentPane.add(getJTextField(), gridBagConstraints3);  // Generated
			jContentPane.add(lblScale, gridBagConstraints4);  // Generated
		}
		return jContentPane;
	}
	/**
	 * This method initializes jButton
	 *
	 * @return javax.swing.JButton
	 */
	private JButton getBtnOk() {
		if (btnOk == null) {
			btnOk = new JButton();
			btnOk.setText("OK");  // Generated
			btnOk.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {
					getScale_();
					dispose();
				}
			});
		}
		return btnOk;
	}
	/**
	 * This method initializes jButton1
	 *
	 * @return javax.swing.JButton
	 */
	private JButton getBtnCancel() {
		if (btnCancel == null) {
			btnCancel = new JButton();
			btnCancel.setText("Cancel");  // Generated
			btnCancel.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {
					dispose();
				}
			});
		}
		return btnCancel;
	}
	/**
	 * This method initializes jTextField
	 *
	 * @return javax.swing.JTextField
	 */
	private JTextField getJTextField() {
		if (jTextField == null) {
			jTextField = new JTextField();
			jTextField.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {
					getScale_();
				}
			});
		}
		return jTextField;
	}
}  //  @jve:decl-index=0:visual-constraint="10,10"
